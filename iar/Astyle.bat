set iarProjPath=%CD%
set list=..\source\Appl
set list=%list%;..\source\Base\StateMachine 
set list=%list%;..\source\Driver

@echo off
for %%a in (%list%) do ( 
	cd %iarProjPath%
	cd %%a
	for /r %%i in  (*.c) do "%1\bin\AStyle.exe" %%i  --options=%1\buchiStyle 
	for /r %%i in  (*.cpp) do "%1\bin\AStyle.exe" %%i  --options=%1\buchiStyle 
	for /r %%i in (*.h) do "%1\bin\AStyle.exe" %%i  --options=%1\buchiStyle
)


