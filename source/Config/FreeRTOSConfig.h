/*-----------------------------------------------------------------------------

Bόchi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------

Autor                  Datum
R. Kaufmann            16.01.2020

-- BESCHREIBUNG ---------------------------------------------------------------
Basiert auf dem lwip_httpsrv und lwip_httpscli_mbedTLS Beispielen
mit FreeRTOS fόr IAR aus dem SDK.

Achtung, jeweiliges Speichermodell (configFRTOS_MEMORY_SCHEME) beachten!

-- AENDERUNGEN ----------------------------------------------------------------

Autor                  Datum

-----------------------------------------------------------------------------*/

/*
 * FreeRTOS Kernel V10.2.0
 * Copyright (C) 2019 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * http://www.FreeRTOS.org
 * http://aws.amazon.com/freertos
 *
 */

//
// Memory scheme info: https://www.freertos.org/a00111.html
//
//Memory allocation implementations included in the RTOS source code download
//The FreeRTOS download includes five sample memory allocation implementations, each of which are described in the following subsections.
//The subsections also include information on when each of the provided implementations might be the most appropriate to select.
//
//Each provided implementation is contained in a separate source file(heap_1.c, heap_2.c, heap_3.c, heap_4.c and heap_5.c respectively)
//which are located in the Source / Portable / MemMang directory of the main RTOS source code download.Other implementations can be added
//as needed.Exactly one of these source files should be included in a project at a time[the heap defined by these portable layer functions
// will be used by the RTOS kernel even if the application that is using the RTOS opts to use its own heap implementation].
//
//Following below :
//
//heap_1  the very simplest, does not permit memory to be freed
//heap_2  permits memory to be freed, but does not coalescence adjacent free blocks.
//heap_3  simply wraps the standard malloc() and free() for thread safety
//heap_4  coalescences adjacent free blocks to avoid fragmentation.Includes absolute address placement option
//heap_5  as per heap_4, with the ability to span the heap across multiple non - adjacent memory areas
//
//
//heap_1.c
//This is the simplest implementation of all.It does not permit memory to be freed once it has been allocated.Despite this, heap_1.c is appropriate
//for a large number of embedded applications.This is because many small and deeply embedded applications create all the tasks, queues, semaphores,
//etc.required when the system boots, and then use all of these objects for the lifetime of program(until the application is switched off again,
//or is rebooted).Nothing ever gets deleted.
//
//The implementation simply subdivides a single array into smaller blocks as RAM is requested.The total size of the array(the total size of
//the heap) is set by configTOTAL_HEAP_SIZE  which is defined in FreeRTOSConfig.h.The configAPPLICATION_ALLOCATED_HEAP FreeRTOSConfig.h
//configuration constant is provided to allow the heap to be placed at a specific address in memory.
//
//The xPortGetFreeHeapSize() API function returns the total amount of heap space that remains unallocated, allowing the configTOTAL_HEAP_SIZE setting to be optimised.
//
//The heap_1 implementation :
//
//Can be used if your application never deletes a task, queue, semaphore, mutex, etc. (which actually covers the majority of applications in which FreeRTOS gets used).
//
//Is always deterministic(always takes the same amount of time to execute) and cannot result in memory fragmentation.
//
//Is very simple and allocated memory from a statically allocated array, meaning it is often suitable for use in applications that do not permit true dynamic memory allocation.
//
//heap_2.c
//This scheme uses a best fit algorithm and, unlike scheme 1, allows previously allocated blocks to be freed.It does not combine adjacent free 
//blocks into a single large block.See heap_4.c for an implementation that does coalescence free blocks.
//
//The total amount of available heap space is set by configTOTAL_HEAP_SIZE  which is defined in FreeRTOSConfig.h.The 
//configAPPLICATION_ALLOCATED_HEAP FreeRTOSConfig.h configuration constant is provided to allow the heap to be placed at a specific address in memory.
//
//The xPortGetFreeHeapSize() API function returns the total amount of heap space that remains unallocated, (allowing the configTOTAL_HEAP_SIZE setting 
//to be optimised), but does not provided information on how the unallocated memory is fragmented into smaller blocks.
//
//This implementation :
//
//Can be used even when the application repeatedly deletes tasks, queues, semaphores, mutexes, etc., with the caveat below regarding memory fragmentation.
//
//Should not be used if the memory being allocated and freed is of a random size.For example :
//If an application dynamically creates and deletes tasks, and the size of the stack allocated to the tasks being created is always the same, then heap2.c
//can be used in most cases.However, if the size of the stack allocated to the tasks being created was not always the same, then the available free memory 
//might become fragmented into many small blocks, eventually resulting in allocation failures.heap_4.c would be a better choise in this case.
//
//If an application dynamically creates and deletes queues, and the queue storage area is the same in each case (the queue storage area is the queue
//item size multiplied by the length of the queue), then heap_2.c can be used in most cases.However, if the queue storage area were not the same in
//each case, then the available free memory might become fragmented into many small blocks, eventually resulting in allocation failures.heap_4.c would be a better choise in this case.
//
//The application called pvPortMalloc() and vPortFree() directly, rather than just indirectly through other FreeRTOS API functions.
//
//Could possible result in memory fragmentation problems if your application queues, tasks, semaphores, mutexes, etc.in an unpredictable order.
//This would be unlikely for nearly all applications but should be kept in mind.
//
//Is not deterministic  but is much more efficient that most standard C library malloc implementations.
//
//heap_2.c is suitable for many small real time systems that have to dynamically create objects.See heap_4 for a similar implementation
//that combines free memory blocks into single larger blocks.
//
//heap_3.c
//This implements a simple wrapper for the standard C library malloc() and free() functions that will, in most cases, be supplied with 
//your chosen compiler.The wrapper simply makes the malloc() and free() functions thread safe.
//
//This implementation :
//
//Requires the linker to setup a heap, and the compiler library to provide malloc() and free() implementations.
//
//Is not deterministic.
//
//Will probably considerably increase the RTOS kernel code size.
//
//Note that the configTOTAL_HEAP_SIZE setting in FreeRTOSConfig.h has no effect when heap_3 is used.
//
//heap_4.c
//This scheme uses a first fit algorithm and, unlike scheme 2, it does combine adjacent free memory blocks into a single large block(it does include a coalescence algorithm).
//
//The total amount of available heap space is set by configTOTAL_HEAP_SIZE  which is defined in FreeRTOSConfig.h.The configAPPLICATION_ALLOCATED_HEAP 
//FreeRTOSConfig.h configuration constant is provided to allow the heap to be placed at a specific address in memory.
//
//The xPortGetFreeHeapSize() API function returns the total amount of heap space that remains unallocated when the function is called, and the 
//xPortGetMinimumEverFreeHeapSize() API function returns lowest amount of free heap space that has existed system the FreeRTOS application booted.Neither 
//function provides information on how the unallocated memory is fragmented into smaller blocks.
//
//This implementation :
//
//Can be used even when the application repeatedly deletes tasks, queues, semaphores, mutexes, etc..
//
//Is much less likely than the heap_2 implementation to result in a heap space that is badly fragmented into multiple small blocks  even when the memory
//being allocated and freed is of random size.
//
//Is not deterministic  but is much more efficient that most standard C library malloc implementations.
//
//heap_4.c is particularly useful for applications that want to use the portable layer memory allocation schemes directly in the application code(rather than
//just indirectly by calling API functions that themselves call pvPortMalloc() and vPortFree()).
//
//heap_5.c
//This scheme uses the same first fit and memory coalescence algorithms as heap_4, and allows the heap to span multiple non adjacent(non - contiguous) memory regions.
//
//Heap_5 is initialised by calling vPortDefineHeapRegions(), and cannot be used until after vPortDefineHeapRegions() has executed.Creating an RTOS object(task,
//queue, semaphore, etc.) will implicitly call pvPortMalloc() so it is essential that, when using heap_5, vPortDefineHeapRegions() is called before the creation of any such object.
//
//vPortDefineHeapRegions() takes a single parameter.The parameter is an array of HeapRegion_t structures.HeapRegion_t is defined in portable.h as
//
//
//typedef struct HeapRegion
//{
//   /* Start address of a block of memory that will be part of the heap.*/
//   uint8_t *pucStartAddress;
//
//   /* Size of the block of memory. */
//   size_t xSizeInBytes;
//} HeapRegion_t;
//
//The HeapRegion_t type definition
//
//
//The array is terminated using a NULL zero sized region definition, and the memory regions defined in the array must appear in address order, 
//from low address to high address.The following source code snippets provide an example.The MSVC Win32 simulator demo also uses heap_5 so can be used as a reference.
//
//
///* Allocate two blocks of RAM for use by the heap.  The first is a block of
//0x10000 bytes starting from address 0x80000000, and the second a block of
//0xa0000 bytes starting from address 0x90000000.  The block starting at
//0x80000000 has the lower start address so appears in the array fist. */
//const HeapRegion_t xHeapRegions[] =
//{
//    { (uint8_t *)0x80000000UL, 0x10000 },
//    { (uint8_t *)0x90000000UL, 0xa0000 },
//    { NULL, 0 } /* Terminates the array. */
//};
//
///* Pass the array into vPortDefineHeapRegions(). */
//vPortDefineHeapRegions(xHeapRegions);
//
//Initialising heap_5 after defining the memory blocks to be used by the heap
//
//The xPortGetFreeHeapSize() API function returns the total amount of heap space that remains unallocated when the function is called, and the xPortGetMinimumEverFreeHeapSize()
//API function returns lowest amount of free heap space that has existed system the FreeRTOS application booted.Neither function provides information on how the unallocated
//memory is fragmented into smaller blocks.

#ifndef FREERTOS_CONFIG_H
#define FREERTOS_CONFIG_H

/*-----------------------------------------------------------
 * Application specific definitions.
 *
 * These definitions should be adjusted for your particular hardware and
 * application requirements.
 *
 * THESE PARAMETERS ARE DESCRIBED WITHIN THE 'CONFIGURATION' SECTION OF THE
 * FreeRTOS API DOCUMENTATION AVAILABLE ON THE FreeRTOS.org WEB SITE.
 *
 * See http://www.freertos.org/a00110.html.
 *
 * Low priority numbers denote low priority tasks.
 * The idle task has priority zero (tskIDLE_PRIORITY).
 *
 * The FreeRTOS scheduler ensures that tasks in the Ready or Running state 
 * will always be given processor (CPU) time in preference to tasks of a lower 
 * priority that are also in the ready state. In other words, the task placed
 * into the Running state is always the highest priority task that is able 
 * to run. Any number of tasks can share the same priority.
 *
 *---------------------------------------------------------- */

#define configUSE_PREEMPTION                    1
#define configUSE_TICKLESS_IDLE                 0
#define configCPU_CLOCK_HZ                      (SystemCoreClock)
#define configTICK_RATE_HZ                      ((TickType_t)1000)
#define configMAX_PRIORITIES                    18
#define configMINIMAL_STACK_SIZE                ((unsigned short)90)
#define configMAX_TASK_NAME_LEN                 20
#define configUSE_16_BIT_TICKS                  0
#define configIDLE_SHOULD_YIELD                 1
#define configUSE_TASK_NOTIFICATIONS            1
#define configUSE_MUTEXES                       1
#define configUSE_RECURSIVE_MUTEXES             1
#define configUSE_COUNTING_SEMAPHORES           1
#define configUSE_ALTERNATIVE_API               0 /* Deprecated! */
#define configQUEUE_REGISTRY_SIZE               8
#define configUSE_QUEUE_SETS                    0
#define configUSE_TIME_SLICING                  0
#define configUSE_NEWLIB_REENTRANT              0
#define configENABLE_BACKWARD_COMPATIBILITY     0
#define configNUM_THREAD_LOCAL_STORAGE_POINTERS 5
#define configUSE_APPLICATION_TASK_TAG          0

/* Used memory allocation (heap_x.c) */
#define configFRTOS_MEMORY_SCHEME               4
/* Tasks.c additions (e.g. Thread Aware Debug capability) */
#define configINCLUDE_FREERTOS_TASK_C_ADDITIONS_H 1

/* Memory allocation related definitions. */
#define configSUPPORT_STATIC_ALLOCATION         0
#define configSUPPORT_DYNAMIC_ALLOCATION        1
#define configTOTAL_HEAP_SIZE                   0x20000
#define configAPPLICATION_ALLOCATED_HEAP        0

/* Hook function related definitions. */
#define configUSE_IDLE_HOOK                     0
#define configUSE_TICK_HOOK                     0
#define configCHECK_FOR_STACK_OVERFLOW          0
#define configUSE_MALLOC_FAILED_HOOK            0
#define configUSE_DAEMON_TASK_STARTUP_HOOK      0

/* Run time and task stats gathering related definitions. */
#define configGENERATE_RUN_TIME_STATS           0
#define configUSE_TRACE_FACILITY                1
#define configUSE_STATS_FORMATTING_FUNCTIONS    1

/* Task aware debugging. */
#define configRECORD_STACK_HIGH_ADDRESS         1

/* Co-routine related definitions. */
#define configUSE_CO_ROUTINES                   0
#define configMAX_CO_ROUTINE_PRIORITIES         2

/* Software timer related definitions. */
#define configUSE_TIMERS                        1
#define configTIMER_TASK_PRIORITY               (configMAX_PRIORITIES - 1)
#define configTIMER_QUEUE_LENGTH                10
#define configTIMER_TASK_STACK_DEPTH            (configMINIMAL_STACK_SIZE * 2)

/* Define to trap errors during development. */
#define configASSERT(x) if((x) == 0) {taskDISABLE_INTERRUPTS(); for (;;);}

/* Optional functions - most linkers will remove unused functions anyway. */
#define INCLUDE_vTaskPrioritySet                1
#define INCLUDE_uxTaskPriorityGet               1
#define INCLUDE_vTaskDelete                     1
#define INCLUDE_vTaskSuspend                    1
#define INCLUDE_xResumeFromISR                  1
#define INCLUDE_vTaskDelayUntil                 1
#define INCLUDE_vTaskDelay                      1
#define INCLUDE_xTaskGetSchedulerState          1
#define INCLUDE_xTaskGetCurrentTaskHandle       1
#define INCLUDE_uxTaskGetStackHighWaterMark     0
#define INCLUDE_xTaskGetIdleTaskHandle          0
#define INCLUDE_eTaskGetState                   0
#define INCLUDE_xEventGroupSetBitFromISR        1
#define INCLUDE_xTimerPendFunctionCall          1
#define INCLUDE_xTaskAbortDelay                 0
#define INCLUDE_xTaskGetHandle                  0
#define INCLUDE_xTaskResumeFromISR              1


/* Interrupt nesting behaviour configuration. Cortex-M specific. */
#ifdef __NVIC_PRIO_BITS
/* __BVIC_PRIO_BITS will be specified when CMSIS is being used. */
#define configPRIO_BITS __NVIC_PRIO_BITS
#else
#define configPRIO_BITS 4 /* 15 priority levels */
#endif

/* The lowest interrupt priority that can be used in a call to a "set priority"
function. */
#define configLIBRARY_LOWEST_INTERRUPT_PRIORITY ((1U << (configPRIO_BITS)) - 1)

/* The highest interrupt priority that can be used by any interrupt service
routine that makes calls to interrupt safe FreeRTOS API functions.  DO NOT CALL
INTERRUPT SAFE FREERTOS API FUNCTIONS FROM ANY INTERRUPT THAT HAS A HIGHER
PRIORITY THAN THIS! (higher priorities are lower numeric values. */
#define configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY 2

/* Interrupt priorities used by the kernel port layer itself.  These are generic
to all Cortex-M ports, and do not rely on any particular library functions. */
#define configKERNEL_INTERRUPT_PRIORITY (configLIBRARY_LOWEST_INTERRUPT_PRIORITY << (8 - configPRIO_BITS))
/* !!!! configMAX_SYSCALL_INTERRUPT_PRIORITY must not be set to zero !!!!
See http://www.FreeRTOS.org/RTOS-Cortex-M3-M4.html. */
#define configMAX_SYSCALL_INTERRUPT_PRIORITY (configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY << (8 - configPRIO_BITS))

/* Definitions that map the FreeRTOS port interrupt handlers to their CMSIS
standard names. */
#define vPortSVCHandler SVC_Handler
#define xPortPendSVHandler PendSV_Handler
#define xPortSysTickHandler SysTick_Handler

#if defined(__ICCARM__)||defined(__CC_ARM)||defined(__GNUC__)
    /* Clock manager provides in this variable system core clock frequency */
    #include <stdint.h>
    extern uint32_t SystemCoreClock;
#endif

#endif /* FREERTOS_CONFIG_H */
