/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------
Autor                Datum
R. Kaufmann          16.01.2020

-- BESCHREIBUNG ---------------------------------------------------------------

Produktionsger�te haben i.d.Regel keine M�glichkeit f�r Debugausgaben
z.B. �ber einen UART usw. SWO und Segger-Tools sind ein Drama, langsam, buggy.
Um Debugausgaben w�hrend der Entwicklung ohne Hardwareaufwand zu erm�glichen,
nutzen wir den vorhandenen MQTT-Client und schalten den entsprechenden Code im 
SDK ab.

-- AENDERUNGEN ----------------------------------------------------------------
Autor                Datum

-----------------------------------------------------------------------------*/

#include <stdio.h>


// Diese Datei ist absichtlich so und der Include-Pfad des Projekts enth�lt NICHT
// den Ort im SDK, der urspr�nglichen fsl_debug_console.h

// Leere Funktionen
#define PRINTF printf
#define SCANF scanf
#define PUTCHAR putchar
#define GETCHAR getchar