/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------

Autor                  Datum
F.Sch�r                20.03.2014

-- BESCHREIBUNG ---------------------------------------------------------------

Allgemeine L-200 / L-300 spezifische Definitionen


-- AENDERUNGEN ----------------------------------------------------------------

Autor                 Datum

-----------------------------------------------------------------------------*/
#pragma once

#define MAX_MSG                 (76u)

// commands
#define COM_DO_TEST            (0x60)   // Test-command

// IO simulation port: request
#define COM_IO_REQUEST         (0x70)   // Test IO request

// message communication
#define COM_DO_MSG             (0x80)   // Test message

#define L100PRO_ADDRESS        (0x51)
#define L100_ADDRESS           (0x52)
#define NO_ADDRESS             (0x00)

// observable ID's
#define OBSERVABLE_MSG         (0x10)
#define OBSERVABLE_MSGSYS      (0x11)
#define OBSERVABLE_MSGSYS_ACK  (0x12)
#define OBSERVABLE_TMR         (0x13)

#define OP_ON                  (0x10)   // operation on
#define OP_OFF                 (0x20)   // operation off
                               
#define OP_ON1                 (0x10)   // operation side 1 on
#define OP_OFF1                (0x20)   // operation side 1 off
                               
#define OP_ON2                 (0x40)   // operation side 2 on
#define OP_OFF2                (0x80)   // operation side 2 off

#define VALVE_OFF              (0x00)   // valve is off
#define VALVE_ON               (0x01)   // valve is on
#define VALVE_AV               (0x10)   // valve is available
#define VALVE_UNAV             (0x00)   // valve is unavailable

#define VALVE_1_OFF            (0x00)   // valve 1 is off
#define VALVE_1_ON             (0x01)   // valve 1 is on
#define VALVE_1_AV             (0x10)   // valve 1 is available
#define VALVE_1_UNAV           (0x00)   // valve 1 is unavailable

#define VALVE_2_OFF            (0x00)   // valve 2 is off
#define VALVE_2_ON             (0x02)   // valve 2 is on
#define VALVE_2_AV             (0x20)   // valve 2 is available
#define VALVE_2_UNAV           (0x00)   // valve 2 is unavailable

// Communication
// messages

// Cyclic master communication Freeze dryer -> control unit (view)
#define COM_CYCLIC_STATUS                  (0x40)   // Master sendet und empf�ngt zyklisch Status
#define COM_CYCLIC_ADV_STATUS              (0x41)   // Master sendet zyklisch Advanced Status und empf�ngt zyklisch Advanced Command
#define COM_CYCLIC_PIL_STATUS              (0x42)   // Master sendet zyklisch Pilot Status und empf�ngt zyklisch Pilot Command
#define COM_CYCLIC_MSG                     (0x43)   // Master sendet und empf�ngt zyklisch eine Nachricht
#define COM_CYCLIC_INTERNAL                (0x44)   // Master sendet (und empf�ngt) zyklisch interne Daten
#define COM_CYCLIC_DATA                    (0x45)   // Master sendet (und empf�ngt) zyklisch Daten
#define COM_CYCLIC_DATA_ADV                (0x46)   // Master sendet (und empf�ngt) zyklisch Advanced Daten
#define COM_CYCLIC_DATA_PILOT              (0x47)   // Master sendet (und empf�ngt) zyklisch Pilot Daten
                                           
#define COM_CYCLIC_METHOD_STEP             (0x48)   // Master beantragt (und empf�ngt) zyklisch Methoden Daten

#define COM_CYCLIC_MEAS_BOARD              (0x49)   // Master beantragt (und empf�ngt) zyklisch Messboard Daten
                                           
                                           
// Device type defines                     
#define MODEL_BASIC                        (0x01)   // Basic device model
#define MODEL_ADVANCED                     (0x02)   // Advanced device model
#define MODEL_PILOT                        (0x04)   // Pilot device model
                                           
// Condenser type defines                  
#define ICE_CONDENSER_SINGLE               (0x01)   // Single ice condenser
#define ICE_CONDENSER_TWIN                 (0x02)   // Twin ice condenser
                                           
#define COND1_ACT                        (0x1000)
#define COND2_ACT                        (0x2000)
#define COND_SWITCH_FREQ                     (10)
                                          
#define DEFROST_ANS_YES                    (0x04)
#define DEFROST_ANS_NO                     (0x08)

static const short                         iMinTemp                 = -1300; // -130�C minimum
static const short                         iMaxTemp                 = 1800; // 180�C maximum

static const short                         iINV_TEMP                = -10000; // invalid temperature (-1000�C)
                                           
static const float                         fMinEnvironmentPress     = 689.4f; // 689.4 mBar. Minimum environment pressure (3000 m)
static const float                         fMaxWheatherPressDelta   = 60.0f; // 60.0 mBar. Delta of environment pressure dependent of wheater conditions

static const float                         fMINPRESS_NO_SENSOR      = 1200.1f;
static const float                         fANALOG_MIN_PRESS        = 0.0005f; // 0.0005 mBar this vacuum value is outside the system's vacuum range

static const float                         fPRESS_NO_SENSOR         = 2000.0f; // 2Bar: value for no pressure sensor attached
static const float                         fPRESS_NO_PRESS_SENSOR   = 65000.0f; // 65Bar: value for no pressure sensor (in cooling circuit) attached

#ifdef _DEBUG
  #define SIM_TIME_DIV                          (1) /* max 50! */
#else
  #define SIM_TIME_DIV                          (1)
#endif

//#define ENDURANCE
//#define ENDURANCE_1

#ifdef ENDURANCE
#ifdef ENDURANCE_1
#define ENDURANCE_TEST_1
#else
#define ENDURANCE_TEST_2
#endif
#endif

// switch to use the analog pressures sensors to regulate pressure in ice condensers
//#define USE_ANALOG_PRESS_SENSORS

// bit definitions for process states:
typedef enum
{
   PROC_STATE_STANDBY,
   PROC_STATE_UNUSED,
   PROC_STATE_DEFROSTING,
   PROC_STATE_FORCED_MANUAL_DEFROSTING,
   PROC_STATE_CONDITIONING,
   PROC_STATE_RECONDITIONING,
   PROC_STATE_RECOVER,
   PROC_STATE_MANUAL_DRYING,
   PROC_STATE_VACUUM_TEST,
   PROC_STATE_PUMP_WARM_UP,
   PROC_STATE_LEAK_TEST,
   PROC_STATE_DEVICE_TEST,
   PROC_STATE_PRESSURE_RISE_TEST,
   PROC_STATE_SHUT_DOWN,
   PROC_STATE_TEMPERING_SHELF,
   PROC_STATE_LOAD,
   PROC_STATE_UNLOAD,
   PROC_STATE_LOAD_UNLOAD,
   PROC_STATE_LOAD_DONE,
   PROC_STATE_FREEZING,
   PROC_STATE_PRIMARY_DRYING,
   PROC_STATE_SECONDARY_DRYING,
   PROC_STATE_STOPPERING,
   PROC_STATE_HOLD,
   PROC_STATE_AERATE,
   PROC_STATE_TEMP_SAMPLE_PROT,
   PROC_STATE_VACUUMIZING,
   PROC_STATE_TIMER1,
   PROC_STATE_TIMER2,
   PROC_STATE_PRESS_SAMPLE_PROT
} PROCESS_STATE;

// method phases
typedef enum
{
   METHOD_PHASE_UNUSED = 0,
   METHOD_PHASE_LOADING,
   METHOD_PHASE_FREEZING,
   METHOD_PHASE_PRIMARY_DRYING,
   METHOD_PHASE_SECONDARY_DRYING,
   METHOD_PHASE_STOPPERING,
   METHOD_PHASE_HOLD,
   METHOD_PHASE_MAN_TEMP_RAMP
} METHOD_PHASES;

// bit definitions for operation permissions:
typedef enum
{
   OP_PERMISSION_START_CONDITIONING,
   OP_PERMISSION_START_DRYING,
   OP_PERMISSION_STOP_DRYING,
   OP_PERMISSION_GOTO_STANDBY,
   OP_PERMISSION_AERATE,
   OP_PERMISSION_LOAD_ACK,
   OP_PERMISSION_VACUUM_TEST,
   OP_PERMISSION_LEAK_TEST,
   OP_PERMISSION_START_VAC_REG,
   OP_PERMISSION_SET_TARGET_VAC,
   OP_PERMISSION_SWITCH_ACTORS,
   OP_PERMISSION_START_PROCESS,
   OP_PERMISSION_ABORT_PROCESS,
   OP_PERMISSION_SELECT_NEW_METHOD,
   OP_PERMISSION_UNLOAD,
   OP_PERMISSION_SET_SHELF_TARGET_TEMP,
   OP_PERMISSION_START_STOP_SHELF_TEMP_REG,
   OP_PERMISSION_STOPPERING,
   OP_PERMISSION_SMART_FREEZE_DRYING,
   OP_PERMISSION_SERVICE,
   OP_PERMISSION_MANUAL,
   OP_PERMISSION_MANUAL_DEFR,
   OP_PERMISSION_PRESS_RISE_TEST
} OP_PERMISSION;


enum enLeakTestVar { LEAK_TEST_DRY_CHAMB, LEAK_TEST_ICE_COND_1, LEAK_TEST_ICE_COND_2, LEAK_TEST_SYSTEM };

enum enTimer {
   MAN_DRYING_TIMER,
   AUTOM_DRYING_TIMER,
   DRYING_STEP_TIMER
};
//-----------------------------------------------------------------------------
