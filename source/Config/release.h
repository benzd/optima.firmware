/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------
Autor                Datum
R. Kaufmann          27.08.2018

-- BESCHREIBUNG ---------------------------------------------------------------


-- AENDERUNGEN ----------------------------------------------------------------
Autor                Datum

-----------------------------------------------------------------------------*/
#pragma once

// Firmware Version
#define FW_MAJOR_VERSION   0        // Firmware Major Version   0...99
#define FW_MINOR_VERSION   1        // Firmware Minor Version   0...99
#define FW_BUILD_NUMBER    0        // Firmware Build Nummer    0...99
#define FW_REVISION_NUMBER 99        // Firmware Revision Nummer 0...99

// Artikelnummer der Firmware   "12345678" (Programmierunterlagen)
#define FW_ARTICLE_NUMBER       "12345678"


