/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------
Autor                  Datum
Dominik Benz           19.09.2019

-- BESCHREIBUNG ---------------------------------------------------------------



-- AENDERUNGEN ----------------------------------------------------------------
Autor                  Datum

-----------------------------------------------------------------------------*/
//-----------------------------------------------------------------------------
// INCLUDES
//-----------------------------------------------------------------------------
#include "fsl_lpi2c.h"
#include "I2C_Controller.h"
#include "Hardware.h"

//-----------------------------------------------------------------------------
// MACRO DEFINITIONS
//-----------------------------------------------------------------------------
#define LPI2C_MASTER_BASEADDR           LPI2C1
#define LPI2C_CLOCK_SOURCE_SELECT       (0U)
#define LPI2C_CLOCK_SOURCE_DIVIDER      (5U)
#define LPI2C_CLOCK_FREQUENCY           ((CLOCK_GetFreq(kCLOCK_Usb1PllClk) / 8) / (LPI2C_CLOCK_SOURCE_DIVIDER + 1U))
#define LPI2C_MASTER_IRQ                LPI2C1_IRQn
#define LPI2C_BAUDRATE                  100000U

//#define I2C_FLOW_SLAVE_ADR_7BIT         0x12    // 0x12 = touch controller for test, final flow sensor 0x40
#define I2C_FLOW_SLAVE_ADR_7BIT         0x40    // 0x12 = touch controller for test, final flow sensor 0x40


//-----------------------------------------------------------------------------
// LOCAL TYPE DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// LOCAL FUNCTION PROTOTYPES
//-----------------------------------------------------------------------------
static void lpi2c_master_callback(LPI2C_Type *base, lpi2c_master_handle_t *handle, status_t status, void *userData);
static Bool I2C_Send(UInt8 slaveAdr, UInt32 subAdr, UInt32 subAdrSize, UInt8 *txDataBuff, UInt8 txDataLen);
static Bool I2C_Receive(UInt8 slaveAdr, UInt32 subAdr, UInt32 subAdrSize, UInt8 *rxDataBuff, UInt8 rxDataLen);

//-----------------------------------------------------------------------------
// LOCAL VARIABLE DEFINITIONS
//-----------------------------------------------------------------------------
static lpi2c_master_transfer_t masterXfer = {0};
static lpi2c_master_handle_t g_m_handle;
static Bool i2cTransCompleteFlag = false;
static Bool i2cTransTimoutFlag = false;
static Bool i2cComFlowError = false;

//-----------------------------------------------------------------------------
// CONSTANT DATA DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PUBLIC FUNCTIONS
//-----------------------------------------------------------------------------

//----------------------------------------------------------------------------
void InitI2C_Controller(void)
//----------------------------------------------------------------------------
{
   lpi2c_master_config_t  masterConfig = {0};
   //UInt32 clockMcr0Register;
   
   //SetOutputPin(eTOUCH_RST_n, 1);
   
   CLOCK_SetMux(kCLOCK_Lpi2cMux, LPI2C_CLOCK_SOURCE_SELECT);
   CLOCK_SetDiv(kCLOCK_Lpi2cDiv, LPI2C_CLOCK_SOURCE_DIVIDER);
   
   LPI2C_MasterGetDefaultConfig(&masterConfig);
   masterConfig.baudRate_Hz = LPI2C_BAUDRATE;
   LPI2C_MasterInit(LPI2C_MASTER_BASEADDR, &masterConfig, LPI2C_CLOCK_FREQUENCY);
   
   // to correct the clock symetry we adjust the clk hi, lo register manually.   
   /*
   clockMcr0Register = LPI2C_MASTER_BASEADDR->MCCR0;
   clockMcr0Register &= ~(0x00FFFF);
   clockMcr0Register |= LPI2C_MCCR0_CLKHI(0x16);
   clockMcr0Register |= LPI2C_MCCR0_CLKLO(0x18);
   LPI2C_MASTER_BASEADDR->MCCR0 = clockMcr0Register;
*/

}

//----------------------------------------------------------------------------
void UpdateFlowSensor_I2C(void)
//----------------------------------------------------------------------------
{
   static UInt8 step = 0;
   static UInt8 flowRxBuff[3];
   static UInt8 flowTxBuff[2];
   static UInt16 flowSensorValue;
   //static UInt16 tempSensorValue;
   //static UInt8 crc;
   
   switch(step)
   {
   case 0:
      flowTxBuff[0] = 0x10;
      flowTxBuff[1] = 0x00;
      if(I2C_Send(I2C_FLOW_SLAVE_ADR_7BIT, 0, 0, flowTxBuff, 2))        // 0x1000 flow register
      {
         step = 1;
      }
      break;
      
   case 1:
      if(I2C_Receive(I2C_FLOW_SLAVE_ADR_7BIT, 0, 0, flowRxBuff, 3))        // 0x1000 flow register
      {
         flowSensorValue = (flowRxBuff[0] << 8) | flowRxBuff[1];
         GetIOData()->analogIn.dispersionGasFlow = flowSensorValue;
         //crc = flowRxBuff[2];
         step = 1;
      }
      break;
     /*
   case 2:
      if(I2C_Receive(I2C_FLOW_SLAVE_ADR_7BIT, 0x1001, 2, tempRxBuff, 3))        // 0x1000 flow register
      {
         tempSensorValue =  (tempRxBuff[0] << 8) | tempRxBuff[1];
         crc = tempRxBuff[2];
         step = 0;
      }
      break;
*/
   }
}

//-----------------------------------------------------------------------------
// LOCAL FUNCTIONS
//-----------------------------------------------------------------------------
static void lpi2c_master_callback(LPI2C_Type *base, lpi2c_master_handle_t *handle, status_t status, void *userData)
{
   if (status == kStatus_Success)
   {
      i2cTransCompleteFlag = true;
      i2cTransTimoutFlag = false;
   }
   else // timeout, fail, invalid argument, ...
   {
      i2cTransCompleteFlag = false;
      i2cTransTimoutFlag = true;
   }
}

//-----------------------------------------------------------------------------
static Bool I2C_Send(UInt8 slaveAdr, UInt32 subAdr, UInt32 subAdrSize, UInt8 *txDataBuff, UInt8 txDataLen)
//-----------------------------------------------------------------------------
{
   static Int32 i2cTxStep = 0;
   static Int32 busyTimer = 0;
   Bool i2cTxResult;
   status_t reVal = kStatus_Fail;

   switch(i2cTxStep)
   {
   case 0:
      // start + slaveaddress(w) + subAddress + repeated start + slaveaddress(r) + rx data buffer + stop
      LPI2C_MasterTransferCreateHandle(LPI2C_MASTER_BASEADDR, &g_m_handle, lpi2c_master_callback, NULL);
      masterXfer.slaveAddress = slaveAdr;
      masterXfer.direction = kLPI2C_Write;
      masterXfer.subaddress = (uint32_t)subAdr;
      masterXfer.subaddressSize = subAdrSize;
      masterXfer.data = txDataBuff;
      masterXfer.dataSize = txDataLen;
      masterXfer.flags = kLPI2C_TransferDefaultFlag;
      
      i2cTransCompleteFlag = false;
      reVal = LPI2C_MasterTransferNonBlocking(LPI2C_MASTER_BASEADDR, &g_m_handle, &masterXfer);
      
      // if bus busy or already non blocking transfer in progress - wait in this step
      if (reVal == kStatus_LPI2C_Busy)
      {
         busyTimer++;
         if(busyTimer > 10)
         {
            busyTimer = 0;
            i2cTxStep++;
            i2cTransTimoutFlag = true;
            LPI2C_MasterTransferAbort(LPI2C_MASTER_BASEADDR, &g_m_handle);
            InitI2C_Controller();
         }
         else
         {
            i2cTxStep = 0;
         }
      }
      else
      {
         i2cTxStep++;
      }
      i2cTxResult = false;
      break;
      
   case 1:
      if(i2cTransCompleteFlag)
      {
         if(masterXfer.slaveAddress == I2C_FLOW_SLAVE_ADR_7BIT){
            i2cComFlowError = false;
         }
         i2cTxResult = true;
         i2cTxStep = 0;
      }
      else if(i2cTransTimoutFlag)
      {
         if(masterXfer.slaveAddress == I2C_FLOW_SLAVE_ADR_7BIT){
            i2cComFlowError = true;
         }
         i2cTxResult = true;
         i2cTxStep = 0;
      }
      else
      {
         i2cTxResult = false;
      }
      break;
   }

   return i2cTxResult;
}

//-----------------------------------------------------------------------------
static Bool I2C_Receive(UInt8 slaveAdr, UInt32 subAdr, UInt32 subAdrSize, UInt8 *rxDataBuff, UInt8 rxDataLen)
//-----------------------------------------------------------------------------
{
   static Int32 i2cRxStep = 0;
   static Int32 busyTimer = 0;
   Bool i2cRxResult;
   status_t reVal = kStatus_Fail;

   switch(i2cRxStep)
   {
   case 0:
      // start + slaveaddress(w) + subAddress + repeated start + slaveaddress(r) + rx data buffer + stop
      LPI2C_MasterTransferCreateHandle(LPI2C_MASTER_BASEADDR, &g_m_handle, lpi2c_master_callback, NULL);
      masterXfer.slaveAddress = slaveAdr;
      masterXfer.direction = kLPI2C_Read;
      masterXfer.subaddress = (uint32_t)subAdr;
      masterXfer.subaddressSize = subAdrSize;
      masterXfer.data = rxDataBuff;
      masterXfer.dataSize = rxDataLen;
      masterXfer.flags = kLPI2C_TransferDefaultFlag;
      
      i2cTransCompleteFlag = false;
      reVal = LPI2C_MasterTransferNonBlocking(LPI2C_MASTER_BASEADDR, &g_m_handle, &masterXfer);
      
      // if bus busy or already non blocking transfer in progress - wait in this step
      if (reVal == kStatus_LPI2C_Busy)
      {
         busyTimer++;
         if(busyTimer > 10)
         {
            busyTimer = 0;
            i2cRxStep++;
            i2cTransTimoutFlag = true;
            LPI2C_MasterTransferAbort(LPI2C_MASTER_BASEADDR, &g_m_handle);
            InitI2C_Controller();
         }
         else
         {
            i2cRxStep = 0;
         }
      }
      else
      {
         i2cRxStep++;
      }
      i2cRxResult = false;
      break;
      
   case 1:
      if(i2cTransCompleteFlag)
      {
         if(masterXfer.slaveAddress == I2C_FLOW_SLAVE_ADR_7BIT){
            i2cComFlowError = false;
         }
         i2cRxResult = true;
         i2cRxStep = 0;
      }
      else if(i2cTransTimoutFlag)
      {
         if(masterXfer.slaveAddress == I2C_FLOW_SLAVE_ADR_7BIT){
            i2cComFlowError = true;
         }
         i2cRxResult = true;
         i2cRxStep = 0;
      }
      else
      {
         i2cRxResult = false;
      }
      break;
   }

   return i2cRxResult;
}

