/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------
Autor                  Datum
Dominik Benz           19.09.2019

-- BESCHREIBUNG ---------------------------------------------------------------

PWM Funktionen

-----------------------------------------------------------------------------*/

//-----------------------------------------------------------------------------
// INCLUDES
//-----------------------------------------------------------------------------
#include "Pwm.h"
#include "Hardware.h"
#include "Gpio.h"
#include "fsl_qtmr.h"

//-----------------------------------------------------------------------------
// MACRO DEFINITIONS
//-----------------------------------------------------------------------------
#define QTMR_SOURCE_CLOCK       CLOCK_GetFreq(kCLOCK_IpgClk)/8
#define PWM_LED_HZ              (UInt32)100
#define PWM_LED1_TIMER          TMR2
#define PWM_LED1_TIMER_CH       kQTMR_Channel_1
#define PWM_LED2_TIMER          TMR2
#define PWM_LED2_TIMER_CH       kQTMR_Channel_2

#define PWM_VALVE_HZ            (UInt32)1500
#define PWM_VAVLE_TIMER         TMR1
#define PWM_VALVE_CH1           kQTMR_Channel_0
#define PWM_VALVE_CH2           kQTMR_Channel_2


//-----------------------------------------------------------------------------
// LOCAL TYPE DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// LOCAL FUNCTION PROTOTYPES
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// LOCAL VARIABLE DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// CONSTANT DATA DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PUBLIC FUNCTIONS
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void InitializePwm(void)
//-----------------------------------------------------------------------------
{
   qtmr_config_t qtmrConfig;
   
   qtmrConfig.primarySource = kQTMR_ClockDivide_8;
   QTMR_Init(PWM_LED1_TIMER, PWM_LED1_TIMER_CH, &qtmrConfig);
   QTMR_Init(PWM_LED2_TIMER, PWM_LED2_TIMER_CH, &qtmrConfig);
   
   QTMR_Init(PWM_VAVLE_TIMER, PWM_VALVE_CH1, &qtmrConfig);
   QTMR_Init(PWM_VAVLE_TIMER, PWM_VALVE_CH2, &qtmrConfig);
}

//-----------------------------------------------------------------------------
void UpdatePwmLed1(UInt8 dutyCycle)
//-----------------------------------------------------------------------------
{
   uint32_t periodCount, highCount, lowCount;
   
   periodCount = QTMR_SOURCE_CLOCK / PWM_LED_HZ;
   highCount = (periodCount * (uint32_t)dutyCycle) / 100;
   lowCount = periodCount - highCount;

   PWM_LED1_TIMER->CHANNEL[PWM_LED1_TIMER_CH].CMPLD1 = lowCount;
   PWM_LED1_TIMER->CHANNEL[PWM_LED1_TIMER_CH].CMPLD2 = highCount;
}

//-----------------------------------------------------------------------------
void UpdatePwmLed2(UInt8 dutyCycle)
//-----------------------------------------------------------------------------
{
   uint32_t periodCount, highCount, lowCount;
   
   periodCount = QTMR_SOURCE_CLOCK / PWM_LED_HZ;
   highCount = (periodCount * (uint32_t)dutyCycle) / 100;
   lowCount = periodCount - highCount;

   PWM_LED2_TIMER->CHANNEL[PWM_LED2_TIMER_CH].CMPLD1 = lowCount;
   PWM_LED2_TIMER->CHANNEL[PWM_LED2_TIMER_CH].CMPLD2 = highCount;

}

//-----------------------------------------------------------------------------
void StartPwmLed1(UInt8 dutyCycle)
//-----------------------------------------------------------------------------
{
   QTMR_SetupPwm(PWM_LED1_TIMER, PWM_LED1_TIMER_CH, PWM_LED_HZ, dutyCycle, false, QTMR_SOURCE_CLOCK);
   QTMR_StartTimer(PWM_LED1_TIMER, PWM_LED1_TIMER_CH, kQTMR_PriSrcRiseEdge);
}

//-----------------------------------------------------------------------------
void StartPwmLed2(UInt8 dutyCycle)
//-----------------------------------------------------------------------------
{
   QTMR_SetupPwm(PWM_LED2_TIMER, PWM_LED2_TIMER_CH, PWM_LED_HZ, dutyCycle, false, QTMR_SOURCE_CLOCK);
   QTMR_StartTimer(PWM_LED2_TIMER, PWM_LED2_TIMER_CH, kQTMR_PriSrcRiseEdge);
}

//-----------------------------------------------------------------------------
void StopPwmLed1(void)
//-----------------------------------------------------------------------------
{
   QTMR_SetupPwm(PWM_LED1_TIMER, PWM_LED1_TIMER_CH, PWM_LED_HZ, 0, false, QTMR_SOURCE_CLOCK);
   QTMR_StopTimer(PWM_LED1_TIMER, PWM_LED1_TIMER_CH);
}

//-----------------------------------------------------------------------------
void StopPwmLed2(void)
//-----------------------------------------------------------------------------
{
   QTMR_SetupPwm(PWM_LED2_TIMER, PWM_LED2_TIMER_CH, PWM_LED_HZ, 0, false, QTMR_SOURCE_CLOCK);
   QTMR_StopTimer(PWM_LED2_TIMER, PWM_LED2_TIMER_CH);
}


//-----------------------------------------------------------------------------
void UpdatePwmValve1(UInt16 dutyCycle)
//-----------------------------------------------------------------------------
{
   UInt16 periodCount, highCount, lowCount;
   
   // setup the high/low register
   /* Counter values to generate a PWM signal */
   periodCount = (QTMR_SOURCE_CLOCK / PWM_VALVE_HZ);
   highCount   = (periodCount * dutyCycle) / 1000U;
   lowCount    = periodCount - highCount;

   /* Setup the compare registers for PWM output */
   PWM_VAVLE_TIMER->CHANNEL[PWM_VALVE_CH1].COMP1 = lowCount - 1U;;
   PWM_VAVLE_TIMER->CHANNEL[PWM_VALVE_CH1].COMP2 = highCount  - 1U;;

   /* Setup the pre-load registers for PWM output */
   PWM_VAVLE_TIMER->CHANNEL[PWM_VALVE_CH1].CMPLD1 = lowCount - 1U;;
   PWM_VAVLE_TIMER->CHANNEL[PWM_VALVE_CH1].CMPLD2 = highCount - 1U;;
}

//-----------------------------------------------------------------------------
void StartPwmValve1(UInt16 dutyCycle)
//-----------------------------------------------------------------------------
{
   UInt16 periodCount, highCount, lowCount;
   UInt16 reg;
   
   // set oflag
   /* Set OFLAG pin for output mode and force out a low on the pin */
   PWM_VAVLE_TIMER->CHANNEL[PWM_VALVE_CH1].SCTRL |= (TMR_SCTRL_FORCE_MASK | TMR_SCTRL_OEN_MASK);
   
   // setup the high/low register
   periodCount = (QTMR_SOURCE_CLOCK / PWM_VALVE_HZ);
   highCount   = (periodCount * dutyCycle) / 1000U;
   lowCount    = periodCount - highCount;

   /* Setup the compare registers for PWM output */
   PWM_VAVLE_TIMER->CHANNEL[PWM_VALVE_CH1].COMP1 = lowCount - 1U;;
   PWM_VAVLE_TIMER->CHANNEL[PWM_VALVE_CH1].COMP2 = highCount - 1U;;

   /* Setup the pre-load registers for PWM output */
   PWM_VAVLE_TIMER->CHANNEL[PWM_VALVE_CH1].CMPLD1 = lowCount - 1U;;
   PWM_VAVLE_TIMER->CHANNEL[PWM_VALVE_CH1].CMPLD2 = highCount - 1U;;
   
   // get the acutal CSCTRL regiser and set it up to capture compare 
   reg = PWM_VAVLE_TIMER->CHANNEL[PWM_VALVE_CH1].CSCTRL;   
   reg &= (uint16_t)(~(TMR_CSCTRL_CL1_MASK | TMR_CSCTRL_CL2_MASK));
   reg |= (TMR_CSCTRL_CL1(kQTMR_LoadOnComp2) | TMR_CSCTRL_CL2(kQTMR_LoadOnComp1));
   PWM_VAVLE_TIMER->CHANNEL[PWM_VALVE_CH1].CSCTRL = reg;
   
   /* True polarity, no inversion */
   PWM_VAVLE_TIMER->CHANNEL[PWM_VALVE_CH1].SCTRL &= ~(uint16_t)TMR_SCTRL_OPS_MASK;
   
   // get the actual CTRL register
   reg = PWM_VAVLE_TIMER->CHANNEL[PWM_VALVE_CH1].CTRL;
   reg &= ~(uint16_t)TMR_CTRL_OUTMODE_MASK;
   /* Count until compare value is  reached and re-initialize the counter, toggle OFLAG output
   * using alternating compare register
   */
   reg |= (TMR_CTRL_LENGTH_MASK | TMR_CTRL_OUTMODE(kQTMR_ToggleOnAltCompareReg));
   PWM_VAVLE_TIMER->CHANNEL[PWM_VALVE_CH1].CTRL = reg;

   QTMR_StartTimer(PWM_VAVLE_TIMER, PWM_VALVE_CH1, kQTMR_PriSrcRiseEdge);
}

//-----------------------------------------------------------------------------
void StopPwmValve1(void)
//-----------------------------------------------------------------------------
{
   QTMR_SetupPwm(PWM_VAVLE_TIMER, PWM_VALVE_CH1, PWM_VALVE_HZ, 0, false, QTMR_SOURCE_CLOCK);
   QTMR_StopTimer(PWM_VAVLE_TIMER, PWM_VALVE_CH1);
}