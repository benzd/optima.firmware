/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------
Autor                  Datum
Fitze Ernst            9.08.2019

-- BESCHREIBUNG ---------------------------------------------------------------



-- AENDERUNGEN ----------------------------------------------------------------
Autor                  Datum

-----------------------------------------------------------------------------*/

#include "MacAddress.h"

#include "string.h"
#include "CoreBase.h"
#include "I2C_Poll.h"


//---------------------------------------------------------------------------------------
#define DEVICE_ADR       0xA2


//---------------------------------------------------------------------------------------
static UInt8 s_macAdr[6];
static Bool  s_macAdrOK = FALSE;

//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------
Bool InitMacAddress(void)
//-----------------------------------------------------------------------------
// MAC Adresse lesen:
{
   SDAStart();
   if (!SDASendByte(DEVICE_ADR)) 
      return FALSE;   // Bausteinadresse
   if (!SDASendByte(0xfa)) 
      return FALSE;   // interne Adresse der MAC
   SDAStart();
   if (!SDASendByte(DEVICE_ADR + 1)) 
      return FALSE;   // Bausteinadresse + Read-Bit
   s_macAdr[0] = SDAReadByte(1);
   s_macAdr[1] = SDAReadByte(1);
   s_macAdr[2] = SDAReadByte(1);
   s_macAdr[3] = SDAReadByte(1);
   s_macAdr[4] = SDAReadByte(1);
   s_macAdr[5] = SDAReadByte(0);
   SDAStop();
   if ((s_macAdr[0] == s_macAdr[1]) && (s_macAdr [0] == s_macAdr[2])) 
   {  // alle 3 sind gleich
      if (s_macAdr[0] == 0 || s_macAdr[0] == 0xFF)
      {
         return FALSE;
      }
   }
   if ((s_macAdr[3] == s_macAdr[4]) && (s_macAdr [3] == s_macAdr[5])) 
   {  // alle 3 sind gleich
      if (s_macAdr[3] == 0 || s_macAdr[3] == 0xFF)
      {
         return FALSE;
      }
   }  
   s_macAdrOK = TRUE;
   return TRUE;
}

//-----------------------------------------------------------------------------
Bool GetMacAddress(UInt8 mac[6])
//-----------------------------------------------------------------------------
{
   if (mac)
      memcpy(mac, s_macAdr, sizeof(s_macAdr));
   return s_macAdrOK;
}

