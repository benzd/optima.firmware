/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------
Autor                  Datum
Fitze Ernst            9.08.2019

-- BESCHREIBUNG ---------------------------------------------------------------



-- AENDERUNGEN ----------------------------------------------------------------
Autor                  Datum

-----------------------------------------------------------------------------*/

#include "PT1000.h"
#include "Hardware.h"
#include "fsl_clock.h"
#include "fsl_lpspi.h"
#include "math.h"


//-----------------------------------------------------------------------------
#define EXAMPLE_LPSPI_CLOCK_SOURCE_SELECT (1)
// Clock divider for master lpspi clock source 
#define EXAMPLE_LPSPI_CLOCK_SOURCE_DIVIDER (7)
#define LPSPI_MASTER_CLK_FREQ (CLOCK_GetFreq(kCLOCK_Usb1PllPfd0Clk) / (EXAMPLE_LPSPI_CLOCK_SOURCE_DIVIDER + 1))
#define EXAMPLE_LPSPI_MASTER_BASEADDR (LPSPI3)
#define EXAMPLE_LPSPI_MASTER_IRQN LPSPI3_IRQn
#define EXAMPLE_LPSPI_MASTER_PCS_FOR_INIT (kLPSPI_Pcs0)
#define EXAMPLE_LPSPI_MASTER_PCS_FOR_TRANSFER (kLPSPI_MasterPcs0)
#define TRANSFER_BAUDRATE 100000 // Transfer baudrate - 500k 
#define RECEIVE_SIZE 1
#define A                       (Float32)0.00390802
#define B                       (Float32)(-0.0000005775)
#define R0                      (Float32)1000.0

//-----------------------------------------------------------------------------
UInt32 masterRxData[RECEIVE_SIZE];
UInt32 masterTxData[RECEIVE_SIZE];
//UInt32 m_channelConvert[4] = {0xB0, 0xB8, 0xB1, 0xB9};
UInt32 m_channelConvert[2] = {0xB0, 0xB8};
UInt32 m_channelValueRaw[2];
UInt32 m_channelValue[2];
UInt32 m_channelNumberCnv = 0;
UInt32 m_channelNumberCnvDone = 2;
UInt32 masterRxCount = 0;

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void LPSPI3_IRQHandler(void)
//-----------------------------------------------------------------------------
{
   UInt32 val;
   if (masterRxCount < RECEIVE_SIZE)
   {
      // First, disable the interrupts to avoid potentially triggering another interrupt
      // while reading out the RX FIFO as more data may be coming into the RX FIFO. We'll
      // re-enable the interrupts EXAMPLE_LPSPI_MASTER_BASEADDRd on the LPSPI state after reading out the FIFO.
      LPSPI_DisableInterrupts(EXAMPLE_LPSPI_MASTER_BASEADDR, kLPSPI_RxInterruptEnable);
      
      while (LPSPI_GetRxFifoCount(EXAMPLE_LPSPI_MASTER_BASEADDR))
      {
         // Read out the data
         val = LPSPI_ReadData(EXAMPLE_LPSPI_MASTER_BASEADDR);       
         masterRxData[masterRxCount] = val;
         masterRxCount++;
         
         if (masterRxCount == RECEIVE_SIZE)
         {
            break;
         }
      }     
   }
   if (masterRxCount < RECEIVE_SIZE)
   {
      // Set the TDF and RDF interrupt enables simultaneously to avoid race conditions 
      LPSPI_EnableInterrupts(EXAMPLE_LPSPI_MASTER_BASEADDR, kLPSPI_RxInterruptEnable);
   }
   else
   {   
      LPSPI_DisableInterrupts(EXAMPLE_LPSPI_MASTER_BASEADDR, kLPSPI_AllInterruptEnable);
   }
}

//----------------------------------------------------------------------------
static void StartConvert(void)
//----------------------------------------------------------------------------
{
   masterRxCount = 0;
   LPSPI_WriteData(EXAMPLE_LPSPI_MASTER_BASEADDR, (m_channelConvert[m_channelNumberCnv] << 16) | 0x8000 );
   LPSPI_EnableInterrupts(EXAMPLE_LPSPI_MASTER_BASEADDR, kLPSPI_RxInterruptEnable);  
}

//----------------------------------------------------------------------------
static UInt32 IncChannelNumber(UInt32 number)
//----------------------------------------------------------------------------
{
   if (number < 2)
      return number + 1;
   return 0;
}

static const Float32 c_RL = 0.0;
static const Float32 c_1  = 12.83421 * 1000000000.0;
static const Float32 c_2  = 2.98 * 1000000;
static const Float32 c_3  = 2.9890 * 1000000;
//----------------------------------------------------------------------------
static void Calculate(void)
//----------------------------------------------------------------------------
{
   UInt32 sig_msb, channel;
   UInt32 D;
   
   for (channel = 0; channel < 2; channel++)
   {
      sig_msb = m_channelValueRaw[channel] >> 20;
      if (sig_msb == 2)
      {
         D = (m_channelValueRaw[channel] >> 4) & 0xFFFF;
         m_channelValue[channel] = (UInt32)( (D*c_1 / (c_2 * 0x20000 - D*c_3)) - c_RL );  // Ohm
      }
      else // if (sig_msb == 3)
      {  // Kabelbruch. Sensor nicht angeschlossen
         m_channelValue[channel] = 0;
      }
   }
   
   GetIOData()->analogIn.inletTemperatureOhm = m_channelValue[0];
   GetIOData()->analogIn.outletTemperatureOhm = m_channelValue[1];
   GetIOData()->analogIn.inletTemperatureCelsius = (-R0 * A + sqrt(R0*R0*A*A - 4*R0*B*(R0-m_channelValue[0])))/(2*R0*B);
   GetIOData()->analogIn.outletTemperaturCelsius = (-R0 * A + sqrt(R0*R0*A*A - 4*R0*B*(R0-m_channelValue[1])))/(2*R0*B);
}

//----------------------------------------------------------------------------
void UpdatePT1000(void)
//----------------------------------------------------------------------------
{
   static UInt8 countConversions = 0; 
   if (masterRxCount)
   {
      m_channelValueRaw[m_channelNumberCnvDone] = masterRxData[0];
      m_channelNumberCnv      = IncChannelNumber(m_channelNumberCnv);
      m_channelNumberCnvDone  = IncChannelNumber(m_channelNumberCnvDone);
      StartConvert();

      if (countConversions < 8)
      {
         countConversions++;
         return;
      }      
      Calculate();
   }
   else
   {
      StartConvert();
   }      
}

//----------------------------------------------------------------------------
UInt32 GetSPIChannelRaw(UInt32 channel)
//----------------------------------------------------------------------------
{
   if (channel < 2)
      return (m_channelValueRaw[channel] >> 4) & 0xFFFF;
   return 0;
}

//----------------------------------------------------------------------------
UInt32 GetTemperatureBase(void)
//----------------------------------------------------------------------------
{
   return m_channelValue[0];
}
//----------------------------------------------------------------------------
UInt32 GetTemperatureTop(void)
//----------------------------------------------------------------------------
{
   return m_channelValue[1];
}

//----------------------------------------------------------------------------
void InitPT1000(void)
//----------------------------------------------------------------------------
{
   lpspi_master_config_t   masterConfig;
   UInt32 whichPcs;
   UInt32 srcClock_Hz;
   
   memset(m_channelValueRaw, 0, sizeof(m_channelValueRaw));
   memset(m_channelValue, 0, sizeof(m_channelValue));
   CLOCK_SetMux(kCLOCK_LpspiMux, EXAMPLE_LPSPI_CLOCK_SOURCE_SELECT);
   CLOCK_SetDiv(kCLOCK_LpspiDiv, EXAMPLE_LPSPI_CLOCK_SOURCE_DIVIDER);
   
   //////////////////////////////////////////////////////////////////
   // SPI 3 master config
   //////////////////////////////////////////////////////////////////
   masterConfig.baudRate = TRANSFER_BAUDRATE;
   masterConfig.bitsPerFrame = 24;
   masterConfig.cpol = kLPSPI_ClockPolarityActiveHigh;
   masterConfig.cpha = kLPSPI_ClockPhaseFirstEdge;
   masterConfig.direction = kLPSPI_MsbFirst;
   
   masterConfig.pcsToSckDelayInNanoSec = 1000000000 / masterConfig.baudRate;
   masterConfig.lastSckToPcsDelayInNanoSec = 1000000000 / masterConfig.baudRate;
   masterConfig.betweenTransferDelayInNanoSec = 1000000000 / masterConfig.baudRate;
   
   masterConfig.whichPcs = EXAMPLE_LPSPI_MASTER_PCS_FOR_INIT;
   masterConfig.pcsActiveHighOrLow = kLPSPI_PcsActiveLow;
   
   masterConfig.pinCfg = kLPSPI_SdiInSdoOut;
   masterConfig.dataOutConfig = kLpspiDataOutRetained;
   
   srcClock_Hz = LPSPI_MASTER_CLK_FREQ;
   LPSPI_MasterInit(EXAMPLE_LPSPI_MASTER_BASEADDR, &masterConfig, srcClock_Hz);

   whichPcs = EXAMPLE_LPSPI_MASTER_PCS_FOR_INIT;
   LPSPI_SetFifoWatermarks(EXAMPLE_LPSPI_MASTER_BASEADDR, 1, 1);
   
   LPSPI_Enable(EXAMPLE_LPSPI_MASTER_BASEADDR, false);
   EXAMPLE_LPSPI_MASTER_BASEADDR->CFGR1 &= (~LPSPI_CFGR1_NOSTALL_MASK);
   LPSPI_Enable(EXAMPLE_LPSPI_MASTER_BASEADDR, true);
   
   //Flush FIFO , clear status , disable all the inerrupts.
   LPSPI_FlushFifo(EXAMPLE_LPSPI_MASTER_BASEADDR, true, true);
   LPSPI_ClearStatusFlags(EXAMPLE_LPSPI_MASTER_BASEADDR, kLPSPI_AllStatusFlag);
   LPSPI_DisableInterrupts(EXAMPLE_LPSPI_MASTER_BASEADDR, kLPSPI_AllInterruptEnable);
   
   EXAMPLE_LPSPI_MASTER_BASEADDR->TCR =
      (EXAMPLE_LPSPI_MASTER_BASEADDR->TCR &
       ~(LPSPI_TCR_CONT_MASK | LPSPI_TCR_CONTC_MASK | LPSPI_TCR_RXMSK_MASK | LPSPI_TCR_PCS_MASK)) |
         LPSPI_TCR_CONT(0) | LPSPI_TCR_CONTC(0) | LPSPI_TCR_RXMSK(0) | LPSPI_TCR_TXMSK(0) | LPSPI_TCR_PCS(whichPcs);
   
   // Enable the NVIC for LPSPI peripheral. Note that below code is useless if the LPSPI interrupt is in INTMUX ,
   // and you should also enable the INTMUX interupt in your application.
   EnableIRQ(EXAMPLE_LPSPI_MASTER_IRQN);
   while (LPSPI_GetTxFifoCount(EXAMPLE_LPSPI_MASTER_BASEADDR) != 0)
   {
   }

   StartConvert();

}

