/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------
Autor                  Datum
Fitze Ernst            9.08.2019

-- BESCHREIBUNG ---------------------------------------------------------------



-- AENDERUNGEN ----------------------------------------------------------------
Autor                  Datum

-----------------------------------------------------------------------------*/
#include "Gpio.h"
#include "board.h"
#include "fsl_gpio.h"
#include "CoreBase.h"

//-----------------------------------------------------------------------------
static PinType a_Pins[eLast];

//-----------------------------------------------------------------------------
PinType* GetPinType(PinNumberType pin)
//-----------------------------------------------------------------------------
{
   return &a_Pins[pin];
}

//-----------------------------------------------------------------------------
void SetOutputPin(PinNumberType pin, UInt32 val)
//-----------------------------------------------------------------------------
{
   if (val < 2)
      GPIO_PinWrite(a_Pins[pin].pRegister, a_Pins[pin].pinNumber, val);
   else
   {
      GPIO_PinWrite(a_Pins[pin].pRegister, a_Pins[pin].pinNumber, !(GPIO1->DR & (1 << a_Pins[pin].pinNumber)));
   }
}
//-----------------------------------------------------------------------------
UInt32 ReadInputPin(PinNumberType pin)
//-----------------------------------------------------------------------------
{
   return GPIO_PinRead(a_Pins[pin].pRegister, a_Pins[pin].pinNumber);
}

//-----------------------------------------------------------------------------
Bool InitGPIO(void)
//-----------------------------------------------------------------------------
{
   gpio_pin_config_t sw_configOut    = { kGPIO_DigitalOutput, 0, kGPIO_NoIntmode, };

   // define output pins
   /*
   a_Pins[eEXP_SCL].pRegister = GPIO1;       a_Pins[eEXP_SCL].pinNumber = 9;
   a_Pins[eROT_RST_n].pRegister = GPIO2;     a_Pins[eROT_RST_n].pinNumber = 10;
   a_Pins[eDRV_FAN].pRegister = GPIO2;       a_Pins[eDRV_FAN].pinNumber = 6;
   a_Pins[eHEAT_AC_EN].pRegister = GPIO2;    a_Pins[eHEAT_AC_EN].pinNumber = 15;
   a_Pins[eHEAT_AC].pRegister = GPIO2;       a_Pins[eHEAT_AC].pinNumber = 18;
   a_Pins[ePHY_RESET].pRegister = GPIO1;     a_Pins[ePHY_RESET].pinNumber = 8;
   */
   a_Pins[eDRV_RST_n].pRegister = GPIO2;     a_Pins[eDRV_RST_n].pinNumber = 9;
   a_Pins[eDRV_EN_n].pRegister = GPIO2;      a_Pins[eDRV_EN_n].pinNumber = 8;
   a_Pins[eLED_FW].pRegister = GPIO1;        a_Pins[eLED_FW].pinNumber = 11;
   a_Pins[eTOUCH_RST_n].pRegister = GPIO2;   a_Pins[eTOUCH_RST_n].pinNumber = 7;
   a_Pins[eHEAT_SSR1].pRegister = GPIO2;     a_Pins[eHEAT_SSR1].pinNumber = 4;
   a_Pins[eHEAT_SSR2].pRegister = GPIO2;     a_Pins[eHEAT_SSR2].pinNumber = 5;

   
   // define input pins
   a_Pins[eROT_ZERO].pRegister = GPIO1;      a_Pins[eROT_ZERO].pinNumber = 25;  
   /*
   a_Pins[eENC_C].pRegister = GPIO1;         a_Pins[eENC_C].pinNumber = 30;  
   a_Pins[eFAN_IN].pRegister = GPIO1;        a_Pins[eFAN_IN].pinNumber = 14;  
   a_Pins[eROT_ZERO].pRegister = GPIO1;      a_Pins[eROT_ZERO].pinNumber = 25;  
   a_Pins[eDRV_FAULT_n].pRegister = GPIO1;   a_Pins[eDRV_FAULT_n].pinNumber = 22;  
   a_Pins[eHEAT_DC1_IN].pRegister = GPIO1;   a_Pins[eHEAT_DC1_IN].pinNumber = 29;
   a_Pins[eHEAT_DC2_IN].pRegister = GPIO1;   a_Pins[eHEAT_DC2_IN].pinNumber = 31;
   a_Pins[eACCL_INT1].pRegister = GPIO1;     a_Pins[eACCL_INT1].pinNumber = 20;
   a_Pins[eHW_ID_0].pRegister = GPIO5;       a_Pins[eHW_ID_0].pinNumber = 2;
   a_Pins[eHW_ID_1].pRegister = GPIO5;       a_Pins[eHW_ID_1].pinNumber = 1;
   a_Pins[eROT_FAULT_n].pRegister = GPIO1;   a_Pins[eROT_FAULT_n].pinNumber = 21;
   */

   // init output pins
   /*
   GPIO_PinInit(a_Pins[eEXP_SCL].pRegister, a_Pins[eEXP_SCL].pinNumber, &sw_configOut);
   GPIO_PinInit(a_Pins[eDRV_FAN].pRegister, a_Pins[eDRV_FAN].pinNumber, &sw_configOut); 
   GPIO_PinInit(a_Pins[eROT_RST_n].pRegister, a_Pins[eROT_RST_n].pinNumber, &sw_configOut);
   GPIO_PinInit(a_Pins[eHEAT_AC_EN].pRegister, a_Pins[eHEAT_AC_EN].pinNumber, &sw_configOut); 
   GPIO_PinInit(a_Pins[eHEAT_AC].pRegister, a_Pins[eHEAT_AC].pinNumber, &sw_configOut); 
   GPIO_PinInit(a_Pins[ePHY_RESET].pRegister, a_Pins[ePHY_RESET].pinNumber, &sw_configOut); 
   */
   GPIO_PinInit(a_Pins[eDRV_RST_n].pRegister, a_Pins[eDRV_RST_n].pinNumber, &sw_configOut); 
   GPIO_PinInit(a_Pins[eDRV_EN_n].pRegister, a_Pins[eDRV_EN_n].pinNumber, &sw_configOut); 
   GPIO_PinInit(a_Pins[eLED_FW].pRegister, a_Pins[eLED_FW].pinNumber, &sw_configOut);
   GPIO_PinInit(a_Pins[eTOUCH_RST_n].pRegister, a_Pins[eTOUCH_RST_n].pinNumber, &sw_configOut); 
   GPIO_PinInit(a_Pins[eHEAT_SSR1].pRegister, a_Pins[eHEAT_SSR1].pinNumber, &sw_configOut); 
   GPIO_PinInit(a_Pins[eHEAT_SSR2].pRegister, a_Pins[eHEAT_SSR2].pinNumber, &sw_configOut); 
   
   SetOutputPin(eDRV_RST_n, 1);
   SetOutputPin(eDRV_EN_n, 0);
   SetOutputPin(eTOUCH_RST_n, 1);
   SetOutputPin(eHEAT_SSR1, 0);
   SetOutputPin(eHEAT_SSR2, 0);
   
   return 1;
}


