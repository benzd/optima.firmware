/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------
Autor                  Datum
Fitze Ernst            2019

-- BESCHREIBUNG ---------------------------------------------------------------

Das Modul liest die Daten aus dem gesamten E2PROM und speicher diese im RAM.
Mit Read und Write hat man somit einen schnellen Zugriff auf das EEPROM.
Das EEPROM wird in Bl�cke unterteilt und ins RAM kopiert.

Beim lesen werden die Daten aus dem Schattenspeicher gelesen.
Falls der Block noch nicht im Schattenspeicher ist wir der Block sofort geladen.

Bei schreiben werden die Daten in den Schattenspeicher geschrieben und
im zyklisch Aufruf asynchron ins EEPROM geschrieben.

-- AENDERUNGEN ----------------------------------------------------------------
Autor                  Datum

-----------------------------------------------------------------------------*/
#pragma once

#include "BuchiTypes.h"

//---------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
Bool InitMacAddress(void);
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------
Bool GetMacAddress(UInt8 mac[6]);
//-----------------------------------------------------------------------------
