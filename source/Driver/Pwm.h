/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------
Autor                  Datum
Dominik Benz           19.09.2019

-- BESCHREIBUNG ---------------------------------------------------------------

Funktionen Heizungen

-- AENDERUNGEN ----------------------------------------------------------------
Autor                  Datum

-----------------------------------------------------------------------------*/
#pragma once

//-----------------------------------------------------------------------------
// INCLUDES
//-----------------------------------------------------------------------------
#include "BuchiTypes.h"

//-----------------------------------------------------------------------------
// GLOBAL TYPE DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PUBLIC FUNCTIONS
//-----------------------------------------------------------------------------
void InitializePwm(void);
void StartPwmLed1(UInt8 dutyCycle);
void StartPwmLed2(UInt8 dutyCycle);
void StopPwmLed1(void);
void StopPwmLed2(void);
void UpdatePwmLed1(UInt8 dutyCycle);
void UpdatePwmLed2(UInt8 dutyCycle);
void StartPwmValve1(UInt16 dutyCycle);
void StopPwmValve1(void);
void UpdatePwmValve1(UInt16 dutyCycle);