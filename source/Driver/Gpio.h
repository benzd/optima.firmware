/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------
Autor                  Datum
Fitze Ernst            2019

-- BESCHREIBUNG ---------------------------------------------------------------

Das Modul ist das GPIO Interface.

-- AENDERUNGEN ----------------------------------------------------------------
Autor                  Datum

-----------------------------------------------------------------------------*/
#pragma once

#include "BuchiTypes.h"

//-----------------------------------------------------------------------------
typedef struct _PinType
{ 
   void * pRegister;
   UInt32 pinNumber;
} PinType;

typedef enum 
{
   // Output
   eLED_FW,
   eEXP_SCL,
   eROT_RST_n,
   eDRV_EN_n,
   eDRV_RST_n,
   eDRV_FAN,
   eHEAT_AC_EN,
   eHEAT_AC,
   eTOUCH_RST_n,
   eHEAT_SSR1,
   eHEAT_SSR2,
   
   eLastOutput,
   
   // Input
   eENC_C,      // Drehrad Druckknopf
   eFAN_IN,
   eROT_ZERO,
   eDRV_FAULT_n,
   eHEAT_DC1_IN,
   eHEAT_DC2_IN,
   eACCL_INT1,
   eHW_ID_0,
   eHW_ID_1,
   eROT_FAULT_n,

   eLast
} PinNumberType;


//-----------------------------------------------------------------------------
Bool InitGPIO(void);
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
PinType* GetPinType(PinNumberType pin);
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void SetOutputPin(PinNumberType pin, UInt32 val);
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
UInt32 ReadInputPin(PinNumberType pin);
//-----------------------------------------------------------------------------
