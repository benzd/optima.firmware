/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------

Autor                  Datum

-- BESCHREIBUNG ---------------------------------------------------------------

-- AENDERUNGEN ----------------------------------------------------------------

Autor                  Datum

-----------------------------------------------------------------------------*/
//-----------------------------------------------------------------------------

#include "I2C_Poll.h"

#include "board.h"
#include "fsl_common.h"
#include "fsl_iomuxc.h"
#include "pin_mux.h"
#include "fsl_gpio.h"

#include "CoreBase.h"


//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
#define SCL_PORT BOARD_INITPINS_I2C_SCL_GPIO
#define SCL_PIN  BOARD_INITPINS_I2C_SCL_PIN
#define SDA_PORT BOARD_INITPINS_I2C_SDA_GPIO
#define SDA_PIN  BOARD_INITPINS_I2C_SDA_PIN
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
static void SetPinSCL(UInt8 val)
//----------------------------------------------------------------------------
{
   GPIO_PinWrite(SCL_PORT, SCL_PIN, val);
}
//----------------------------------------------------------------------------
static Bool GetPinSCL(void)
//----------------------------------------------------------------------------
{
   return GPIO_PinReadPadStatus(SCL_PORT, SCL_PIN);
}
#define SCL_0             SetPinSCL(0)
#define SCL_1             SetPinSCL(1)

//----------------------------------------------------------------------------
static void SetPinSDA(UInt8 val)
//----------------------------------------------------------------------------
{
   GPIO_PinWrite(SDA_PORT, SDA_PIN, val);
}
//----------------------------------------------------------------------------
static Bool GetPinSDA(void)
//----------------------------------------------------------------------------
{
   return GPIO_PinReadPadStatus(SDA_PORT, SDA_PIN);
}


//----------------------------------------------------------------------------
// Empirisch ermittelt: Bei  8 Durchl�ufen ca. 4 microsec bei V-850, 20 Mhz
// Empirisch ermittelt: Bei 11 Durchl�ufen ca. 90kHz f�r SCL
// Empirisch ermittelt: Bei 10 Durchl�ufen ca. 95kHz f�r SCL
//----------------------------------------------------------------------------
static void Wait4Microsec()
//----------------------------------------------------------------------------
{
   Timer_WaitUs(4);
}
static void Wait2Microsec()
//----------------------------------------------------------------------------
{
   Timer_WaitUs(2);
}

//----------------------------------------------------------------------------
static void SDALowLevel(void)
//----------------------------------------------------------------------------
{
   SetPinSDA(0); // zur Sicherheit, grunds�tzlich unn�tig, da immer 0 
}


//----------------------------------------------------------------------------
static void SDAHighImpedance(void)
//----------------------------------------------------------------------------
{
   SetPinSDA(1);
}

//------------------------------------------------------------------------------------
static void MakeStop(void)
//------------------------------------------------------------------------------------
{
   SDALowLevel();
   SCL_1;
   Wait2Microsec();
   Wait4Microsec();
   SDAHighImpedance();
}

//------------------------------------------------------------------------------------
// clock streching. warte bis clock leitung frei ist
//------------------------------------------------------------------------------------
static void SetSCL(void)
//------------------------------------------------------------------------------------
{
   UINT16 idx;
   
   SCL_1;
   for (idx=0; idx<5000; idx++)
   {
      if (GetPinSCL() == 1)
      { // der Slave hat die clock Leitung nun losgelassen
         return;
      }
      Wait2Microsec();
   }
   MakeStop(); // falls der Slave h�ngt, wird er zur�ckgesetzt.
}


//----------------------------------------------------------------------------
static void MakeClock(void)
//----------------------------------------------------------------------------
{
   // ohne Pause ergit sich eine Impulsl�nge von ca. 0.4 Mikrosekunden. 
   Wait4Microsec();
   SetSCL();
   Wait4Microsec();
   SCL_0;
}


//----------------------------------------------------------------------------
static unsigned char CheckAck(void)
//----------------------------------------------------------------------------
{
   unsigned char ok;
   SDAHighImpedance(); //MBMBMB
   Wait4Microsec();
   //SDAHighImpedance(); //MBMBMB
   SetSCL();
   Wait4Microsec();
   ok = 1 - GetPinSDA();
   SCL_0;
   Wait4Microsec();
   return ok;
}

//----------------------------------------------------------------------------
// F�hrt ein Acknowledge durch. Dies ist nach dem Lesen eines Bytes notwendig, wenn
// weitere Bytes direkt anschliessend gesendet werden. Die Datenleitung wird auf
// Null gezogen und ein Clock-Impuls erzeugt.
//----------------------------------------------------------------------------
static void DoAck(void)
//----------------------------------------------------------------------------
{
   SDALowLevel();
   MakeClock();
   SDAHighImpedance();
   Wait4Microsec(); //MBMBMB
}

//----------------------------------------------------------------------------
// F�hrt ein Not-Acknowledge durch. Dies ist nach dem Lesen eines Bytes notwendig, wenn
// kene weiteren Bytes direkt anschliessend gelesen werden sollen.
// F�r ein Not-Acknowledge wird ein 1-Bit gesendet, d.h. die Datenleitung wird frei-
// gegeben und ein Clock-Impuls erzeugt.
//----------------------------------------------------------------------------
static void DoNack(void)
//----------------------------------------------------------------------------
{
   SDAHighImpedance();
   MakeClock();
   Wait4Microsec(); //MBMBMB
}

//----------------------------------------------------------------------------
// Public Prozeduren: 
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// SDAStart realisiert eine Start-Condition. Die Prozedur geht nicht davon aus, dass der
// Bus zu Beginn im Ruhezustand ist, sie kann deshalb auch f�r ein Restart ohne vorherige
// Stop-Condition verwendet werden.
// Nach dieser Prozedur sind beide Leitungen auf Low-Pegel.
//----------------------------------------------------------------------------
void SDAStart(void)
//----------------------------------------------------------------------------
{
   SDAHighImpedance();
   SetSCL();
   Wait4Microsec();
   SDALowLevel();
   Wait4Microsec();
   SCL_0;
   Wait4Microsec();
}

//----------------------------------------------------------------------------
// SDAStart realisiert eine Stop-Condition. Die Prozedur geht davon aus, dass der
// Bus zu Beginn aktiv ist, d.h. die Clock-Leitung ist auf Low-Pegel, die Datenleitung
// undefiniert.
// Nach dieser Prozedur sind beide Leitungen auf High-Pegel.
//----------------------------------------------------------------------------
void SDAStop(void)
//----------------------------------------------------------------------------
{
   MakeStop();
}

//----------------------------------------------------------------------------
// Bei fehlerfreiem Senden gibt die Prozedur 1 zur�ck, bei Fehler gibt sie 0 zur�ck.
//----------------------------------------------------------------------------
UInt8 SDASendByte(UInt8 byte)
//----------------------------------------------------------------------------
{
   UInt8 i;
   UInt8 mask;
   
   mask = 0x80;
   for (i=0; i<8; i++)
   {
      if (byte & mask)
      {
         SDAHighImpedance();
      }
      else
      {
         SDALowLevel();
      }
      mask >>= 1;
      MakeClock();
   }
   return CheckAck();
}

//----------------------------------------------------------------------------
UInt8 SDAReadByte(UInt8 ack)
//----------------------------------------------------------------------------
{
   UInt8 i;
   UInt8 mask;
   UInt8 byte;
   
   SDAHighImpedance();
   mask = 0x80;
   byte = 0;
   for (i = 0;i < 8; i++)
   {
      SetSCL();
      Wait4Microsec();
      if (GetPinSDA())
      {
         byte |= mask;
      }
      SCL_0;
      Wait4Microsec();
      mask >>= 1;
   }
   if (ack)
   {
      DoAck();
   }
   else
   {
      DoNack();
   }
   return byte;
}

//----------------------------------------------------------------------------
void InitializeIIC_Poll(void)
//----------------------------------------------------------------------------
{ 

   gpio_pin_config_t config = {kGPIO_DigitalOutput, 1, kGPIO_NoIntmode}; //MBMBMB 1
   GPIO_PinInit(BOARD_INITPINS_I2C_SCL_GPIO, BOARD_INITPINS_I2C_SCL_PIN, &config);
   GPIO_PinInit(BOARD_INITPINS_I2C_SDA_GPIO, BOARD_INITPINS_I2C_SDA_PIN, &config);
 
   // SCL_1;              // Clock High MBMBMB
   // SDAHighImpedance(); // SDA High   MBMBMB
   // SetPinSDA(0);        // MBMBMB Dauernd auf Low-Pegel. High-Pegel wird durch Umschalten auf High-Pegel und Pullup-Wiederstand realisiert. 
}

