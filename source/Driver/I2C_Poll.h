/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------

Autor                  Datum

-- BESCHREIBUNG ---------------------------------------------------------------

-- AENDERUNGEN ----------------------------------------------------------------

Autor                  Datum

-----------------------------------------------------------------------------*/

//-----------------------------------------------------------------------------
#pragma once

#include "BuchiTypes.h"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// SDAStart realisiert eine Start-Condition. Die Prozedur geht nicht davon aus, dass der
// Bus zu Beginn im Ruhezustand ist, sie kann deshalb auch f�r ein Restart ohne vorherige
// Stop-Condition verwendet werden.
// Nach dieser Prozedur sind beide Leitungen auf Low-Pegel.
//----------------------------------------------------------------------------
void SDAStart(void);
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// SDAStart realisiert eine Stop-Condition. Die Prozedur geht davon aus, dass der
// Bus zu Beginn aktiv ist, d.h. die Clock-Leitung ist auf Low-Pegel. 
// Nach dieser Prozedur sind beide Leitungen auf High-Pegel.
//----------------------------------------------------------------------------
void SDAStop(void);
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Prozedur SDASendByte schreibt ein Byte und gibt bei fehlerfreiem Senden 1 zur�ck, 
// bei Fehler gibt sie 0 zur�ck (fehlerfrei heisst: Acknowledge wurde empfangen). 
// SDASendByte f�hrt keine Start- und Stop-Condition durch. Diese m�ssen separat 
// erzeugt werden.
//----------------------------------------------------------------------------
UInt8 SDASendByte(UInt8 byte);
//----------------------------------------------------------------------------
 
//----------------------------------------------------------------------------
// Die Prozedur SDAReadByte liest ein Byte und gibt es zur�ck.  
// SDAReadByte f�hrt keine Start- und Stop-Condition durch. Diese m�ssen separat 
// erzeugt werden. Das Acknowledge wird hingegen in dieser Prozedur erzeugt.
// Wenn in ack 1 �bergeben wird, antwortet die Prozedur auf die Daten mit einem
// Acknowledge, sonst mit Not Acknowledge.
//----------------------------------------------------------------------------
UInt8 SDAReadByte(UInt8 ack);
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// InitializeIIC muss bein Aufstarten einmal aufgerufen werden. 
//----------------------------------------------------------------------------
void InitializeIIC_Poll(void);
//----------------------------------------------------------------------------

