/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------
Autor                  Datum


-- BESCHREIBUNG ---------------------------------------------------------------



-- AENDERUNGEN ----------------------------------------------------------------
Autor                  Datum

-----------------------------------------------------------------------------*/
#include "Hardware.h"
#include "PT1000.h"
#include "Pwm.h"
#include "Gpio.h"
#include "I2C_Controller.h"

IO IO_Data;
PARAM PARAM_Data;

//-----------------------------------------------------------------------------
Bool InitHardware(void)
//-----------------------------------------------------------------------------
{
   InitGPIO();
   InitPT1000();
   InitializePwm();
   InitI2C_Controller();
   
   // init the fw led pin
   SetOutputPin(eLED_FW, TRUE);
   
   // init temproray parameters - later from eeprom
   PARAM_Data.dryingGasTarget = 346;              // 34.6 float
   PARAM_Data.inletTemperatureTarget = 1120;      // 112.0  float
   PARAM_Data.dispersionGasTarget = 800;          // 800 int
   PARAM_Data.samplePumpTarget = 290;             // 29.0 float
   PARAM_Data.unblockNozzleTarget = 25;           // 25 int
   PARAM_Data.dispersionGasMode = 3;              // 3 = PI Regulator
   PARAM_Data.heaterMode = 3;              // 3 = PI Regulator
   PARAM_Data.kpHeater = 7.0;
   PARAM_Data.kiHeater = 0.02;
   PARAM_Data.offsetHeater = 70.0;
   
   // Steuerung mit I-Anteil
   PARAM_Data.offsetDispGas = 410;
   PARAM_Data.kpDispGas = 0.23;
   PARAM_Data.kiDispGas = 0.2;
   // pi regler
   /*
   PARAM_Data.kp = 0.03;
   PARAM_Data.ki = 0.2;
   PARAM_Data.offset = 600;
   */
   return 1;
}

//-----------------------------------------------------------------------------
void GetHardware(void)
//-----------------------------------------------------------------------------
{
   static UInt32 timer10ms = 0;
   
   //UpdateFlowSensor_I2C();
   
   timer10ms++;
   if((timer10ms%2) == 0)   // each 10.call (2*10ms = 20ms)
   {
      IO_Data.digIn.bInertLoopConnected = ReadInputPin(eROT_ZERO);
      SetOutputPin(eHEAT_SSR1, IO_Data.digOut.bHeatSSR1);
      SetOutputPin(eHEAT_SSR2, IO_Data.digOut.bHeatSSR2);
      UpdateFlowSensor_I2C();
   }
   if((timer10ms%20) == 0)   // each 20.call (20*10ms = 200ms)
   {
      UpdatePT1000();
      //UpdateFlowSensor_I2C();
   }
   
}

//-----------------------------------------------------------------------------
void SetHardware(void)
//-----------------------------------------------------------------------------
{
   SetOutputPin(eLED_FW, IO_Data.digOut.bFwLed);
   

}

//-----------------------------------------------------------------------------
IO* GetIOData(void)
//-----------------------------------------------------------------------------
{
   return &IO_Data;
}

//-----------------------------------------------------------------------------
PARAM* GetParamData(void)
//-----------------------------------------------------------------------------
{
   return &PARAM_Data;
}
