/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------

Autor                  Datum
R. Kaufmann            29.05.2019

-- BESCHREIBUNG ---------------------------------------------------------------
Kommunikation zu Dongle etc. �ber B�chi-Bus.

-- AENDERUNGEN ----------------------------------------------------------------

Autor                  Datum

-----------------------------------------------------------------------------*/

#include "BusTelegram.h"
#include "StdTelegram.h"
#include "Telegram.h"
#include "UiCommunication.h"
#include "ProcessControl.h"
//#include "TestCommands.h"
//#include "DataIO.h"
#include <stdio.h>

extern "C" 
{
   #include "BuchiBus.h"
   #include "Hardware.h"
   #include "Release.h"
   #include "string.h"
   #include "fsl_device_registers.h"
}

// Senden und Empfang
#define RX_DATA_BUF_LEN                    (255)
#define TX_DATA_BUF_LEN                    (255)
#define BAUDRATE                           (230400)
#define RX_TIMEOUT_MS                      (7)  //ms

// Master-Priorit�t, laut Liste Priorit�ten.
#define MASTER_PRIO                        (0x04)

// Maximale Zeit die auf eine Nachricht auf dem Bus gewartet wird (2)
#define MAX_IDLE_CNT                       (200)

// Maximale Speichermenge in Bytes die der Bluetooth Dongle senden darf
#define MAX_BLE_MEMORY_REQUEST_SIZE 0x20000

// Maximale Anzahl Sendeversuche per BLE zum Handy bevor Senden abgebrochen wird.
// Bei 100ms Sendeintervall ergibt sich f�r 20 Versuche 2s.
#define MAX_BLE_TX_TIMEOUT_CALLS    20

// Versionsnummer der ID-basierten Objektschnittstelle zur App
#define ID_BASED_OBJECT_API_VERSION 0x01

// Errorcodes, ID-basierte Objektschnittstelle zur App
#define APP_OK                         0x00
#define APP_ERROR_INVALID_OBJECT       0x01
#define APP_ERROR_DEVICE_OUT_OF_SPACE  0x02
#define APP_ERROR_UNSUPPORTED_COMMAND  0xFE
#define APP_ERROR_UNSUPPORTED_PROTOCOL 0xFF

// Prototypen
void ResetTxIO();
void ResetTxBLE();
void ResetComBLE();
void StatusErrorTxBLE(Bool bError);

///////////////////////////////////////////////////////////////////////////////
/*
 act... variables: read data via the RS-485 Buchi Bus interface
 Get...() functions to read them out from the BusTelegram module

 Set...(...) to send data via the RS-485 Buchi Bus interface
*/

// Print Temperatur in �C, floating point
static FLOAT32 s_actPrintTemp;

// Print Eingangsspannung 30V
static FLOAT32 s_actVoltage30V;

// Print Eingangsspannung 5V
static FLOAT32 s_actVoltage5V;

// Print Eingangsspannung 3.3V
static FLOAT32 s_actVoltage3V3;

// Angeschlossene Ger�te (Bitmaske)
static UINT16 s_actConnectedDevices;

// Kommunikationsbuffer und Parameter B�chi-Bus
static UInt8 s_command;
static UInt8 s_rxData[RX_DATA_BUF_LEN];
static UInt8 s_rxDataLen;
static UINT8 s_txData[TX_DATA_BUF_LEN];
static UInt8 s_masterPrio;
static UInt8 s_slaveAdr;
static Bool s_bShutUp;
static UInt8* s_pRxData = 0;
static UInt32 s_RxCurPos = 0;
static UInt32 s_RxTotalLength = 0;
static UInt8* s_pTxData = 0;
static UInt32 s_TxCurPos = 0;
static UInt32 s_TxTotalLength = 0;
static UInt32 s_TxLastSize = 0;
static UInt32 s_TxTimeout = 0;

// Z�hlt die Anzahl Durchl�ufe ohne Empfang
static INT16 s_idleCounter = 0;

// Z�hlt damit alle 100ms der BLE-Dongle angefragt wird
static UInt32 s_sendCounter = 0; 

// Frametypen zum Datenschaufeln vom/zum BLE-Dongle
#define TYPE_RX_FIRST      0
#define TYPE_RX_NEXT       1
#define TYPE_RX_LAST       2
#define TYPE_RX_GET        4
#define TYPE_TX_STOP       10
#define TYPE_TX_CONTINUE   11
#define TYPE_TX_PUT        12
typedef struct __attribute__((__packed__))
{
   UInt8 dataResponseType;
   UInt8 dataResponseLength;
   UInt32 dataResponseOverallLength;
   UInt8 dataResponse[117];
} IO_Rx_0X50_TY;

typedef IO_Rx_0X50_TY   IO_Tx_0X50_TY;

// Direkte Kommunikation mit dem BLE Dongle
IO_Tx_0X50_TY s_txBLE;
IO_Rx_0X50_TY s_rxBLE;

// Request von der App �ber den BLE-Dongle
typedef struct
{
   UInt8 protocolVersion;
   UInt8 command;
} APP_Request_TY;

// Response zur App per BLE-Dongle.
typedef struct
{
   UInt8 ErrorCode;
} APP_Response_TY;

// Auf dem B�chi-Bus empfangene Werte auf "undefiniert" setzen, konkret 
// bei Kommunikationsausfall mit dem IO-Board.
static void SetAllValuesUndefined(void)
{
   // Print Temperatur in �C, floating point
   s_actPrintTemp = -20.0f;

   // Print Eingangsspannung 30V
   s_actVoltage30V = 0.0f;

   // Print Eingangsspannung 5V
   s_actVoltage5V = 0.0f;

   // Print Eingangsspannung 3.3V
   s_actVoltage3V3 = 0.0f;

   // Angeschlossene Ger�te (Bitmaske)
   s_actConnectedDevices = 0;
}

// Empfangenes B�chi-Bus Kommando interpretieren und beantworten.
static void InterpreteCommand(void)
{
   UINT8 len = 0; // Vorgabe
   
   // Standard-Telegram empfangen?
   if (s_command < 0x3d)
   {
      len = DoStdTelegram(s_command, s_rxData, s_rxDataLen, s_txData);
      SendResponse(len, s_txData);
      return;
   }
   
   // Anderes Telegramm empfangen...
   switch (s_command)
   {
      /*case 0x3d: // Befehle f�r Ger�te-Endtest, die an das Grundger�t weiter geleitet werden
         len = InterpreteDeviceTestCommandBasic(s_rxDataLen, s_rxData, s_txData);
         SendResponse(len, s_txData);
         break;

      case 0x3e: // Befehle f�r Ger�te-Endtest
         len = InterpreteDeviceTestCommand(s_rxDataLen, s_rxData ,s_txData);
         SendResponse(len, s_txData);
         break;

      case 0x3f: // Befehle f�r Print Test Testbefehle:
         len = InterpretePrintTestCommand(s_rxDataLen, s_rxData, s_txData);
         SendResponse(len, s_txData);*/
         break;
         

   case 0x40: // dispersion gas control
      if (s_rxDataLen >= 1)
      {
         // start / stopp drying gas
         if(s_rxData[0] == 1)
         {
            if (s_rxData[1] == 1)
            {
               CProcessControl::GetInstance()->DoCommand(CProcessControl::CMD_MAN_DISPERSIONGAS_ON);
            }
            else if(s_rxData[1] == 0)
            {
               CProcessControl::GetInstance()->DoCommand(CProcessControl::CMD_MAN_DISPERSIONGAS_OFF);
            }
         }
         // dispersion gas mode
         if(s_rxData[0] == 2)
         {
            if(s_rxData[1] <= 4 && s_rxData[1] >= 1)
            {
               GetParamData()->dispersionGasMode = s_rxData[1]; // mode 1, 2 ,3
            }
         }
         // pi regler parameter
         if(s_rxData[0] == 3)
         {
            memcpy(&GetParamData()->kpDispGas, &s_rxData[1], 4);
            memcpy(&GetParamData()->kiDispGas, &s_rxData[1+4], 4);
            memcpy(&GetParamData()->offsetDispGas, &s_rxData[1+4+4], 2);
            memcpy(&GetParamData()->dispersionGasTarget, &s_rxData[1+4+4+2], 4);
         }
      }
      break;
      
   case 0x41: // Heater control
      // start / stopp heater
      if(s_rxData[0] == 1)
      {
         if (s_rxData[1] == 1)
         {
            CProcessControl::GetInstance()->DoCommand(CProcessControl::CMD_MAN_HEATER_ON);
         }
         else if(s_rxData[1] == 0)
         {
            CProcessControl::GetInstance()->DoCommand(CProcessControl::CMD_MAN_HEATER_OFF);
         }
      }
      // heater mode
      else if(s_rxData[0] == 2)
      {
         if(s_rxData[1] <= 4 && s_rxData[1] >= 1)
         {
            GetParamData()->heaterMode = s_rxData[1]; // mode 1, 2 ,3
         }
      }
      // heater parameter
      else if(s_rxData[0] == 3)
      {
         memcpy(&GetParamData()->kpHeater, &s_rxData[1], 4);
         memcpy(&GetParamData()->kiHeater, &s_rxData[5], 4);
         memcpy(&GetParamData()->offsetHeater, &s_rxData[9], 2);
      }
      // heater man control
      else if(s_rxData[0] == 4)
      {
         memcpy(&GetParamData()->heaterManPower, &s_rxData[1], 2);
      }

      
      break;

   case 0x50: // trace
      
      memcpy(s_txData, CUICommunication::GetInstance()->GetRegTraceSendData(), sizeof(SPRAYDRYER_REG_TRACE));
      SendResponse(sizeof(SPRAYDRYER_REG_TRACE), s_txData);
      
      break;
      
      default:
         break;
   }
}

// Wenn die Funktion true zur�ck gibt, wurde ein Speicherblock mit den Out-Daten erzeugt.
static Bool ProcessBLE(void)
{
   UInt32 responseSize = 0;
   UInt8* pData = 0;
   UInt32 i;
   static UInt8 status = 0;
   static UInt8 buffer[128];
   static APP_Request_TY appRequest;
   static APP_Response_TY appResponse;

   // Veralteten Sendpuffer wegschmeissen
   if (s_pTxData)
   {
      delete s_pTxData;
      s_pTxData = 0;
   }

   // Es kann immer nur einen Antwortblock geben. In dieser Funktion werden evtl. ben�tigte Hilfsfunktionen aufgerufen,
   // Speicher f�r den Ausgangsblock alloziert, dieser entsprechend gef�llt und bei Erfolg true zur�ckgegeben.
   if (s_pRxData)
   {
      // Put
      if (*s_pRxData == 0)
      {
         responseSize = 1;
         pData = &status;
      }
      // Get
      else if (*s_pRxData == 1)
      {
         responseSize = *((UInt32*)(s_pRxData + 1));
         responseSize++; // Statusbyte 0
         if (responseSize > sizeof(buffer))
            responseSize = sizeof(buffer);
         for (i = 0; i < responseSize; i++)
            buffer[i] = (i % 256);
         pData = &buffer[0];
      }
      
      // ID-basierte Objektschnittstelle f�r App
      memcpy(&appRequest, s_pRxData, sizeof(appRequest));
      
      // API version pr�fen
      if (appRequest.protocolVersion != ID_BASED_OBJECT_API_VERSION)
      {
         // Falsche API Version
         appResponse.ErrorCode = APP_ERROR_UNSUPPORTED_PROTOCOL;
         responseSize = sizeof(appResponse);
      }
      else
      {
         // Welches Kommando von der App soll ausgef�hrt werden?
         switch (appRequest.command)
         {
//         case APP_COMMAND_:
//            break;

         default:
            // Kommando nicht unterst�tzt
            appResponse.ErrorCode = APP_ERROR_UNSUPPORTED_COMMAND;
            responseSize = sizeof(appResponse);
            break;
         }
      }
   }

   // Versandfertig machen
   s_TxCurPos = 0;
   s_TxTotalLength = responseSize;
   if (responseSize && pData)
   {
      s_pTxData = new UInt8[responseSize] ();
      if (s_pTxData == 0)
      {
         // Leider ging uns der Speicher aus, ganze Aktion abbrechen.
         StatusErrorTxBLE(true);
         return false;
      }
      
      // Daten bef�llen, wir kopieren einfach die gleichen Daten welche wir empfangen haben.
      memcpy(s_pTxData, pData, s_TxTotalLength);
      
      // Versand erfolgt in der 0x50 Anfrage an den Dongle.
      return true;
   }
   return false;
}

// Die Anworten auf als Master gesendete Abfragen auswerten.
static void InterpreteResponse(void)
{
   switch (s_command)
   {
      case 0x50: // Kombibefehl zum BLE-Dongle
         // Empfangene Daten haben die richtige Gr�sse f�r die Nachrichtenstruktur?
         if (s_rxDataLen)
         {
            memcpy(&s_rxBLE, s_rxData, s_rxDataLen);

            // D�rfen wir weiter an den Dongle Daten senden? Ja:
            if (s_rxBLE.dataResponseType == TYPE_TX_CONTINUE)
            {
               // Alles paletti
               s_TxCurPos += s_TxLastSize;
               s_TxTimeout = 0;
            }
            
            // D�rfen wir weiter an den Dongle Daten senden? NEIN:
            if (s_rxBLE.dataResponseType == TYPE_TX_STOP)
            {
               // Sendez�hler bleibt gleich, wir pr�fen aber ob noch etwas geht und brechen bei Bedarf mit Timeout ab.
               s_TxTimeout++;
               
               if (s_TxTimeout > MAX_BLE_TX_TIMEOUT_CALLS)
               {
                  ResetTxBLE();
                  ResetComBLE();
                  
                  /*static TXTCHR text[128] = { 0 };
                  static TXTCHR* pT_Message[2] = { 0, text };

                  sprintf(text, "Kann nicht per BLE zur App schreiben!");
                  MenuHandler::GetInstance()->OpenMessageMenuOK(0, pT_Message, 9999, TYPE_INFO);*/
               }                                                     
            }

            // Dongle hat Daten f�r das Display
            // Ist es der erste Frame?
            // Ja, vorherigen Nachrichtenpuffer wegschmeissen,
            // neuen Nachrichtenpuffer f�r die erwartete Datenmenge anlegen,
            // Frame im Nachrichtenpuffer ablegen.
            //
            // Nein, Frame im Nachrichtenpuffer ablegen.
            // Beim letzten Frame den vollst�ndigen Empfang quittieren,
            // damit der Dongle den Status ok ans Handy senden kann.
            if ((s_rxBLE.dataResponseType == TYPE_RX_FIRST) || 
                ((s_rxBLE.dataResponseOverallLength <= sizeof(s_rxBLE.dataResponse)) &&
                 (s_rxBLE.dataResponseType == TYPE_RX_LAST)))
            {
               if (s_pRxData)
                  delete s_pRxData;
               
               // Bullshitsrequest welcher die Resourcen �bersteigt?
               // Negative Antwort.
               if (s_rxBLE.dataResponseOverallLength > MAX_BLE_MEMORY_REQUEST_SIZE)
               {
                  // Daumen runter (Status) ans BLE zur�ck.
                  StatusErrorTxBLE(true);
                  return;
               }
               s_RxCurPos = 0;
               s_RxTotalLength = s_rxBLE.dataResponseOverallLength;
               s_pRxData = new UInt8[s_RxTotalLength]();
               if (s_pRxData == 0)
               {
                  // Hat leider nicht geklappt, kein Speicher. Bluetooth Dongle 
                  // benachrichtigen, dass die Nachricht nicht empfangen werden kann.
                  // Daumen runter (Status) ans BLE zur�ck.
                  StatusErrorTxBLE(true);
                  return;
               }
            }
               
            // Empfangenen Frame im Nachrichtenpuffer ablegen
            if (s_rxBLE.dataResponseType <= TYPE_RX_LAST)
            {
               if ((s_pRxData == 0) ||  (s_rxBLE.dataResponseLength > s_RxTotalLength) ||
                   (s_rxBLE.dataResponseLength > sizeof(s_rxBLE.dataResponse)) || 
                   ((s_RxCurPos + s_rxBLE.dataResponseLength) > s_RxTotalLength))
               {
                  // Bullshit empfangen, Abbruch.
                  StatusErrorTxBLE(true);
                  return;
               }

               memcpy(&s_pRxData[s_RxCurPos], s_rxBLE.dataResponse, s_rxBLE.dataResponseLength);
               s_RxCurPos += s_rxBLE.dataResponseLength;

               // Fertig?
               if (s_RxCurPos == s_RxTotalLength)
               {
                  // Empfangene BLE Daten verarbeiten und Antwort zusammenbauen.
                  if (ProcessBLE())
                  {
                     // Zur�cksenden der Anwort vorbereiten. Der zu sendende Datenblock ist s_pTxData,
                     // seine Daten schicken wir in LoopTelegram anstatt nach neuen Daten zu fragen.
                     // Sendez�hler zur�cksetzten.
                     return;
                  }
            
                  // Daumen runter (Status) ans BLE zur�ck.
                  StatusErrorTxBLE(false);
                  return;
               }
            }           
         }
         ResetTxBLE();
         break;

      default:
         break;
   }
}

// Hauptschleife die aufgerufen werden muss um als Slave empfangene
// Anfragen zu beantworten als auch um als Master selbst Anfragen versenden
// zu k�nnen.
void LoopTelegram(void)
// Aufrufintervall: 5ms
{
   UInt8 remoteEvent;
   UInt8 statusLight[2];
   static Bool bBLEDataRequested = false;
  
   remoteEvent = LoopBusProtocolLayer();

   // Das Ger�t hat normal eine Slave Funktion. Alle 100ms fragt es den BLE-Dongle
   // ob er Daten f�r das Display hat.
   if (remoteEvent == BUS_REQUEST)
   {
      // Z�hler f�r "Aufruf und kein Request auf dem Bus" zur�ck setzen
      s_idleCounter = 0;
      InterpreteCommand();
   }
   else if (remoteEvent == BUS_BROADCAST)
   {
      switch (s_command)
      {
         case 0x1F: // 1 = ShutUp, Disable MasterFunction, 2 = Enable MasterFunction
            if (s_rxDataLen > 0)
            {
               if (s_rxData[0] == 1) s_bShutUp = true;
               if (s_rxData[0] == 2) s_bShutUp = false;
            }
            return;

      default:
         break;
      }
   }
   else if (remoteEvent == BUS_RESPONSE)
   {
      // Hat der Dongles was f�r uns?
      if (bBLEDataRequested)
      {
         // Na hoffentlich, wir fragten ja.
         InterpreteResponse();
      }
   }
   else
   {  
      if (s_idleCounter < MAX_IDLE_CNT)
      {
         s_idleCounter++;
      }
      else
      {
         // Wenn keine Verbindung zum Master (IO-Board), dann Werte undefiniert setzen.
         SetAllValuesUndefined();

         // DEBUG FEHLERMELDUNG WENN KEINE KOMMUNIKATION


         // Kommunikationsstrukturen mit BLE-Dongle reinitialisieren
         ResetTxBLE();
         ResetComBLE();
      }
      
      // Master wurde gestoppt?
      if (s_bShutUp)
         return;

      // Dongle fragen ob er Daten f�rs Display hat. Alle 100ms
      // Test schneller, alle 50ms: if ((s_sendCounter % 10) == 0)
      if ((s_sendCounter % 20) == 3330)
      {
         // Soll etwas an den BLE-Dongle gesendet werden?
         s_txBLE.dataResponseType = TYPE_RX_GET;         
         if (s_pTxData && s_TxTotalLength)
         {
            // Wurde bereits alles gesendet? Ja, Sendepuffer und Z�hler zur�cksetzen.
            if (s_TxCurPos >= s_TxTotalLength)
            {
               // Wir sind fertig. Aufr�umen
               ResetComBLE();
            }
            else
            {
               // Nein, wir sind noch nicht fertig: Weitere Daten senden.
               s_TxLastSize = ((s_TxTotalLength - s_TxCurPos) > sizeof(s_txBLE.dataResponse)) ? sizeof(s_txBLE.dataResponse) : (s_TxTotalLength - s_TxCurPos);
               s_txBLE.dataResponseType = TYPE_TX_PUT;
               s_txBLE.dataResponseLength = s_TxLastSize;
               s_txBLE.dataResponseOverallLength = s_TxTotalLength;               
               memcpy(s_txBLE.dataResponse, (s_pTxData + s_TxCurPos), s_TxLastSize);
            }
         }

         // Wenn sonst nichts zu senden, nach neuen Daten fragen.
         if (SendRequest(0x0a, 0, 0x50, sizeof(s_txBLE), (UInt8*)&s_txBLE))
         {
            bBLEDataRequested = true;
            ResetTxBLE();
         }
      }
      
      // Dem Statuslight sagen, wie es leuchten soll. Alle 1000ms
      if ((s_sendCounter % 200) == 18)
      {         
         // Command 0x1E  1 = Ready, 2 = Running, 3 = Error
         statusLight[0] = 1;//Device::GetStatusLightStatus();
         statusLight[1] = 0;
         *(UInt32*)&statusLight[2] = s_sendCounter;
         SendRequest(BROADCAST_ADDRESS, 0, 0x1e, 6, statusLight);
      }
      s_sendCounter++;
   }
}

// Anwortstruktur f�r Anfragen des IO-Boards zur�cksetzen.
void ResetTxIO()
{
   /*memset(&s_txIO, 0xff, sizeof(s_txIO));
   s_txIO.paramProcess = 0;
   s_txIO.paramProcessDosingH2O = 0;
   s_txIO.paramProcessDosingNaOH = 0;
   s_txIO.paramProcessDosingH3BO3 = 0;
   s_txIO.paramTimeReaction = 0;
   s_txIO.paramTimeDistillation = 0;
   s_txIO.paramTimeTitrationStart = 0;
   s_txIO.paramTypeTitration = 0;
   s_txIO.paramPowerSteam = 100;
   s_txIO.paramSpeedStirrerDestillation = 0;
   s_txIO.paramSpeedStirrerTitration = 0;
   s_txIO.paramTimeAspirationReceiver = 0;
   s_txIO.paramTimeAspirationSampleTube = 0;*/
}

// Datenstruktur zum Senden zum Bluetooth-Dongle zur�cksetzen.
void ResetTxBLE()
{
   memset(&s_txBLE, 0, sizeof(s_txBLE));
   s_txBLE.dataResponseType = TYPE_RX_GET;   
}

// Empfangs- und Sendeoperationen zum Bluetooth-Dongle stoppen/zur�cksetzen.
void ResetComBLE()
{
   s_RxTotalLength = s_RxCurPos = 0;
   if (s_pRxData)
      delete s_pRxData;
   s_pRxData = 0;
   s_TxTotalLength = s_TxCurPos = s_TxLastSize = 0;
   if (s_pTxData)
      delete s_pTxData;
   s_pTxData = 0;
   s_TxTimeout = 0;
}

// Statusflag zum Bluetooth-Dongle senden.
void StatusErrorTxBLE(Bool bError)
{
   memset(&s_txBLE, 0, sizeof(s_txBLE));
   s_txBLE.dataResponseType = TYPE_RX_GET;
   s_txBLE.dataResponseLength = s_txBLE.dataResponseOverallLength = 1;
   s_txBLE.dataResponse[0] = (UInt8)bError;
}

/*****************************************************************************/
void InitializeBus()
{   
   // Kommunikationsvariablen initialisieren
   s_command = 0x0;
   memset(s_rxData, 0, sizeof(s_rxData));
   s_rxDataLen = 0;
   memset(s_txData, 0, sizeof(s_txData));
   s_masterPrio = 0;
   s_slaveAdr = 0;
   s_bShutUp = false;
   s_idleCounter = 0;
   s_sendCounter = 0; 

   // Info die vom Bus gelesen wird initialisieren
   SetAllValuesUndefined();
   
   // Kommunikationsstrukturen mit IO-Board initialisieren
   //memset(&s_rxIO, 0xff, sizeof(s_rxIO));
   ResetTxIO();
   
   // Kommunikationsstrukturen mit BLE-Dongle initialisieren
   memset(&s_rxBLE, 0xff, sizeof(s_rxBLE));
   ResetTxBLE();
   s_pRxData = 0;
   s_RxCurPos = 0;
   s_RxTotalLength = 0;
   s_pTxData = 0;
   s_TxCurPos = 0;
   s_TxTotalLength = 0;
   s_TxLastSize = 0;
}

/*****************************************************************************/
void StartBusCommunication(UInt8 ucSlaveAddr)
{   
   s_slaveAdr  = ucSlaveAddr;
   s_masterPrio = MASTER_PRIO;
   
   InitializeBusProtocolLayer(BAUDRATE, RX_TIMEOUT_MS, &s_masterPrio, &s_slaveAdr, &s_command, &s_rxDataLen, s_rxData,
                              (Int8*)FW_ARTICLE_NUMBER,
                              FW_MAJOR_VERSION, FW_MINOR_VERSION, FW_BUILD_NUMBER, FW_REVISION_NUMBER,
                              &NVIC_SystemReset);
}

////////////////////////////////////////////////////////////////////////////////
// system information
////////////////////////////////////////////////////////////////////////////////

/***************************************************************************/
FLOAT32 GetPrintTemp(void)
{
   return s_actPrintTemp * 0.1f;
}

/***************************************************************************/
FLOAT32 GetVoltage30V(void)
{
   return s_actVoltage30V * 0.1f;
}

/***************************************************************************/
FLOAT32 GetVoltage5V(void)
{
   return s_actVoltage5V * 0.1f;
}

/***************************************************************************/
FLOAT32 GetVoltage3V3(void)
{
   return s_actVoltage3V3 * 0.1f;
}

/***************************************************************************/
UINTL32 GetConnectedDevices(void)
{
   return s_actConnectedDevices;
}



