/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------

Autor                  Datum
D. Benz                23.03.2020
Project:               Optima Sprydryer
-------------------------------------------------------------------------------



-- AENDERUNGEN ----------------------------------------------------------------


-----------------------------------------------------------------------------*/

#include "Sample.h"
#include "UiCommunication.h"

extern "C"
{
   #include "Hardware.h"
   #include "FreeRTOS.h"
}

// static instance pointer initialization
CSample* CSample::m_pInstance = 0;

//-----------------------------------------------------------------------------
// Konstruktor
//-----------------------------------------------------------------------------
CSample:: CSample()
{
   

}

//-----------------------------------------------------------------------------
// Destruktor
//-----------------------------------------------------------------------------
CSample::~CSample()
{

}

//-----------------------------------------------------------------------------
// singleton
//-----------------------------------------------------------------------------
CSample* CSample::GetInstance()
{
   if (m_pInstance == 0)
   {
      m_pInstance = new CSample();
   }
   
   return m_pInstance;
}




extern "C" void SampleTaskWrapper(void* parm) {

    (static_cast<CSample*>(parm))->SampleTask();

}

//static void main_task(void *pvParameters)
void CSample::SampleTask(void)
{
   //init
   GetInstance(); // singleton classe instanzieren
   
   while(1)
   {
      /*
      CyclicCall();
      StateCall();
      vTaskDelayUntil(&xNextWakeTime, main_task_INTERVAL);
      */
   }
}



/*
void CyclicCall()
{

}

void StateCall()
{
   switch(state)
   {
      state on:
         StateOn()
         break;

      state off:
         StateOff()
         break;
   }
}

stateOn()
{

}


stateOff()

{

}

*/


