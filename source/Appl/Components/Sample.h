/*--------------        ---------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------

Autor                  Datum
D. Benz                23.03.2020
Project:               Optima Sprydryer
-------------------------------------------------------------------------------



-- AENDERUNGEN ----------------------------------------------------------------


-----------------------------------------------------------------------------*/

#pragma once

class CSample
{
   public:
      static CSample* GetInstance();

   
   void SampleTask(void);

   protected:

   private:
      static CSample* m_pInstance;
      
      CSample();                // Konstruktor private da singleton
      ~CSample();

};

extern "C" void SampleTaskWrapper(void* parm);