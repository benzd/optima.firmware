/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------

Autor                  Datum
D. Benz                06.02.2020
Project:               Optima Sprydryer
-------------------------------------------------------------------------------



-- AENDERUNGEN ----------------------------------------------------------------


-----------------------------------------------------------------------------*/

#include "AirFlow.h"
#include "TimeBase.h"
#include "UiCommunication.h"
extern "C"
{
   #include "Hardware.h"
}

//-----------------------------------------------------------------------------
// Konstruktor
//-----------------------------------------------------------------------------
CAirFlow:: CAirFlow()
{
   m_bEnable = false;
   m_pUiCommunication = 0;
}

//-----------------------------------------------------------------------------
// Destruktor
//-----------------------------------------------------------------------------
CAirFlow::~CAirFlow()
{

}

//-----------------------------------------------------------------------------
// General initialization
//-----------------------------------------------------------------------------
void CAirFlow::Init(void)
{
   m_bEnable = false;
   
   m_pUiCommunication = CUICommunication::GetInstance();

   InitSystem();
}

//-----------------------------------------------------------------------------
// Enable the drying gas
//-----------------------------------------------------------------------------
void CAirFlow::On(void)
{
   m_bEnable = true;
}

//-----------------------------------------------------------------------------
// Disable the drying gas
//-----------------------------------------------------------------------------
void CAirFlow::Off(void)
{
   m_bEnable = false;
}

//-----------------------------------------------------------------------------
// Do startup check of stoppering system availability
//-----------------------------------------------------------------------------
void CAirFlow::StartupCall()
{
   CStateMachine::StartupCall();
}

//-----------------------------------------------------------------------------
// Method call every cycle
//-----------------------------------------------------------------------------
void CAirFlow::CyclicCall()
{
   if(GetState() == STATE_AirFlow_ON)
   {
      m_pUiCommunication->GetCyclicSendData()->fDryingGasFlow = GetParamData()->dryingGasTarget + (rand() % 12 + 1) -6;
   }
   else
   {
      m_pUiCommunication->GetCyclicSendData()->fDryingGasFlow = 0;
   }
}

//-----------------------------------------------------------------------------
// Method call for the state process
//-----------------------------------------------------------------------------
void  CAirFlow::StateCall(UInt32 nStateId, EMethodId eMethodId)
{
   switch (nStateId)
   {
    case STATE_IDLE:
      StateIdle(eMethodId);
      break;
    case STATE_AirFlow_ON:
      StateAirFlowOn(eMethodId);
      break;
    default:

      break;
   }
}

void CAirFlow::UpdateObserver(CObservable* pObservable,UInt32 uiVal)
{
   if (uiVal == OBSERVABLE_MSGSYS_ACK)
   {           
   }
   else if ( uiVal == OBSERVABLE_TMR) // -> timer is observed by base class CStateMachine
   {
      CStateMachine::UpdateObserver(pObservable,uiVal);
   }
}

//-----------------------------------------------------------------------------
// string allocation
//-----------------------------------------------------------------------------
void CAirFlow::InitState(void)
{
   m_ucUsedStates = 2u;
   CStateMachine::InitState();
}

//////////////////// methods from ILifeCycle interface /////////////////////////
// Transfer reference
void  CAirFlow::InitSystem()
{
   InitState();
   
   SetStartupCallTime( 13 ); // check  availability 1.3s after startup. Odd time to check when no other checks are done
      
   // use 100ms tick from time base observer
   CTimeBase::GetInstance()->GetTimeBaseObserver(CTimeBase::EN_BASE100)->AddObserver(this);
}

//-----------------------------------------------------------------------------
// Method call for the state "StateIdle"
//-----------------------------------------------------------------------------
void  CAirFlow::StateIdle(EMethodId eMethodId)
{  
   if (eMethodId == METHOD_ID_ENTER)
   {
      m_pUiCommunication->SendEventComponentStatusChange("dryinggasstatuschanged","Off");
   }
   else if (eMethodId == METHOD_ID_RUN)
   {
      if (m_bEnable)
      {
         DoTransition(STATE_AirFlow_ON);
      }
   }
   else if (eMethodId == METHOD_ID_EXIT)
   {
      m_pUiCommunication->SendEventComponentStatusChange("dryinggasstatuschanged","On");
   }
}

//-----------------------------------------------------------------------------
// Method call for the state "StateAirFlowOn"
//-----------------------------------------------------------------------------
void  CAirFlow::StateAirFlowOn(EMethodId eMethodId)
{  
   if (eMethodId == METHOD_ID_ENTER)
   {
   }
   else if (eMethodId == METHOD_ID_RUN)
   {
      if (!m_bEnable)
      {
         DoTransition(STATE_IDLE);
      }
   }
   else if (eMethodId == METHOD_ID_EXIT)
   {
      
   }
}