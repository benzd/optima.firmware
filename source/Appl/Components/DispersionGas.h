/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------

Autor                  Datum
D. Benz                06.02.2020
Project:               Optima Sprydryer
-------------------------------------------------------------------------------



-- AENDERUNGEN ----------------------------------------------------------------


-----------------------------------------------------------------------------*/

#pragma once

#include "StateMachine.h"
#include "UiCommunication.h"

class CDispersionGas : public CStateMachine
{
  public:
   CDispersionGas();
   virtual ~CDispersionGas();

   // Method call every cycle
   virtual void	CyclicCall();

   // Method call for the state process
   virtual void  StateCall(UInt32 nStateId, EMethodId eMethodId);

   // update function to be called, must be implemented
   void UpdateObserver(CObservable*,UInt32);
   
   // StartupCall function is called by base class after a given time after startup
   virtual void StartupCall();

   void Init(void);
   
   void On(void);
   
   void Off(void);

  protected:
   /////////////// ILifeCycle interface methods //////////////////////////

   // Transfer reference
   void  InitSystem();
    
   // startup
   void InitState(void);

  private:
     
   // class pointer
   CUICommunication* m_pUiCommunication;

   // trace data pointer
   SPRAYDRYER_REG_TRACE* m_pTraceData;
   
   // enable command flag
   Bool m_bEnable;
   
   // current drying gas flow (digits)
   UInt32 m_dispersionGasFlowDigit;
   // current drying gas flow (slm)
   Float32 m_dispersionGasFlowSlm;
   // current drying gas flow (lh)
   Float32 m_dispersionGasFlowLh;
   // current drying gas flow (lh) filtered
   Float32 m_dispersionGasFlowLhFiltered;
   // last target to compare for filtering
   UInt32 m_lastTargetFlowLh;
   
   static const UInt16 MIN_PWM = 400; // 40.0%
   static const UInt16 MAX_PWM = 800; // 80.0%

   // States of this process
   enum enStates {STATE_IDLE, STATE_DISPGAS_ON};

   ////////////////////// own state transition methods /////////////////////////   
   void  StateIdle(EMethodId eMethodId);
   void  StateDispersionGasOn(EMethodId eMethodId);
   
};