/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------

Autor                  Datum
D. Benz                06.02.2020
Project:               Optima Sprydryer
-------------------------------------------------------------------------------



-- AENDERUNGEN ----------------------------------------------------------------


-----------------------------------------------------------------------------*/

#pragma once

//#include "I_IO_System.h"
#include "StateMachine.h"
#include "UiCommunication.h"
extern "C"
{
   #include "Hardware.h"
}

#define FLICKER_INTERVALL_MS    4000
#define HEATPOWER_H1            1520
#define HEATPOWER_H2            780
#define HEATPOWER_MAX           2300

class CHeater : public CStateMachine
{
  public:
   CHeater();
   virtual ~CHeater();

   // Method call every cycle
   virtual void	CyclicCall();

   // Method call for the state process
   virtual void  StateCall(UInt32 nStateId, EMethodId eMethodId);
      
   // update function to be called, must be implemented
   void UpdateObserver(CObservable*,UInt32);

   void Init(void);

   void On(void);

   void Off(void);
   
   // StartupCall function is called by base class after a given time after startup
   virtual void StartupCall();
   
  protected:
   /////////////// ILifeCycle interface methods //////////////////////////

   // Transfer reference
   void  InitSystem();
    
   // startup
   void InitState(void);
   
   
  private:
     
   // class pointer
   CUICommunication* m_pUiCommunication;
   
   // param pointer
   PARAM* m_pParameter;
   
   // io pointer
   IO* m_pIO;
   
   // Trace pointer
   SPRAYDRYER_REG_TRACE* m_pTrace;
   
   // enable command flag
   Bool m_bEnable;
   
   // actual inlet Temperature in �C
   Float32 m_inletTemperature;

   // actual outlet Temperature in �C
   Float32 m_outletTemperature;
   
   // integral part [%]
   Float32 m_integralPart;
   
   // actual requested HeatPower [W]
   UInt16 m_heatPower;
   
   // temperature controller
   void PI_Controller(void);
   
   // sets the pwm 
   void SetPWMOutput(UInt16 heatPowerW);
   
   // States of this process
   enum enStates { STATE_IDLE, STATE_HEATER_ON};

   ////////////////////// own state transition methods /////////////////////////   
   void  StateIdle(EMethodId eMethodId);
   void  StateHeaterOn(EMethodId eMethodId);
   
};