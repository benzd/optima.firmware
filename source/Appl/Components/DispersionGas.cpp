/*-----------------------------------------------------------------------------

Büchi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------

Autor                  Datum
D. Benz                06.02.2020
Project:               Optima Sprydryer
-------------------------------------------------------------------------------



-- AENDERUNGEN ----------------------------------------------------------------


-----------------------------------------------------------------------------*/

#include "DispersionGas.h"
#include "TimeBase.h"
#include "UiCommunication.h"
extern "C"
{
   #include "Hardware.h"
   #include "Pwm.h"
}

//-----------------------------------------------------------------------------
// Konstruktor
//-----------------------------------------------------------------------------
CDispersionGas:: CDispersionGas()
{
   m_bEnable = false;
   m_pUiCommunication = 0;
}

//-----------------------------------------------------------------------------
// Destruktor
//-----------------------------------------------------------------------------
CDispersionGas::~CDispersionGas()
{

}

//-----------------------------------------------------------------------------
// General initialization
//-----------------------------------------------------------------------------
void CDispersionGas::Init(void)
{
   m_bEnable = false;
   
   m_pUiCommunication = CUICommunication::GetInstance();
   m_pTraceData = m_pUiCommunication->GetRegTraceSendData();

   InitSystem();
}

//-----------------------------------------------------------------------------
// Enable the dispersion gas
//-----------------------------------------------------------------------------
void CDispersionGas::On(void)
{
   m_bEnable = true;
}

//-----------------------------------------------------------------------------
// Disable the dispersion gas
//-----------------------------------------------------------------------------
void CDispersionGas::Off(void)
{
   m_bEnable = false;
}

//-----------------------------------------------------------------------------
// Do startup check
//-----------------------------------------------------------------------------
void CDispersionGas::StartupCall()
{
   // always check availability of stoppering system
   //m_bRegStopperingAvail = m_pStopperingIn->IsOn();
   
   CStateMachine::StartupCall();
}

//-----------------------------------------------------------------------------
// Method call every cycle
//-----------------------------------------------------------------------------
void CDispersionGas::CyclicCall()
{
   static Float32 ringBuffer[50];
   static UInt8 actIndex = 0;
   static UInt8 numOfElements = 0;
   static Float32 sum = 0.0;
   
   if(GetParamData()->dispersionGasTarget != m_lastTargetFlowLh)
   {
      numOfElements = 0;
      actIndex = 0;
   }
   m_lastTargetFlowLh = GetParamData()->dispersionGasTarget;
      
   // Update sensor values
   m_dispersionGasFlowDigit = (GetIOData()->analogIn.dispersionGasFlow);
   m_dispersionGasFlowSlm = ((Float32)m_dispersionGasFlowDigit) / 256.0;
   m_dispersionGasFlowLh = (((Float32)m_dispersionGasFlowDigit) * 60.0) / 256.0;
   
   ringBuffer[actIndex] = m_dispersionGasFlowLh;
   actIndex++;
   if(actIndex >= 30)
   {
      actIndex = 0;
   }

   if(numOfElements < 30)
   {
      numOfElements++;
   }

   sum = 0.0;
   for(Int32 i=0; i<numOfElements; i++)
   {
      sum += ringBuffer[i];
   }
   m_dispersionGasFlowLhFiltered = sum/numOfElements;
   
   // set mqtt values
   m_pUiCommunication->GetCyclicSendData()->fDispersionGasFlow = m_dispersionGasFlowLhFiltered;
   
   // set buchi bus trace values
   m_pTraceData->dispGasActual_lh = (UInt32)(m_dispersionGasFlowLh);
   m_pTraceData->dispGasActual_lh_filtered = (UInt32)(m_dispersionGasFlowLhFiltered);
   
   m_pTraceData->dispGasTarget = GetParamData()->dispersionGasTarget;
   m_pTraceData->dispGasError = 0;
   m_pTraceData->dispGasIpart = 0;
   m_pTraceData->dispGasPpart = 0;
   m_pTraceData->dispGasPwmOut = 0;
}

//-----------------------------------------------------------------------------
// Method call for the state process
//-----------------------------------------------------------------------------
void  CDispersionGas::StateCall(UInt32 nStateId, EMethodId eMethodId)
{
   switch (nStateId)
   {
    case STATE_IDLE:
      StateIdle(eMethodId);
      break;
    case STATE_DISPGAS_ON:
      StateDispersionGasOn(eMethodId);
      break;
    default:
      //CLogging::GetInstance()->Log_(GetMachineName(),CLogging::LOG_ERROR,"Unhandled state ID %d.",eMethodId,0,0);
      break;
   }
}

void CDispersionGas::UpdateObserver(CObservable* pObservable,UInt32 uiVal)
{
   if (uiVal == OBSERVABLE_MSGSYS_ACK)
   {           
      /*
      if (CUICommunication::GetInstance()->GetReceiveDataHandler()->GetLatestMsgAcknowledgeNb() == m_pMsgErrNoStoppering->GetId())       
      {
         m_pMsgErrNoStoppering->ChangeState(EVT_CONFIRM);
      }

      if (CUICommunication::GetInstance()->GetReceiveDataHandler()->GetLatestMsgAcknowledgeNb() == m_pMsgInfManualStoppering->GetId())
      {
         m_pMsgInfManualStoppering->ChangeState(EVT_CONFIRM);
      }
*/
   }
   else if ( uiVal == OBSERVABLE_TMR) // -> timer is observed by base class CStateMachine
   {
      CStateMachine::UpdateObserver(pObservable,uiVal);
   }
}

//-----------------------------------------------------------------------------
// string allocation
//-----------------------------------------------------------------------------
void CDispersionGas::InitState(void)
{
   m_ucUsedStates = 2u;
   CStateMachine::InitState();
}

//////////////////// methods from ILifeCycle interface /////////////////////////
// Transfer reference
void  CDispersionGas::InitSystem()
{
   InitState();

   SetStartupCallTime( 13 ); // check stoppering availability 1.3s after startup. Odd time to check when no other checks are done
      
   // use 100ms tick from time base observer
   CTimeBase::GetInstance()->GetTimeBaseObserver(CTimeBase::EN_BASE100)->AddObserver(this);
      
   // observe message system for ACKNOWLEDGE
   //CMessageSystem::GetInstance()->AddObserver(this);
}

//-----------------------------------------------------------------------------
// Method call for the state "StateIdle"
//-----------------------------------------------------------------------------
void  CDispersionGas::StateIdle(EMethodId eMethodId)
{  
   if (eMethodId == METHOD_ID_ENTER)
   {
      m_pUiCommunication->SendEventComponentStatusChange("dispersiongasstatuschanged","Off");
   }
   else if (eMethodId == METHOD_ID_RUN)
   {
      if (m_bEnable)
      {
         DoTransition(STATE_DISPGAS_ON);
      }
   }
   else if (eMethodId == METHOD_ID_EXIT)
   {
      m_pUiCommunication->SendEventComponentStatusChange("dispersiongasstatuschanged","On");
   }
}

//-----------------------------------------------------------------------------
// Method call for the state "StateDispersionGasOn"
//-----------------------------------------------------------------------------
void  CDispersionGas::StateDispersionGasOn(EMethodId eMethodId)
{  
   static UInt32 rampTimer = 0;
   static UInt16 rampOut = 0;
   static Int8 rampDir = +1;
   static Float32 Ta = 0.1;
   static Float32 error;
   static Float32 pPart;
   static Float32 iPart;
   static Float32 regOut;
   static Float32 kp;
   static Float32 ki;
   static Float32 offset;
   Float32 target = 0;
   
   if (eMethodId == METHOD_ID_ENTER)
   {
      // start the valve
      StartPwmValve1(MIN_PWM);

      // init regulator
      pPart = 0;
      iPart = 0;
      regOut = 0;
      m_lastTargetFlowLh = 0;
      
      // reset ramp 
      rampTimer = 0;
      rampOut = MIN_PWM;
   }
   else if (eMethodId == METHOD_ID_RUN)
   {
      UInt8 mode = GetParamData()->dispersionGasMode;
      
      ////////////////////// MANUELL //////////////////////////
      if(mode == 1)
      {
         target = (Float32) GetParamData()->dispersionGasTarget;
         if(target <= MAX_PWM && target >= MIN_PWM)
         {
            UpdatePwmValve1((UInt16)(target));
            m_pTraceData->dispGasPwmOut = (UInt32)target;
         }
         else
         {
            UpdatePwmValve1(0);
            m_pTraceData->dispGasPwmOut = 0;
         }
         // trace daten updaten
         m_pTraceData->dispGasError = 0;
         m_pTraceData->dispGasIpart = 0;
         m_pTraceData->dispGasPpart = 0;
         
      }
      ////////////////////// RAMPEN //////////////////////////
      else if(mode == 2)
      {
         rampTimer++;
         if(rampTimer%200 == 0)
         {
            rampOut = rampOut + rampDir * 10; // in 1% schritten
            if((rampOut >= MAX_PWM) || (rampOut <= MIN_PWM))
            {
               rampDir = rampDir * (-1);
            }
         }
         
         // Stellgrösse ausgeben
         UpdatePwmValve1(rampOut);
         
         // trace daten updaten
         m_pTraceData->dispGasError = 0;
         m_pTraceData->dispGasIpart = 0;
         m_pTraceData->dispGasPpart = 0;
         m_pTraceData->dispGasPwmOut = (UInt32)rampOut;
      }
      ////////////////////// Regelung //////////////////////////
      else if(mode == 3)
      {
         target = (Float32)GetParamData()->dispersionGasTarget;
         kp = GetParamData()->kpDispGas;
         ki = GetParamData()->kiDispGas;
         offset = (Float32)GetParamData()->offsetDispGas;
         error = target - m_dispersionGasFlowLh;
         //pPart = kp * error;
         pPart = (kp * target);         //y = 0.263x + 432.93
         iPart += ki * Ta * error;
         regOut = pPart + iPart + offset;
         
         // Stellgrösse begrenzen
         if(regOut > MAX_PWM)
         {
            regOut = MAX_PWM;
            iPart = MAX_PWM - pPart - offset;
         }
         else if(regOut < MIN_PWM)
         {
            regOut = MIN_PWM;
            iPart = MIN_PWM - pPart - offset;
         }
         
         // Stellgrösse ausgeben
         UpdatePwmValve1((UInt16)(regOut+0.5));
         
         // trace daten updaten
         m_pTraceData->dispGasError = error;
         m_pTraceData->dispGasIpart = iPart;
         m_pTraceData->dispGasPpart = pPart;
         m_pTraceData->dispGasPwmOut = (UInt32)(regOut+0.5);
      }

      // check transition
      if (!m_bEnable)
      {
         DoTransition(STATE_IDLE);
      }
   }
   else if (eMethodId == METHOD_ID_EXIT)
   {
      // reset regulator
      error = 0;
      pPart = 0;
      iPart = 0;
      regOut = 0;
      m_lastTargetFlowLh = 0;
      // trace daten updaten
      m_pTraceData->dispGasError = error;
      m_pTraceData->dispGasIpart = iPart;
      m_pTraceData->dispGasPpart = pPart;
      m_pTraceData->dispGasPwmOut = (UInt32)(regOut+0.5);
      StopPwmValve1();
   }
}
