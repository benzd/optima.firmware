/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------

Autor                  Datum
D. Benz                06.02.2020
Project:               Optima Sprydryer
-------------------------------------------------------------------------------



-- AENDERUNGEN ----------------------------------------------------------------


-----------------------------------------------------------------------------*/

#pragma once

#include "StateMachine.h"
#include "UiCommunication.h"

class CAirFlow : public CStateMachine
{
  public:
   CAirFlow();
   virtual ~CAirFlow();

   // Method call every cycle
   virtual void	CyclicCall();

   // Method call for the state process
   virtual void  StateCall(UInt32 nStateId, EMethodId eMethodId);

   // update function to be called, must be implemented
   void UpdateObserver(CObservable*,UInt32);
   
   // StartupCall function is called by base class after a given time after startup
   virtual void StartupCall();

   void Init(void);
   
   void On(void);
   
   void Off(void);

  protected:
   /////////////// ILifeCycle interface methods //////////////////////////

   // Transfer reference
   void  InitSystem();
    
   // startup
   void InitState(void);

  private:
     
   // class pointer
   CUICommunication* m_pUiCommunication;
   
   // enable command flag
   Bool m_bEnable;

   // States of this process
   enum enStates {STATE_IDLE, STATE_AirFlow_ON};

   ////////////////////// own state transition methods /////////////////////////   
   void  StateIdle(EMethodId eMethodId);
   void  StateAirFlowOn(EMethodId eMethodId);
   
};