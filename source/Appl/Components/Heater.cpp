/*-----------------------------------------------------------------------------

Büchi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------

Autor                  Datum
D. Benz                06.02.2020
Project:               Optima Sprydryer
-------------------------------------------------------------------------------



-- AENDERUNGEN ----------------------------------------------------------------


-----------------------------------------------------------------------------*/

#include "Heater.h"
#include "TimeBase.h"
#include "UiCommunication.h"

//-----------------------------------------------------------------------------
// Konstruktor
//-----------------------------------------------------------------------------
CHeater:: CHeater()
{
   m_pUiCommunication = 0;
   m_bEnable = false;
}

//-----------------------------------------------------------------------------
// Destruktor
//-----------------------------------------------------------------------------
CHeater::~CHeater()
{

}

//-----------------------------------------------------------------------------
// General initialization
//-----------------------------------------------------------------------------
void CHeater::Init(void)
{
   m_bEnable = false;
   
   m_pUiCommunication = CUICommunication::GetInstance();

   InitSystem();
}

//-----------------------------------------------------------------------------
// Enable the heater
//-----------------------------------------------------------------------------
void CHeater::On(void)
{
   m_bEnable = true;
}

//-----------------------------------------------------------------------------
// Disable the heater
//-----------------------------------------------------------------------------
void CHeater::Off(void)
{
   m_bEnable = false;
}

//-----------------------------------------------------------------------------
// Do startup check of stoppering system availability
//-----------------------------------------------------------------------------
void CHeater::StartupCall()
{
   CStateMachine::StartupCall();
}

//-----------------------------------------------------------------------------
// Method call every cycle
//-----------------------------------------------------------------------------
void CHeater::CyclicCall()
{
   m_pParameter = GetParamData();
   m_pIO = GetIOData();
   m_pTrace = m_pUiCommunication->GetRegTraceSendData();

   // Get sensor values
   m_inletTemperature = m_pIO->analogIn.inletTemperatureCelsius;
   //m_inletTemperature = m_pIO->analogIn.inletTemperatureCelsius + 100;
   m_outletTemperature = m_pIO->analogIn.outletTemperaturCelsius;
   
   // update cyclic UI data
   m_pUiCommunication->GetCyclicSendData()->fInletTemperature = m_inletTemperature;
   m_pUiCommunication->GetCyclicSendData()->fOutletTemperature = m_outletTemperature;;
   
   // update trace data (updated to correct values in PI Regulator method)
   m_pTrace->inletTempTarget = m_pParameter->inletTemperatureTarget/10;
   m_pTrace->inletTempActual = m_inletTemperature;
   m_pTrace->heaterError = 0;
   m_pTrace->heaterIpart = 0;
   m_pTrace->heaterPpart = 0;
   m_pTrace->heaterPwmOut = 0;
   m_pTrace->heatPowerW = 0;
   m_pTrace->ssr1 = 0;
   m_pTrace->ssr2 = 0;
   
   
}

//-----------------------------------------------------------------------------
// Method call for the state process
//-----------------------------------------------------------------------------
void  CHeater::StateCall(UInt32 nStateId, EMethodId eMethodId)
{
   switch (nStateId)
   {
    case STATE_IDLE:
      StateIdle(eMethodId);
      break;
    case STATE_HEATER_ON:
      StateHeaterOn(eMethodId);
      break;
    default:
      //CLogging::GetInstance()->Log_(GetMachineName(),CLogging::LOG_ERROR,"Unhandled state ID %d.",eMethodId,0,0);
      break;
   }
}

//-----------------------------------------------------------------------------
// Method call for Observer Update
//-----------------------------------------------------------------------------
void CHeater::UpdateObserver(CObservable* pObservable,UInt32 uiVal)
{
   if (uiVal == OBSERVABLE_MSGSYS_ACK)
   {           
      
   }
   else if ( uiVal == OBSERVABLE_TMR) // -> timer is observed by base class CStateMachine
   {
      CStateMachine::UpdateObserver(pObservable,uiVal);
   }
}

//-----------------------------------------------------------------------------
// Method call for State initialization
//-----------------------------------------------------------------------------
void CHeater::InitState(void)
{
   m_ucUsedStates = 2u;
   CStateMachine::InitState();
}

//-----------------------------------------------------------------------------
// Method call from ILifceCycle
//-----------------------------------------------------------------------------
void  CHeater::InitSystem()
{
   InitState();
   SetStartupCallTime( 13 ); // check stoppering availability 1.3s after startup. Odd time to check when no other checks are done
      
   // use 100ms tick from time base observer
   CTimeBase::GetInstance()->GetTimeBaseObserver(CTimeBase::EN_BASE100)->AddObserver(this);
}


//-----------------------------------------------------------------------------
// Method call for the state "Idle"
//-----------------------------------------------------------------------------
void  CHeater::StateIdle(EMethodId eMethodId)
{  
   if (eMethodId == METHOD_ID_ENTER)
   {
      m_pUiCommunication->SendEventComponentStatusChange("inlettemperaturestatuschanged","Off");
   }
   else if (eMethodId == METHOD_ID_RUN)
   {
      if (m_bEnable)
      {
         DoTransition(STATE_HEATER_ON);
      }
   }
   else if (eMethodId == METHOD_ID_EXIT)
   {
     m_pUiCommunication->SendEventComponentStatusChange("inlettemperaturestatuschanged","On");
   }
}

//-----------------------------------------------------------------------------
// Method call for the state "StateHeatUp"
//-----------------------------------------------------------------------------
void  CHeater::StateHeaterOn(EMethodId eMethodId)
{  
   if (eMethodId == METHOD_ID_ENTER)
   {
      //SetTimer(20, STATE_IDLE); //  20*100ms = 2s
      m_integralPart = 0;
      m_heatPower = 0;
   }
   else if (eMethodId == METHOD_ID_RUN)
   {
      if (!m_bEnable)
      {
         DoTransition(STATE_IDLE);
         return;
      }
      
      switch(m_pParameter->heaterMode)
      {
      case 1:
         m_heatPower = m_pParameter->heaterManPower;
         SetPWMOutput(m_heatPower);
         m_pTrace->heatPowerW = m_heatPower;
         break;
         
      case 2:
         break;
         
      case 3:
         PI_Controller();
         break;
      }
      
      
      
   }
   else if (eMethodId == METHOD_ID_EXIT)
   {
      m_pIO->digOut.bHeatSSR1 = FALSE;
      m_pIO->digOut.bHeatSSR2 = FALSE;
   }
}

//-----------------------------------------------------------------------------
// Method PI Regulator
//-----------------------------------------------------------------------------
void  CHeater::PI_Controller(void)
{
   Float32 error = 0.0;
   Float32 targetTemperature = 0.0;
   Float32 yP = 0.0;
   Float32 y = 0.0;
   Float32 actOffset = 0;
   Float32 tempIntegral = 0.0;

   targetTemperature = (Float32)m_pParameter->inletTemperatureTarget/10;

   error = targetTemperature - m_inletTemperature;

   // Wenn Abweichung gross maximal heizen oder abschalten
   if((error > 20.0) || (error < -5)) 
   {
      if(error > 0)     y = 100;
      else              y = 0.0;
   }
   else
   {
      yP = (error * m_pParameter->kpHeater);
      tempIntegral = m_integralPart + (error * m_pParameter->kiHeater);
      //y = yP + m_integralPart + m_pParameter->offsetHeater;
      actOffset = ((targetTemperature * 0.4) - 11);
      y = yP + tempIntegral + actOffset;
   }

   // Stellgrössenbegrenzung
   if(y < 0) 
   {
      y = 0;
   }
   else if(y > 100)
   {
      y = 100;
   }
   else
   {
   m_integralPart = tempIntegral;
   }

   m_heatPower = (UInt16)((y * HEATPOWER_MAX) / 100.0);
   
   SetPWMOutput(m_heatPower);
   
   // trace daten updaten
   m_pTrace->heaterError = error;
   m_pTrace->heaterIpart = m_integralPart;
   m_pTrace->heaterPpart = yP;
   m_pTrace->heaterPwmOut = (UInt32)(y+0.5);
}


//-----------------------------------------------------------------------------
// Method SetPWMOutput
//-----------------------------------------------------------------------------
void CHeater::SetPWMOutput(UInt16 heatPowerW)
{
   static UInt8 flickerState = 0;
   static UInt32 flickerPwmTimer = 0;
   UInt32 dutyMs = 0;
   
   switch(flickerState)
   {
   // IDLE
   case 0:
      flickerPwmTimer = 0;
      if(heatPowerW > 0)
      {
         flickerState = 1;
      }
      else
      {
         m_pIO->digOut.bHeatSSR1 = FALSE;
         m_pIO->digOut.bHeatSSR2 = FALSE;
      }
      break;
      
   // ON
   case 1:
      flickerPwmTimer++;
      
      if(m_heatPower >= HEATPOWER_H1)
      {
         dutyMs = (((m_heatPower-HEATPOWER_H1) * 100 / HEATPOWER_H2)* FLICKER_INTERVALL_MS) / 100;
         m_pIO->digOut.bHeatSSR1 = TRUE;
         m_pIO->digOut.bHeatSSR2 = TRUE;
         if(flickerPwmTimer >= dutyMs/100)
         {
            m_pIO->digOut.bHeatSSR1 = TRUE;
            if(m_heatPower < HEATPOWER_MAX)
            {
               m_pIO->digOut.bHeatSSR2 = FALSE;
            }
            flickerState = 2;
         }
      }
      else if(m_heatPower >= HEATPOWER_H2)
      {
         dutyMs = (((m_heatPower-HEATPOWER_H2) * 100 / HEATPOWER_H2)* FLICKER_INTERVALL_MS) / 100;
         m_pIO->digOut.bHeatSSR1 = TRUE;
         m_pIO->digOut.bHeatSSR2 = FALSE;
         if(flickerPwmTimer >= dutyMs/100)
         {
            m_pIO->digOut.bHeatSSR1 = FALSE;
            m_pIO->digOut.bHeatSSR2 = TRUE;
            flickerState = 2;
         }
      }
      else
      {
         dutyMs = ((m_heatPower * 100 / HEATPOWER_H2)* FLICKER_INTERVALL_MS) / 100;
         m_pIO->digOut.bHeatSSR1 = FALSE;
         m_pIO->digOut.bHeatSSR2 = TRUE;
         if(flickerPwmTimer >= dutyMs/100)
         {
            m_pIO->digOut.bHeatSSR1 = FALSE;
            m_pIO->digOut.bHeatSSR2 = FALSE;
            flickerState = 2;
         }
      }
      break;
      
   // OFF
   case 2:
      flickerPwmTimer++;
      if(flickerPwmTimer >= (FLICKER_INTERVALL_MS / 100))
      {
         flickerState = 0;
      }
      break;
   }
   
   m_pTrace->heatPowerW = m_heatPower;
   m_pTrace->ssr1 = (Int8)m_pIO->digOut.bHeatSSR1 * 20;
   m_pTrace->ssr2 = (Int8)m_pIO->digOut.bHeatSSR2 * 25;
}