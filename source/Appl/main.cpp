/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------
Autor                  Datum
R. Kaufmann            26.06.2019

-- BESCHREIBUNG ---------------------------------------------------------------
System-Startup und Tasks erzeugen.

-- AENDERUNGEN ----------------------------------------------------------------
Autor                  Datum

-----------------------------------------------------------------------------*/

#include <string>

using namespace std;

#include "BuchiBus.h"
#include "BusTelegram.h"
#include "BuchiTypes.h"
#include "eth.h"
#include "TimeBase.h"
#include "UICommunication.h"
#include "ProcessControl.h"
#include "Sample.h"

extern "C"
{
   #include "board.h"
   #include "pin_mux.h"
   #include "clock_config.h"
   #include "fsl_iomuxc.h"
   #include "fsl_gpio.h"
   #include "fsl_xbara.h"
   #include "fsl_enc.h"
   #include "fsl_wdog.h"
   #include "FreeRTOS.h"
   #include "stdout_fifo.h"
   #include "Hardware.h"
}

// Debug-Ausgaben ein/aus
#ifdef DEBUG
   #define DEBUG_PRINT(...) \
      do { PRINTF(__VA_ARGS__); } while (0)
#else
   #define DEBUG_PRINT(...) do {} while (0)
#endif

// B�chi-Bus Task, da sie timingkritische Aktionen durchf�hrt und auf die 
// gleichen Daten zugreift wie die main_task, darf sie weder
// von der main_task unterbrochen werden noch vom tcp_thread.
#define buchibus_task_PRIO       8
#define buchibus_task_INTERVAL   5
#define buchibus_task_ADDRESS    0x85

// Main Task, Programmhauptschleife
#define main_task_PRIO           6
#define main_task_INTERVAL       10

CTimeBase* m_pTimeBase;
CProcessControl* m_pProcessControl;
CUICommunication* m_pUICommunication;

//-------------------------------------------------------------------------------------------------
static void delay(void)
//-------------------------------------------------------------------------------------------------
{
    volatile uint32_t i = 0;
    for (i = 0; i < 1000000; ++i)
    {
        __asm("NOP"); /* delay */
    }
}

//-------------------------------------------------------------------------------------------------
static void buchibus_task(void *pvParameters)
//-------------------------------------------------------------------------------------------------
{
   TickType_t xNextWakeTime = xTaskGetTickCount();

   DEBUG_PRINT("buchibus_task started.\r\n");
   
   InitializeBus();
   
   StartBusCommunication(buchibus_task_ADDRESS);
   
   for (;;)
   {
      LoopTelegram();
      vTaskDelayUntil(&xNextWakeTime, buchibus_task_INTERVAL);
   }
}

//-------------------------------------------------------------------------------------------------
static void main_task(void *pvParameters)
//-------------------------------------------------------------------------------------------------
{
   MACAddressTy mac;
   //ip4_addr_t ip, nm, gw;
   TickType_t xNextWakeTime = xTaskGetTickCount();

   DEBUG_PRINT("main_task started.\r\n");
   
   // Initialize the hardware layer
   InitHardware();

   // Setup MAC 
   mac.b0 = 0x02;
   mac.b1 = 0x12;
   mac.b2 = 0x13;
   mac.b3 = 0x10;
   mac.b4 = 0x15;
   mac.b5 = 0x11;
   SetMACAddress(mac); // Hier bleibt man h�ngen wenn kein ETH Kabel angeschlossen!
   
   SetIP4viaDHCP(true);
   
   // Warten bis eth ready
   while (IsEth0Busy())
   {
      WDOG_Refresh(WDOG1);
      vTaskDelayUntil(&xNextWakeTime, main_task_INTERVAL);
   }

   // IP setzen
   //IP4_ADDR(&ip, 10, 93, 152, 92);
   //IP4_ADDR(&nm, 255, 255, 255, 0);
   //IP4_ADDR(&gw, 10, 93, 152, 1);
   //SetIP4(ip, nm, gw);

   m_pTimeBase = CTimeBase::GetInstance();
   m_pTimeBase->SetTickLength(100000u); // 1 tick is 100ms
   m_pTimeBase->SetDateTime(0,0,0,0,0,0); // invalid date time
   
   m_pProcessControl = CProcessControl::GetInstance();
   m_pUICommunication = CUICommunication::GetInstance();

   if ( m_pUICommunication != 0 && m_pProcessControl != 0)
   {
      m_pUICommunication->Init();
      m_pProcessControl->Init();
      m_pUICommunication->Start();              // UI Klasse starten und zum MQTT Broker verbinden
      m_pProcessControl->StartSystem();         // startet die Statemachine
      m_pProcessControl->Start();               // startet die sub-Komponenten
   }
   
   xTaskCreate(SampleTaskWrapper, "sample_task", 1024, NULL, 4, NULL);  
   

   for (;;)
   {
      // Get Inputs
      GetHardware();
      
      // Trigger all the timer observed classes
      //if ( m_pProcessControl != 0 && m_pUICommunication->m_mqttClient.IsConnected())
      if ( m_pProcessControl != 0 )
      {
         m_pTimeBase->Tick10();
      }
      
      // Set Outputs
      SetHardware();

      // Watchdog refresh und delay den task
      WDOG_Refresh(WDOG1);
      vTaskDelayUntil(&xNextWakeTime, main_task_INTERVAL);
   }
}

//-------------------------------------------------------------------------------------------------
int main(void)
//-------------------------------------------------------------------------------------------------
{
   const clock_enet_pll_config_t config = {.enableClkOutput = true, .enableClkOutput25M = false, .loopDivider = 1};
   gpio_pin_config_t gpio_config_out = {kGPIO_DigitalOutput, 0, kGPIO_NoIntmode};

   /* Reingefallene Fallgruben in diesem Projekt, wichtig zu wissen wenn man den Code woanders nutzt:
      - F�r DRAM Nutzung siehe unten SCB_DisableDCache();
      - F�r DRAM Nutzung Pr�prozesserdefine SKIP_SYSCLK_INIT in Compiler-Optionen.
      - Wir legen aus Performancegr�nden den Stack ins interne RAM.
      - Generell alle Compileroptionen checken.
      - Pinkonfiguration SWO war falsch (wird nicht mehr benutzt).
      - Linkercontrol-File pr�fen.
      - Startup-Code unten genauestens pr�fen, ebenso die Clock-Konfiguration!
      - In den Projektoptionen vom Linker die Konfigurationsstrukturen f�r die CPU listen (Keep symbols), sonst werden die wegoptimiert.
      - Der Bootloader muss die dcd Struktur zur Nutzung des SDRAM konfigurieren.
      - BOARD_InitBootPins() muss aufgerufen werden, BOARD_InitPins() reicht nicht, weil sonst die Peripherie nicht richtig initialisiert wird.
        Der Grund ist, dass in der .mex Datei die Pins f�r Ethernet etc. einer anderen funktionalen Gruppe zugeordnet sind.
        Also bei den Inits im Startup darauf achten, dass die der richtigen funktionalen Gruppen aufgerufen werden.
   */

   BOARD_ConfigMPU();
   BOARD_InitBootPins();
   BOARD_BootClockRUN();
          
   CLOCK_InitEnetPll(&config);

   IOMUXC_EnableMode(IOMUXC_GPR, kIOMUXC_GPR_ENET1TxClkOutputDir, true);

   /* Data cache must be temporarily disabled to be able to use sdram */
   SCB_DisableDCache();

   GPIO_PinInit(GPIO1, 9, &gpio_config_out);
   GPIO_PinInit(GPIO1, 10, &gpio_config_out);
   /* Pull up the ENET_INT before RESET. */
   GPIO_WritePinOutput(GPIO1, 10, 1);
   GPIO_WritePinOutput(GPIO1, 9, 0);
   delay();
   GPIO_WritePinOutput(GPIO1, 9, 1);
    
   // Debugging zu stdout aktivieren.
   EnableStdOutBuf(1);
   DEBUG_PRINT("Template started.\r\n");
   
   // Tasks erzeugen  
   xTaskCreate(buchibus_task, "buchibus_task", 1024, NULL, buchibus_task_PRIO, NULL);  
   xTaskCreate(main_task, "main_task", 4096, NULL, main_task_PRIO, NULL);

   // Tasks abarbeiten
   vTaskStartScheduler();
   
   // Wir kommen hier nie an, ausser eine Task ruft vTaskEndScheduler() auf.
   return 0;
}

