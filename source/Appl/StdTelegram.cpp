/*-----------------------------------------------------------------------------

Büchi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------

Autor                  Datum
R. Kaufmann            14.11.2018

-- BESCHREIBUNG ---------------------------------------------------------------
Kommunikation Standard-Telegramme über Büchi-Bus.

-- AENDERUNGEN ----------------------------------------------------------------


-----------------------------------------------------------------------------*/
#include "StdTelegram.h"
#include "BusTelegram.h"

extern "C"
{
   #include "string.h"
}


///////////////////////////////////////////////////////////////////////////////
static void WriteUINT16ToBuffer(UINT8* pBuf, UINT16 data)
{  // data wird im little endian Format nach pBuf geschrieben:
   *(pBuf+0) =  data & 0x00ff;       // niederwertiges Byte
   *(pBuf+1) = (data & 0xff00) >> 8; // höherwertiges Byte
}

///////////////////////////////////////////////////////////////////////////////
static void WriteINT16ToBuffer(UINT8* pBuf, INT16 data)
{  // data wird im little endian Format nach pBuff geschrieben:
   WriteUINT16ToBuffer(pBuf,(UINT16)data);
}

///////////////////////////////////////////////////////////////////////////////
static INT16 ReadInt16FromBuffer(UINT8* pBuffer)
{
   INT16 value;

   value  = *(pBuffer+1);       // höherwertiges Byte
   value  = value << 8;
   value += *pBuffer;           // niederwertiges Byte
   return value;
}

//-----------------------------------------------------------------------------
UInt32 DoStdTelegram(UInt8 command, UInt8 * pRecData, UInt32 recDataLen, UInt8 * pSendData)
//-----------------------------------------------------------------------------
{
   UINT8 len = 0;

   switch (command)
   {
   case 0x12: // Serienummer Read/Write
      if (recDataLen == 0)
      {  // Serienummer wird angefordert, also zurücksenden
         //DataIO::GetData(DE_SerialNumber, pSendData);
         strcpy((char*)pSendData, (char*)"11223344");
         len = strlen((char*)pSendData);
      }
      else
      {  // Die Serienummer wird übergeben, also speichern
         pRecData[recDataLen] = 0;
         //DataIO::SetData(DE_SerialNumber, pRecData);
         len = 0;                         // keine Daten zurücksenden
      }
      break;

   case 0x13: // Betriebsstunden                
      if (recDataLen == 0)
      {  // Betriebsstunden werden angefordert, also zurücksenden
         UInt32 minutes = 368;
         //DataIO::SetData(DE_OperatingMinutes, minutes);
         *(UInt32*)&pSendData[0] = minutes / 60;
         len = 2;
      }
      else
      {  // Die Betriebsstunden werden übergeben, also speichern
         if (recDataLen >= 2)
         {
            UInt32 minutes = 60 * (pRecData[0] + (pRecData[1] << 8));
            //DataIO::SetData(DE_OperatingMinutes, minutes);
         }                        
      }
      break;

   case 0x17: // PrintTestDatum Read/Write
      if (recDataLen == 0)
      {  // Das Printtest-Datum wird angefordert, also zurücksenden
         DateTY date;

         //if (!DataIO::GetData(DE_PrintTestDate, date))
           //memset(&date, 0xff, sizeof(date));
         len = 4;
         pSendData[0] = (UInt8)date.year;
         pSendData[1] = (UInt8)(date.year >> 8);
         pSendData[2] = date.month;
         pSendData[3] = date.day;
      }
      else
      {  // Das Printtestdatum wird übergeben, also speichern
         DateTY date;

         date.year = pRecData[0] + (pRecData[1] << 8);
         date.month = pRecData[2];
         date.day  = pRecData[3];
         //DataIO::SetData(DE_PrintTestDate, date);
         len = 0;                         
      }
      break;

   case 0x18: // Mac Adresse lesen
      len = 0;
      if (recDataLen == 0)
      {  // Wird angefordert, also zurücksenden
         /*if (GetMacAddress(pSendData))
         {
            len = 6;
         }*/
      }
      return len;
      break;

   case 0x30: // Memory RW
      {
         UInt32 *pData32 = *(UInt32**)&pRecData[0];
         UInt16 *pData16 = *(UInt16**)&pRecData[0];
         UInt8  *pData8  = *(UInt8 **)&pRecData[0];
         UInt8  *pDest = &pSendData[0];
         UInt8 accessMode, countElements, totLenRead;
         accessMode = pRecData[4];
         countElements = pRecData[5];
         totLenRead = 0;
         if (recDataLen == 6)
         {  // Read
            UInt32 data32;
            UInt8 lenRead;
            while (countElements && totLenRead < 251)
            {
               switch (accessMode)
               {
               case 1:  data32 = *pData16++; lenRead = 2; break;
               case 2:  data32 = *pData32++; lenRead = 4; break;
               default: data32 = *pData8++;  lenRead = 1; break;
               }
               memcpy(pDest, &data32, lenRead);
               countElements--;
               totLenRead += lenRead;
               pDest += lenRead;
            }
            len = totLenRead;
         }
         else if (recDataLen > 6)
         {  // Write
            switch (accessMode)
            {
            case 1:  *pData16 = *(UInt16*)&pRecData[6]; break;
            case 2:  *pData32 = *(UInt32*)&pRecData[6]; break;
            default: *pData8  = *(UInt8* )&pRecData[6]; break;
            }
         }
      }
      break;

   case 0x31: // EEPROM RW
      /*{
         UInt32 adr;
         UINT8  lenRead;
         UINT8  accessMode;
         
         adr = *(UInt32*)&pRecData[0];
         accessMode = pRecData[4];
         switch (accessMode)
         {
         case 1:  lenRead = 2; break;
         case 2:  lenRead = 4; break;
         default: lenRead = 1; break;
         }
         if (recDataLen == 6)
         {  // Read from EEPROM
            len = lenRead * pRecData[5];
            if (!EEPROMLoadDataShadow(pSendData, adr, len))
            {
               len = 0;
            }
         }
         else if (recDataLen > 6)
         {  // Write to EEPROM
            len = recDataLen - 6;
            EEPROMStoreDataShadow(&pRecData[6], adr, len);
            len = 0;
         }
      }*/
      break;

   case 0x32: // SetFactoryDefault
      {
         if (recDataLen == 0)
         {
            //DatabaseInit();            
         }
      }
      break;

   case 0x33: // RTC Read/Write:
      if (recDataLen == 0)   // Zeit lesen
      {
         TimeDateTY tm;

         /*if (GetRTC(&tm))
         {
            pSendData[2] = tm.date.month;
            pSendData[3] = tm.date.day;
            pSendData[4] = tm.time.hour;
            pSendData[5] = tm.time.min;
            pSendData[6] = tm.time.sec;
            WriteINT16ToBuffer(pSendData, tm.date.year);
            len = 7;
         }
         else*/
            len = 0;
      }
      else if (recDataLen >= 7) // Zeit schreiben
      {
         TimeDateTY tm;

         tm.date.year = ReadInt16FromBuffer(pRecData);
         tm.date.month = pRecData[2];
         tm.date.day = pRecData[3];
         tm.time.hour = pRecData[4];
         tm.time.min = pRecData[5];
         tm.time.sec = pRecData[6];
         //SetRTC(&tm);
         len = 0;
      }
      break;

   case 0x38: // MAC-gebundene Signaturdatei schreiben
      /*{
         static UInt32 totalWritten = 0;
         UInt8 fileType, command;
         UInt16 blockNumber;
         UInt32 dataToWrite;
         fileType = pRecData[0];
         command  = pRecData[1];
         blockNumber = *(UInt16*)&pRecData[2];
         pSendData[0] = 1; // 1=error
         len = 1;
         if (fileType == 1)
         {
            if (command == 1)
            {
               UInt32 fileLen;
               DataIO::GetData(DE_MacSignatureSize, fileLen);
               memcpy(pSendData, &fileLen, 4);
               len = 4;
            }
            else if (command == 10)
            {
               totalWritten = 0;
               pSendData[0] = DataIO::SetData(DE_MacSignatureSize, totalWritten);
            }
            else if (command == 11)
            {
               UInt32 signatureSize = 0;
               static char macSigFile[MAC_SIGNATURE_SIZE] = {0};

               dataToWrite = 0;
               if (blockNumber && recDataLen > 4)
               {
                  dataToWrite = recDataLen - 4;
                  memcpy(macSigFile + totalWritten, &pRecData[4], dataToWrite);
                  totalWritten += dataToWrite;
               }
               
               DataIO::GetData(DE_MacSignatureSize, signatureSize);
               if (totalWritten == signatureSize)
               {
                  DataIO::SetData(DE_MacSignature, macSigFile);
               }
            }
            else if (command == 12)
            {
               pSendData[0] = DataIO::SetData(DE_MacSignatureSize, totalWritten);
            }
         }
      }*/
      break;
   }
   return len;
}



