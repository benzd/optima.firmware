/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------

Autor                  Datum
Felix Sch�r            09.04.2014
Project:               L-200 / L-300

-- BESCHREIBUNG ---------------------------------------------------------------

Communication interface to the UI
Singleton class
Receive and send Data stored as dual port RAM

-- AENDERUNGEN ----------------------------------------------------------------


-----------------------------------------------------------------------------*/
#pragma once

#include "Observer.h"
#include "mqtt_client.h"
#include "Telegram.h"
//#include "ProcessControl.h"
//#include "OperationPermissions.h"

extern"C"
{
  #include "BuchiTypes.h"
}

class CUICommunication : public IObserver
{
 
 public:
   
   static CUICommunication* GetInstance();
   
   void Init(void);   
   void Start(void);  

   // Get source for send data block
   SPRAYDRYER_CYCLIC_OUT_STATUS* GetCyclicSendData(void);
   
   // Get source for regulator trace block
   SPRAYDRYER_REG_TRACE* GetRegTraceSendData(void);

   // mqtt client instance
   MQTTClient m_mqttClient;
   
   // Trigger a publish of a component status changed
   void SendEventComponentStatusChange(const char* pcComponent, const char* pcNewStatus);
   
   // Trigger a publish of a instrument status changed
   void SendEventInstrumentStatusChange(const char* pcNewStatus);
   
   // function to send the actual parameters
   void SendEventParameterChanged(void);
   
   // function to send the actual configuration
   void SendEventConfigurationChanged(void);
   
   // update function to be called from observer, must be implemented (tick)
   void UpdateObserver(CObservable*,UInt32);
   
 protected:
    
   // constructor
   CUICommunication(void);
   
   // destructor
   ~CUICommunication(void);
   
   // for any communication
   static CUICommunication* m_pInstance;   
   
 private:

   // function to send the instrument process data cyclic
   void SendEventCyclicProcessData(void);
   
   // function to parse the received new parameters
   void parseSetParameterRequest(vector <UInt8> & vData);
   
   // function to setup the last will message
   void SetupLastWill(void);
   
   // the topics
   const char* m_SUBJECT_INSTRUMENT_STATUS = "v1/system/event/status";
   const char* m_SUBJECT_PROC_DATA = "v1/system/event/processdata";
   const char* m_SUBJECT_COMPONENT_STATUS = "v1/system/event/";
   const char* m_SUBJECT_PARAMETER_CHANGED = "v1/system/event/parameterchanged";
   const char* m_SUBJECT_CONFIG_CHANGED = "v1/system/event/configurationchanged";
   const char* m_SUBJECT_RPC_CMD = "v1/system/command/+/+";

   // id's for each subscribed topic (needed to unsuscribe)
   UInt32 m_uiSubscriptionID_rpcManCmd;
   
   // output data structure of cyclic process data
   SPRAYDRYER_CYCLIC_OUT_STATUS m_SEND_CYCLIC_OUT_Data;
   
   // output data structure for "life" regulator tracing
   SPRAYDRYER_REG_TRACE m_SEND_REG_TRACE_Data;
   
};