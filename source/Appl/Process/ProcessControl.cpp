/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------

Autor                  Datum
F. Sch�r               17.09.2014
Project:               L-200 / L-300
-- BESCHREIBUNG ---------------------------------------------------------------

Implementation der Klasse StopperingControl

-- AENDERUNGEN ----------------------------------------------------------------


-----------------------------------------------------------------------------*/

#include "ProcessControl.h"
#include "TimeBase.h"
#include "UiCommunication.h"

// static instance pointer initialization
CProcessControl* CProcessControl::m_pInstance = 0;

//-----------------------------------------------------------------------------
// Konstruktor (private because its singleton)
//-----------------------------------------------------------------------------
CProcessControl:: CProcessControl()
{
   // components
   m_pHeater = 0;
   m_pDispersionGas = 0;
   m_pAirFlow = 0;
   
   m_pHeater = new CHeater();
   m_pDispersionGas = new CDispersionGas();
   m_pAirFlow = new CAirFlow();
   
   // Variables
   cmdRequest = 0;
}

//-----------------------------------------------------------------------------
// Destruktor
//-----------------------------------------------------------------------------
CProcessControl::~CProcessControl()
{

}

//-----------------------------------------------------------------------------
// singleton
//-----------------------------------------------------------------------------
CProcessControl* CProcessControl::GetInstance()
{
   if (m_pInstance == 0)
   {
      m_pInstance = new CProcessControl();
   }
   
   return m_pInstance;
}

//-----------------------------------------------------------------------------
// General initialization
//-----------------------------------------------------------------------------
void CProcessControl::Init(void)
{
   // Init the process control
   InitSystem();

   // components
   if (m_pAirFlow != 0)
   {
      m_pAirFlow->Init();
   }
   if (m_pHeater != 0)
   {
      m_pHeater->Init();
   }
   if (m_pDispersionGas != 0)
   {
      m_pDispersionGas->Init();
   }
}

//-----------------------------------------------------------------------------
// Launch system
//-----------------------------------------------------------------------------
void CProcessControl::Start(void)
{
   // components
   m_pAirFlow->StartSystem();
   m_pHeater->StartSystem();
   m_pDispersionGas->StartSystem();
}

//-----------------------------------------------------------------------------
// DoCommand
//-----------------------------------------------------------------------------
void CProcessControl::DoCommand(enCommand cmd)
{
   switch (cmd)
   {
   case CMD_AUTO_START: 
      cmdRequest |= FLAG_AUTO_START; 
      break;
   case CMD_AUTO_STOP: 
      cmdRequest &= ~FLAG_AUTO_START;
      break;
   case CMD_AUTO_SKIP: 
      cmdRequest |= FLAG_AUTO_SKIP; 
      break;
   case CMD_MAN_DRYINGGAS_ON: 
      cmdRequest |= FLAG_MAN_DRYINGGAS_EN; 
      break;
   case CMD_MAN_DRYINGGAS_OFF: 
      cmdRequest &= ~FLAG_MAN_DRYINGGAS_EN; 
      break;
   case CMD_MAN_HEATER_ON: 
      cmdRequest |= FLAG_MAN_HEATER_EN; 
      break;
   case CMD_MAN_HEATER_OFF: 
      cmdRequest &= ~FLAG_MAN_HEATER_EN; 
      break;
   case CMD_MAN_DISPERSIONGAS_ON: 
      cmdRequest |= FLAG_MAN_DISPERSIONGAS_EN; 
      break;
   case CMD_MAN_DISPERSIONGAS_OFF: 
      cmdRequest &= ~FLAG_MAN_DISPERSIONGAS_EN; 
      break;
      
   default:
      break;
   }

}

//-----------------------------------------------------------------------------
// Do startup check of stoppering system availability
//-----------------------------------------------------------------------------
void CProcessControl::StartupCall()
{
   CStateMachine::StartupCall();
}

//-----------------------------------------------------------------------------
// Method call every cycle
//-----------------------------------------------------------------------------
void CProcessControl::CyclicCall()
{
   // send values from this process to user interface
   SPRAYDRYER_CYCLIC_OUT_STATUS* actOutputData = CUICommunication::GetInstance()->GetCyclicSendData();
   actOutputData->fOxygenConcentration = 12.0 + (rand() % 3 + 1);
   actOutputData->fProductTemperature = 25.6 + (rand() % 10 + 1);
}

//-----------------------------------------------------------------------------
// Method call for the state process
//-----------------------------------------------------------------------------
void  CProcessControl::StateCall(UInt32 nStateId, EMethodId eMethodId)
{
   switch (nStateId)
   {
     case STATE_INIT:
      StateInit(eMethodId);
      break;
    case STATE_IDLE:
      StateIdle(eMethodId);
      break;
    case STATE_HEAT_UP:
      StateHeatUp(eMethodId);
      break;
    case STATE_INTERTIZE:
      StateInertize(eMethodId);
      break;
    case STATE_EQUILIBRATION:
      StateEquilibration(eMethodId);
      break;
    case STATE_SPRAY_DRYING:
      StateSpryDrying(eMethodId);
      break;
    case STATE_NOZZLE_PULSES:
      StateNozzlePulses(eMethodId);
      break;
    case STATE_FLUSHING_NOZZLE:
      StateFlushingNozzle(eMethodId);
      break;
    case STATE_COOL_DOWN:
      StateCoolDown(eMethodId);
      break;
    case STATE_SHUT_DOWN:
      StateShutDown(eMethodId);
      break;
    case STATE_MANUAL:
      StateManual(eMethodId);
      break;
    default:
      //CLogging::GetInstance()->Log_(GetMachineName(),CLogging::LOG_ERROR,"Unhandled state ID %d.",eMethodId,0,0);
      break;
   }
}

void CProcessControl::UpdateObserver(CObservable* pObservable,UInt32 uiVal)
{
   if (uiVal == OBSERVABLE_MSGSYS_ACK)
   {
   }
   else if ( uiVal == OBSERVABLE_TMR) // -> timer is observed by base class CStateMachine
   {
      CStateMachine::UpdateObserver(pObservable,uiVal);
   }
}

//-----------------------------------------------------------------------------
// string allocation
//-----------------------------------------------------------------------------
void CProcessControl::InitState(void)
{
   m_ucUsedStates = 11u;
   CStateMachine::InitState();
}

//////////////////// methods from ILifeCycle interface /////////////////////////
// Transfer reference
void  CProcessControl::InitSystem()
{
   InitState();
   SetStartupCallTime( 13 ); // check stoppering availability 1.3s after startup. Odd time to check when no other checks are done
      
   // use 100ms tick from time base observer
   CTimeBase::GetInstance()->GetTimeBaseObserver(CTimeBase::EN_BASE100)->AddObserver(this);
      
   // observe message system for ACKNOWLEDGE
   //CMessageSystem::GetInstance()->AddObserver(this);
}


//-----------------------------------------------------------------------------
// Method call for the state "Init"
//-----------------------------------------------------------------------------
void  CProcessControl::StateInit(EMethodId eMethodId)
{  
   if (eMethodId == METHOD_ID_ENTER)
   {
      CUICommunication::GetInstance()->SendEventInstrumentStatusChange("Unknown");
      SetTimer(30, STATE_IDLE); // 30*100ms = 3s
   }
   else if (eMethodId == METHOD_ID_RUN)
   {
      
   }
   else if (eMethodId == METHOD_ID_EXIT)
   {
      
   }
}

//-----------------------------------------------------------------------------
// Method call for the state "Idle"
//-----------------------------------------------------------------------------
void  CProcessControl::StateIdle(EMethodId eMethodId)
{  
   if (eMethodId == METHOD_ID_ENTER)
   {
      CUICommunication::GetInstance()->SendEventInstrumentStatusChange("Idle");
   }
   else if (eMethodId == METHOD_ID_RUN)
   {
      if(cmdRequest & 0xFFFF0000)
      {
         DoTransition(STATE_MANUAL);
      }
   }
   else if (eMethodId == METHOD_ID_EXIT)
   {
      CUICommunication::GetInstance()->SendEventInstrumentStatusChange("Running");
   }
}

//-----------------------------------------------------------------------------
// Method call for the state "StateHeatUp"
//-----------------------------------------------------------------------------
void  CProcessControl::StateHeatUp(EMethodId eMethodId)
{  
   if (eMethodId == METHOD_ID_ENTER)
   {
   }
   else if (eMethodId == METHOD_ID_RUN)
   {
   }
   else if (eMethodId == METHOD_ID_EXIT)
   {
   }
}

//-----------------------------------------------------------------------------
// Method call for the state "StateInertize"
//-----------------------------------------------------------------------------
void  CProcessControl::StateInertize(EMethodId eMethodId)
{  
   if (eMethodId == METHOD_ID_ENTER)
   {
   }
   else if (eMethodId == METHOD_ID_RUN)
   {
   }
   else if (eMethodId == METHOD_ID_EXIT)
   {
   }
}

//-----------------------------------------------------------------------------
// Method call for the state "StateEquilibration"
//-----------------------------------------------------------------------------
void  CProcessControl::StateEquilibration(EMethodId eMethodId)
{  
   if (eMethodId == METHOD_ID_ENTER)
   {
   }
   else if (eMethodId == METHOD_ID_RUN)
   {
   }
   else if (eMethodId == METHOD_ID_EXIT)
   {
   }
}

//-----------------------------------------------------------------------------
// Method call for the state "StateSpryDrying"
//-----------------------------------------------------------------------------
void  CProcessControl::StateSpryDrying(EMethodId eMethodId)
{  
   if (eMethodId == METHOD_ID_ENTER)
   {
   }
   else if (eMethodId == METHOD_ID_RUN)
   {
   }
   else if (eMethodId == METHOD_ID_EXIT)
   {
   }
}
//-----------------------------------------------------------------------------
// Method call for the state "StateNozzlePulses"
//-----------------------------------------------------------------------------
void  CProcessControl::StateNozzlePulses(EMethodId eMethodId)
{  
   if (eMethodId == METHOD_ID_ENTER)
   {
   }
   else if (eMethodId == METHOD_ID_RUN)
   {
   }
   else if (eMethodId == METHOD_ID_EXIT)
   {
   }
}

//-----------------------------------------------------------------------------
// Method call for the state "StateFlushingNozzle"
//-----------------------------------------------------------------------------
void  CProcessControl::StateFlushingNozzle(EMethodId eMethodId)
{  
   if (eMethodId == METHOD_ID_ENTER)
   {
   }
   else if (eMethodId == METHOD_ID_RUN)
   {
   }
   else if (eMethodId == METHOD_ID_EXIT)
   {
   }
}

//-----------------------------------------------------------------------------
// Method call for the state "StateCoolDown"
//-----------------------------------------------------------------------------
void  CProcessControl::StateCoolDown(EMethodId eMethodId)
{  
   if (eMethodId == METHOD_ID_ENTER)
   {
   }
   else if (eMethodId == METHOD_ID_RUN)
   {
   }
   else if (eMethodId == METHOD_ID_EXIT)
   {
   }
}

//-----------------------------------------------------------------------------
// Method call for the state "StateShutDown"
//-----------------------------------------------------------------------------
void  CProcessControl::StateShutDown(EMethodId eMethodId)
{  
   if (eMethodId == METHOD_ID_ENTER)
   {
   }
   else if (eMethodId == METHOD_ID_RUN)
   {
   }
   else if (eMethodId == METHOD_ID_EXIT)
   {
   }
}

//-----------------------------------------------------------------------------
// Method call for the state "StateManaul"
//-----------------------------------------------------------------------------
void  CProcessControl::StateManual(EMethodId eMethodId)
{  
   static UInt32 cmdRequestLast;
   
   if (eMethodId == METHOD_ID_ENTER)
   {

   }
   else if (eMethodId == METHOD_ID_RUN)
   {
      // Check if a command was changed
      if(cmdRequest != cmdRequestLast)
      {
         if(cmdRequest & FLAG_MAN_DRYINGGAS_EN)
         {
            m_pAirFlow->On();
         }
         else
         {
            m_pAirFlow->Off();
         }
         
         if(cmdRequest & FLAG_MAN_HEATER_EN)
         {
            m_pHeater->On();
         }
         else
         {
            m_pHeater->Off();
         }
         
         if(cmdRequest & FLAG_MAN_DISPERSIONGAS_EN)
         {
            m_pDispersionGas->On();
         }
         else
         {
            m_pDispersionGas->Off();
         }
         
         if(!(cmdRequest & 0xFFFF0000))
         {
            DoTransition(STATE_IDLE);
         }
      }
      cmdRequestLast = cmdRequest;
   }
   else if (eMethodId == METHOD_ID_EXIT)
   {

   }
}
