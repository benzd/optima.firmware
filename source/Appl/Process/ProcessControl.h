/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------

Autor                  Datum
F. Sch�r               31.07.2014
Project:               L-200 / L-300
---------------------------------------------

Interface der Klasse StopperingControl

-- AENDERUNGEN ----------------------------------------------------------------


-----------------------------------------------------------------------------*/

#pragma once

#include "StateMachine.h"
#include "UiCommunication.h"
#include "AirFlow.h"
#include "Heater.h"
#include "DispersionGas.h"
#include "Sample.h"

class CProcessControl : public CStateMachine
{
public:
   static CProcessControl* GetInstance();
  
   // flags for binary control commans
   enum enCommand{CMD_AUTO_START, CMD_AUTO_STOP, CMD_AUTO_SKIP, CMD_MAN_DRYINGGAS_ON, CMD_MAN_DRYINGGAS_OFF, CMD_MAN_HEATER_ON, CMD_MAN_HEATER_OFF, CMD_MAN_DISPERSIONGAS_ON, CMD_MAN_DISPERSIONGAS_OFF};

   // Initialize the system
   void Init(void);
   
   // Launch system and sub elements
   void Start(void);
   
   // send a command
   void DoCommand(enCommand cmd);
   
      // update function to be called, must be implemented
   void UpdateObserver(CObservable*,UInt32);
   
   // Method call every cycle
   virtual void	CyclicCall();

   // Method call for the state process
   virtual void  StateCall(UInt32 nStateId, EMethodId eMethodId);
   
   // StartupCall function is called by base class after a given time after startup
   virtual void StartupCall();

protected:

   // Transfer reference
   void InitSystem();
    
   // startup
   void InitState(void);
   
   
private:
   static CProcessControl* m_pInstance;
     
   CAirFlow* m_pAirFlow;
   CHeater* m_pHeater;
   CDispersionGas* m_pDispersionGas;
     
   UInt32 cmdRequest;

   static const UInt32 FLAG_AUTO_START                  = 0x00000001;
   static const UInt32 FLAG_AUTO_SKIP                   = 0x00000002;
   static const UInt32 FLAG_MAN_DRYINGGAS_EN            = 0x00010000;
   static const UInt32 FLAG_MAN_HEATER_EN               = 0x00020000;
   static const UInt32 FLAG_MAN_DISPERSIONGAS_EN        = 0x00040000;
   
   // States of this process
   enum enStates {STATE_INIT, STATE_IDLE, STATE_HEAT_UP, STATE_INTERTIZE, 
      STATE_EQUILIBRATION, STATE_SPRAY_DRYING, STATE_NOZZLE_PULSES, 
      STATE_FLUSHING_NOZZLE, STATE_COOL_DOWN, STATE_SHUT_DOWN, STATE_MANUAL};

   ////////////////////// own state transition methods /////////////////////////
   void  StateInit(EMethodId eMethodId);
   void  StateIdle(EMethodId eMethodId);
   void  StateHeatUp(EMethodId eMethodId);
   void  StateInertize(EMethodId eMethodId);
   void  StateEquilibration(EMethodId eMethodId);
   void  StateSpryDrying(EMethodId eMethodId);
   void  StateNozzlePulses(EMethodId eMethodId);
   void  StateFlushingNozzle(EMethodId eMethodId);
   void  StateCoolDown(EMethodId eMethodId);
   void  StateShutDown(EMethodId eMethodId);
   void  StateManual(EMethodId eMethodId);
   
   CProcessControl();
   ~CProcessControl();
   
};