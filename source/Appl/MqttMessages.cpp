/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------
Autor                   Datum
Dominik Benz            09.01.2020

-- BESCHREIBUNG ---------------------------------------------------------------
Einstiegs-Modul. Erstellt die Tasks f�r die Applikation

-- AENDERUNGEN ----------------------------------------------------------------
Autor                  Datum

-----------------------------------------------------------------------------*/

//-----------------------------------------------------------------------------
// INCLUDES
//-----------------------------------------------------------------------------
#include <string>
#include "mqtt_client.h"
#include "BuchiTypes.h"
#include "JsonBox.h"
#include "UiCommunication.h"
#include "MainControl.h"

extern "C"
{
   #include "board.h"
   #include "Hardware.h"
   #include "SPI_Irq.h"
   #include "Pwm.h"
   #include "fsl_wdog.h"
}

//-----------------------------------------------------------------------------
// MACRO DEFINITIONS
//-----------------------------------------------------------------------------
#define A                       (Float32)0.00390802
#define B                       (Float32)(-0.0000005775)
#define R0                      (Float32)1000.0
//#define arrSize                         512
#define mainTaskIntervall               (5 / portTICK_PERIOD_MS) // 5ms

#ifdef DEBUG
   #define DEBUG_PRINT(...) \
      do { printf(__VA_ARGS__); } while (0)
#else
   #define DEBUG_PRINT(...) do {} while (0)
#endif

//-----------------------------------------------------------------------------
// LOCAL FUNCTION PROTOTYPES
//-----------------------------------------------------------------------------
void cbManStartDryingGas(string & sTopic, vector <UInt8> & vData, void* pvArg);
void cbManStopDryingGas(string & sTopic, vector <UInt8> & vData, void* pvArg);
static void sendStatusEvent(const char* pTopic, Bool status );
static void my_strCopy( char* strDest, const char* strSource );

//-----------------------------------------------------------------------------
// LOCAL VARIABLE DEFINITIONS
//-----------------------------------------------------------------------------
MQTTClient mqttClient;
UInt8 timer1 = 0;
UInt8 timer2 = 0;
static UInt8 dryingStatus = 0;

//-----------------------------------------------------------------------------
// CONSTANT DATA DEFINITIONS
//-----------------------------------------------------------------------------

static const char* acSubStatusData = "v1/system//event/instrument/status";
static const char* acSubRpcManStartDryingGas = "v1/system/+/command/startdryinggas";
static const char* acSubRpcManStopDryingGas = "v1/system/+/command/stopdryinggas";
static const char* acSubEvtDryingGasStatusChanged = "v1/system//event/dryinggasstatuschanged";
//static const char* acSubRpcManStartInletTemperature = "v1/system/+/command/startinlettemperature";
//static const char* acSubRpcManStopInletTemperature = "v1/system/+/command/stopinlettemperature";
static const char* acSubEvtInletTemperatureStatusChanged = "v1/system//event/inlettemperaturestatuschanged";

//-----------------------------------------------------------------------------
// LOCAL FUNCTIONS
//-----------------------------------------------------------------------------

static void sendStatusEvent(const char* pTopic, Bool status )
{
   JsonBox::Value jsonSendPayload;
   char acPayload[200] = {0};
   string jsonString;
   
   if(status)
   {
      jsonSendPayload["status"] = "on";
   }
   else
   {
      jsonSendPayload["status"] = "off";
   }
   
   jsonSendPayload.writeToString(jsonString);
   my_strCopy( acPayload, jsonString.c_str() );      
   mqttClient.Publish(pTopic, acPayload, strlen(acPayload), TRUE);
}

//-------------------------------------------------------------------------------------------------
static void my_strCopy( char* strDest, const char* strSource )
//-------------------------------------------------------------------------------------------------
{
  strncpy( strDest, strSource, strlen(strSource) );
  
  strDest[strlen(strSource)] = 0;
}


// callback funktionen funktionieren nicht f�r topics mit wildcard operatoren
static void cbManStartDryingGas(string & sTopic, vector <UInt8> & vData, void* pvArg)
{
   JsonBox::Value recPayload;
   JsonBox::Value sendPayload;
   //char acTopicResponse[200] = {0};
   //char acResponsePayload[200] = {0};

   string jsonPayloadString;

   std::string receivedData(vData.begin(), vData.end());
   Int32 datasize = vData.size();

   StartPwmOut1(100);
   dryingStatus = 1;

   CMainControl::GetInstance()->m_pProcessControl->DoCommand(1);

   // Repsonse auf ManStartDryGas Command absetzen
   sendStatusEvent(acSubEvtDryingGasStatusChanged, TRUE );
   /*
   try{
      recPayload.loadFromString(receivedData);
      string responseTopic = recPayload["responseTopic"].getString();


      strcpy( acTopicResponse, responseTopic.c_str() );

      sendPayload["resultCode"] = "success";
      sendPayload.writeToString(jsonPayloadString);
      my_strCopy( acResponsePayload, jsonPayloadString.c_str() );      
      mqttClient.Publish(acTopicResponse, acResponsePayload, strlen(acResponsePayload));

      sendStatusEvent(acSubEvtDryingGasStatusChanged, TRUE );
   }
   catch(...){
      sendStatusEvent(acSubEvtDryingGasStatusChanged, FALSE );
   }
*/

}

void cbManStopDryingGas(string & sTopic, vector <UInt8> & vData, void* pvArg)
{
   //UInt8 buf[20];
   //memcpy(&buf[0], &vData[0], vData.size());
   
   CMainControl::GetInstance()->m_pProcessControl->DoCommand(99);
   sendStatusEvent(acSubEvtDryingGasStatusChanged, FALSE );     // Hier schmiert die anwendung ab wenn, warum???
  // MQTT_DEBUG("My subcribed topic cbManStopDryingGas data: %s\n",  (const char *)buf);
} 

  
void InitMqttMessages(void)
{
   ip4_addr_t broker;
   
   // configure mqtt broker
   IP4_ADDR(&broker, 192, 168, 10, 100);
   mqttClient.Configure(broker, MQTT_PORT, 1, (TXTCHR*)"RT1062_MQTT", (TXTCHR*)"evalUser", (TXTCHR*)"evalPasswd");
}

void UpdateMqttMessages(void)
{
   static string statusString = "Init";
   static FLOAT32 temperature1 = 15.0;
   static FLOAT32 temperature2 = 15.0;
   static Bool ledState = FALSE;
   UInt32 iSubscriptionID;
   
   //char acTopicResponse[200] = {0};
   char acResponsePayload[500] = {0};
   //char acTopicFound[200] = {0};
   //char acPayLoad[500] = {0};
   string jsonPayloadString;

   TickType_t xNextWakeTime;

   JsonBox::Value sendPayloadStatus;
   JsonBox::Value status;
   JsonBox::Value procData;
   

   if (!mqttClient.IsConnected())
   {
      // Verbinden
      mqttClient.Start();

      // Warten bis die Verbindung steht.
      while(mqttClient.IsConnecting())
      {
         WDOG_Refresh(WDOG1);
         vTaskDelayUntil(&xNextWakeTime, mainTaskIntervall);
      }

      // Subscribe to topics
      /*
      mqttClient.GetSubscribe(acSubRpcManStartDryingGas, cbManStartDryingGas);
      mqttClient.GetSubscribe(acSubRpcManStopDryingGas, cbManStopDryingGas);
      */
      mqttClient.Subscribe(acSubRpcManStartDryingGas, iSubscriptionID, cbManStartDryingGas);
      mqttClient.Subscribe(acSubRpcManStopDryingGas, iSubscriptionID, cbManStopDryingGas);
      //mqttClient.GetSubscribe(acSubRpcManStartInletTemperature);
     // mqttClient.GetSubscribe(acSubRpcManStopInletTemperature);
      sendStatusEvent(acSubEvtDryingGasStatusChanged, FALSE );
      sendStatusEvent(acSubEvtInletTemperatureStatusChanged, FALSE );

   }
   else // connected
   {
      mqttClient.HandleSubscriptionCallbacks();

      //acTopicFound[0] = 0;
      //acPayLoad[0] = 0;
      /*
      if ( mqttClient.GetSearch(acSubRpcManStopInletTemperature, acTopicFound, acPayLoad, strlen(acPayLoad) ) )
      {
         StopPwmOut2();
         dryingStatus = 0;
         
         // Repsonse auf ManStopInletTmeprature Command absetzen
         recPayload.loadFromString(acPayLoad);                                  // response topic aus payload holen
         string responseTopic = recPayload["responseTopic"].getString();
         strcpy( acTopicResponse, responseTopic.c_str() );
         sendPayload["resultCode"] = "success";
         sendPayload.writeToString(jsonPayloadString);
         my_strCopy( acResponsePayload, jsonPayloadString.c_str() );      
         mqttClient.Set(acTopicResponse, acResponsePayload, strlen(acResponsePayload));
              
         // InletTemperatureStatusChange Event absetzen
         sendStatusEvent(acSubEvtInletTemperatureStatusChanged, FALSE );
      }
      if ( mqttClient.GetSearch(acSubRpcManStartInletTemperature, acTopicFound, acPayLoad, strlen(acPayLoad) ) )
      {
         StartPwmOut2(100);
         dryingStatus = 1;

         // Repsonse auf ManStartInletTemperature Command absetzen
         recPayload.loadFromString(acPayLoad);
         string responseTopic = recPayload["responseTopic"].getString();
         strcpy( acTopicResponse, responseTopic.c_str() );
         sendPayload["resultCode"] = "success";
         sendPayload.writeToString(jsonPayloadString);
         my_strCopy( acResponsePayload, jsonPayloadString.c_str() );      
         mqttClient.Set(acTopicResponse, acResponsePayload, strlen(acResponsePayload));
                        
         // InletTemperatureStatusChange Event absetzen
         sendStatusEvent(acSubEvtInletTemperatureStatusChanged, TRUE );
      } */ /*
      if ( mqttClient.GetSearch(acSubRpcManStopDryingGas, acTopicFound, acPayLoad, strlen(acPayLoad) ) )
      {
         StopPwmOut1();
         dryingStatus = 0;
         
         // Repsonse auf ManStopDryGas Command absetzen
         recPayload.loadFromString(acPayLoad);                                  // response topic aus payload holen
         string responseTopic = recPayload["responseTopic"].getString();
         strcpy( acTopicResponse, responseTopic.c_str() );
         sendPayload["resultCode"] = "success";
         sendPayload.writeToString(jsonPayloadString);
         my_strCopy( acResponsePayload, jsonPayloadString.c_str() );      
         mqttClient.Set(acTopicResponse, acResponsePayload, strlen(acResponsePayload));
         
         sendStatusEvent(acSubEvtDryingGasStatusChanged, FALSE );
      }
*/


      // Demo Temperaturen berechnen
      timer2++;
      if(timer2==500)
      {
         if(ledState) ledState = FALSE;
         else ledState = TRUE;
         SetOutputPin( eLED_FW, ledState);
         
         UpdateSPI_Irq();
         UInt32 tempval = 0;
         tempval = GetTemperatureBase();
         if(tempval != 0) // Temperaturmessung io, PT1000 Temperatur berechnen:
         {
            temperature1 = (-R0 * A + sqrt(R0*R0*A*A - 4*R0*B*(R0-tempval)))/(2*R0*B);
         }
         
         tempval = GetTemperatureTop();
         if(tempval != 0) // Temperaturmessung io, PT1000 Temperatur berechnen:
         {
            temperature2 = (-R0 * A + sqrt(R0*R0*A*A - 4*R0*B*(R0-tempval)))/(2*R0*B);
         }
         timer2 = 0;
      }
      
      // instrument status + instrument process data zyklisch absetzen 
      timer1++;
      if(timer1==100)
      {
        FREEZEDRYER_CYCLIC_OUT_STATUS pStatus;
        CUICommunication::GetInstance()->GetSendDataHandler()->GetCyclicSendStatus((UInt8*)&pStatus);
        
         
          // instrument status
         if(dryingStatus==0)
         {
            status["status"] = JsonBox::Value("Idle");
         }
         else if(dryingStatus==1)
         {
            status["status"] = JsonBox::Value("Running");
         }
         status["timestamp"] = "2020-01-15T10:17:55.565";
         status.writeToString(jsonPayloadString);
         my_strCopy( acResponsePayload, jsonPayloadString.c_str() );      
         mqttClient.Publish(acSubStatusData, acResponsePayload, strlen(acResponsePayload));
         
         

         timer1 = 0;
      }
      
   } // else (connected)
   
   // stdout nach BUCHU_DEBUG publishen.
   mqttClient.HandleDebugOutput();

}