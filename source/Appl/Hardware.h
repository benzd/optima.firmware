/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------
Autor                  Datum


-- BESCHREIBUNG ---------------------------------------------------------------



-- AENDERUNGEN ----------------------------------------------------------------
Autor                  Datum

-----------------------------------------------------------------------------*/
#pragma once

#include "BuchiTypes.h"

typedef struct
{
   UInt32 dryingGasTarget;
   UInt32 inletTemperatureTarget;
   UInt32 dispersionGasTarget;
   UInt32 samplePumpTarget;
   UInt32 unblockNozzleTarget;
   UInt8 dispersionGasMode;
   UInt8 heaterMode;
   UInt16 heaterManPower;
   Int16 offsetDispGas;
   Float32 kpDispGas;
   Float32 kiDispGas;
   Int16 offsetHeater;
   Float32 kpHeater;
   Float32 kiHeater;
} PARAM;

typedef struct
{
   Bool bInertLoopConnected;
} DIG_IN;

typedef struct
{
   Bool bFwLed;
   Bool bHeatSSR1;
   Bool bHeatSSR2;
} DIG_OUT;

typedef struct
{
   UInt32 inletTemperatureOhm;
   UInt32 outletTemperatureOhm;
   Float32 inletTemperatureCelsius;
   Float32 outletTemperaturCelsius;
   UInt32 dispersionGasFlow;
} ANALOG_IN;

typedef struct
{
   UInt16 dispersionGasValvePwm;
} ANALOG_OUT;

typedef struct
{
   DIG_IN digIn;
   DIG_OUT digOut;
   ANALOG_IN analogIn;
   ANALOG_OUT analogOut;
} IO;

//-----------------------------------------------------------------------------
Bool InitHardware(void);
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void GetHardware(void);
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void SetHardware(void);
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
IO* GetIOData(void);
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
PARAM* GetParamData(void);
//-----------------------------------------------------------------------------