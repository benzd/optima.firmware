/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------

Autor                  Datum
R. Kaufmann            26.06.2019

-- BESCHREIBUNG ---------------------------------------------------------------
Kommunikation zu Dongle etc. �ber B�chi-Bus.

-- AENDERUNGEN ----------------------------------------------------------------

Autor                  Datum

-----------------------------------------------------------------------------*/
#pragma once

#include "BuchiTypes.h"

// Slave Adresse der Einheit
#define SLAVE_ADR   0x81

// Maximale Stringl�nge in den Bus-Strukturen
#define MAX_LEN_STR (16)

// In der Windows-Simulation packen wir die Struktur nicht, da __attribute__ unverstanden.
#ifdef WIN32
   #define PACKED
#else
   #define PACKED __attribute__((__packed__))
#endif

typedef struct
{
   // 4
   UInt32 ulMemMapVersion;

   // MAX_LEN_STR bytes, terminator included
   UInt8  strDeviceSerialNumber[MAX_LEN_STR];
   
   // 4   format: YYYYh MMh DDh
   UInt32 ulPrintTestDate;
   
   // 4   format: YYYYh MMh DDh
   UInt32 ulEndTestDate;
   
   // 4
   UInt32 ulOperatingHours;
   
   // MAX_LEN_STR bytes, terminator included
   UInt8  strFWArticleNumber[MAX_LEN_STR];
   
   // 4
   UInt32 ulFWVersion;
   
   // 4
   UInt16 usPower48V;
   UInt16 usPower24V;
   
   // 4
   UInt16 usPower5V;
   UInt16 usPower3V3;
   
   // 4
   UInt16 usDummy2;
   Int16 sPrintTemp;
   
   // 4
   UInt32 ulHWStatus;
   
} DEVICE_MSG_INFO;

typedef struct
{   
   UInt8 ucMsgType;
   UInt8 ucMsgState;
   UInt16 usDummy;
   UInt32 uiMsgNb;
   UInt32 uiMsgDate;
   UInt32 uiMsgTime;
} DEVICE_MSG_OUT;

////////////////////////////////////////////////////////////////////////////////
// Telegrammhandling
////////////////////////////////////////////////////////////////////////////////

// Diese Prozedur muss periodisch aufgerufen werden. Sie bedient die RS485.
void LoopTelegram(void);

// Initialisierung des Moduls
void InitializeBus(void);

// Aufrufen nach InitializeBus()
void StartBusCommunication(UInt8 ucSlaveAddr);

////////////////////////////////////////////////////////////////////////////////
// Systeminformation
////////////////////////////////////////////////////////////////////////////////

typedef struct PACKED
{
  UINT8 major;
  UINT8 minor;
  UINT8 build;
  UINT8 revision;
} FWVersionTY;

typedef struct PACKED
{
   UINTL32 status;
   UINT16  opHours;
   TXTCHR  serialNo[MAX_LEN_STR];
   FWVersionTY firmware;
} DeviceDataTY;

// Print Temperaturwert in �C
FLOAT32 GetPrintTemp(void);

// Print Eingangsspannung 30V
FLOAT32 GetVoltage30V(void);

// Print Eingangsspannung 5V
FLOAT32 GetVoltage5V(void);

// Print Eingangsspannung 3.3V
FLOAT32 GetVoltage3V3(void);

//-------------------------------------------------------------------------------------------------
void RS485Request(UInt32 slaveAddress, UInt8 command, UInt8* data, UInt16 length);
Bool RS485RequestAllowed();

// Status Bits vom IO-Board
#define  STATUSBIT_                                      0x00000001
#define  STATUSBIT_CONFIG1                               0x00000002
#define  STATUSBIT_CONFIG2                               0x00000004
#define  STATUSBIT_CONFIG3                               0x00000008
#define  STATUSBIT_CONFIG4                               0x00000010
#define  STATUSBIT_ERROR_RS485                           0x04000000
#define  STATUSBIT_ERROR__                               0x08000000
#define  STATUSBIT_ERROR___                              0x10000000
#define  STATUSBIT_ERROR_FLEXRAM                         0x20000000
#define  STATUSBIT_ERROR____                             0x40000000
#define  STATUSBIT_ERROR_____                            0x80000000
