/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------

Autor                  Datum
Felix Sch�r            16.12.2015
Project:               L-200 / L-300

-- BESCHREIBUNG ---------------------------------------------------------------

Telegramm Datenstruktur
Gemeinsame Datei von L-200-300, Interface L-200/300, Interface L-200/300 Pro
Muss bei jeder �nderung in all dies Projekte Kopiert werden.

-- AENDERUNGEN ----------------------------------------------------------------


-----------------------------------------------------------------------------*/
#pragma once

#define MAX_LEN_STR (16)

enum enEndPoint { END_POINT_IDLE, END_POINT_RUNNING, END_POINT_DONE };
enum enStoppering { STOPPERING_NONE, STOPPERING_MANUAL, STOPPERING_AUTOMATIC };

typedef struct
{
   // 4
   UInt32 ulMemMapVersion;
      
   // 4
   UInt8 ucDeviceConfiguration;
   UInt8 ucIceCondenserType;
   UInt16 usDummy;
   
   // MAX_LEN_STR bytes, terminator included
   UInt8  strDeviceSerialNumber[MAX_LEN_STR];
   
   // 4   format: YYYYh MMh DDh
   UInt32 ulPrintTestDate;
   
   // 4   format: YYYYh MMh DDh
   UInt32 ulEndTestDate;
   
   // 4
   UInt32 ulOperatingHours;
   
   // 4
   UInt32 ulOperatingHoursVacPump;
   
   // 4
   UInt32 ulOperatingHoursVacOil;
   
   // 4
   UInt32 ulOperatingHoursCoolCircuit;
   
   // 4
   UInt32 ulOperatingHoursSteamGen;
   
   // 4
   UInt32 ulOperatingUsageStoppering;
   
   // MAX_LEN_STR bytes, terminator included
   UInt8  strFWArticleNumber[MAX_LEN_STR];
   
   // 4
   UInt32 ulFWVersion;
   
   // 4
   UInt16 usPower48V;
   UInt16 usPower24V;
   
   // 4
   UInt16 usPower5V;
   UInt16 usPower3V3;
   
   // 4
   UInt16 usDummy2;
   Int16 sPrintTemp;
   
   // 4
   UInt32 ulHWStatus;
   
} FREEZEDRYER_INTERNAL;

typedef struct
{   
   UInt8 ucMsgType;
   UInt8 ucMsgState;
   UInt16 usDummy;
   UInt32 uiMsgNb;
   UInt32 uiMsgDate;
   UInt32 uiMsgTime;
} FREEZEDRYER_MSG_OUT;



typedef struct
{
   // 4
   // Ice condenser 1 inlet temperature (or single)
   Int16 sIceCondInletTemp1;
   // Ice condenser 1 outlet temperature (or single)
   Int16 sIceCondOutletTemp1;
   
   // 4
   // Ice condenser 2 inlet temperature
   Int16 sIceCondInletTemp2;
   // Ice condenser 2 outlet temperature
   Int16 sIceCondOutletTemp2;
      
   
} FREEZEDRYER_CYCLIC_OUT_DATA;



typedef struct
{
   Float32 fDryingGasFlow;
   Float32 fInletTemperature;
   Float32 fDispersionGasFlow;
   Float32 fOutletTemperature;
   Float32 fProductTemperature;
   Float32 fOxygenConcentration;
} SPRAYDRYER_CYCLIC_OUT_STATUS;

typedef struct
{
   UInt32 dispGasTarget;                // 0
   UInt32 dispGasActual_lh;             // 4
   UInt32 dispGasActual_lh_filtered;    // 8
   Float32 dispGasError;                // 12
   Float32 dispGasPpart;                // 16
   Float32 dispGasIpart;                // 20
   UInt32 dispGasPwmOut;                // 24
   UInt32 inletTempTarget;              // 28
   Float32 inletTempActual;             // 32
   UInt32 heaterPwmOut;                 // 36
   Float32 heaterError;                 // 40
   Float32 heaterPpart;                 // 44
   Float32 heaterIpart;                 // 48
   UInt32 heatPowerW;                   // 52
   UInt8 ssr1;                          // 56
   UInt8 ssr2;                          // 57
} SPRAYDRYER_REG_TRACE;


typedef struct
{
   // 4
   UInt8 ucOpenCloseProt1Valve;
   UInt8 ucOpenCloseProt2Valve;
   Int16 iBypassLT;
   
   // 4
   Int16 iEEVHT;
   Int16 iDummy;
   
} FREEZEDRYER_CYCLIC_MEAS_BOARD_OUT;

typedef struct
{   
   // logger string
   char cLog[255];
   
} FREEZEDRYER_LOGGER;

// Exceeding error reaction
enum ExcErrReaction { 
                       REACT_UNDEF = 0,
                       NO_REACTION,            
                       REACT_SAMP_PROT,
                       REACT_WARNING_MSG
                    };

typedef struct
{
   // 4
   Int16 sSampErrTemp;
   UInt8 ucActionPressExc;
   UInt8 ucActionSampTempExc;
   
} METHOD_Settings;

typedef struct
{
   // step duration in seconds
   UInt16 usDuration; // minutes
   
   // shelf step end temperatures
   Int16 sShelfTargetTemp;
   
   // chamber step end pressure
   FLOAT32 fChambTargetPress;
   
   // chamber step safety pressure
   FLOAT32 fChambSafetyPress;
   
   // chamber step maximum safety pressure exceed duration
   UInt16 uiChambSafetyDuration;
   
   // dummy
   UInt16 usDummy;
   
} METHOD_STEP;

typedef struct
{   
   // 4
   Int16 sShelfTargetTemp;
   UInt16 usDummy;
} METHOD_LOADING;

typedef struct
{
   // 4
   UInt8 ucEndPointStatus; // value can be: enum enEndPoint { END_POINT_IDLE, END_POINT_RUNNING, END_POINT_DONE };
   UInt8 ucDummy;
   UInt16 usDummy;
   
   // ucEndPointStatus:
   // running method phase:
   //   ucEndPointStatus = END_POINT_DONE
   //   -> current method phase is being terminated now
   //
   // finished method phase:
   //   ucEndPointStatus = END_POINT_RUNNING
   //   -> active method phase is being extended (continued) without time expiration
   //
   // other combinations do not generate any special activity
   
} METHOD_END_POINT;

typedef struct // pilot only
{
   METHOD_END_POINT endPoint;
   
} METHOD_FREEZING_PARAM;

typedef struct
{   
   METHOD_Settings settings;
   
   METHOD_END_POINT endPoint;
   
} METHOD_PRIMARY_DRYING_PARAM;

typedef struct
{   
   METHOD_Settings settings;
   
   METHOD_END_POINT endPoint;   
   
} METHOD_SECONDARY_DRYING_PARAM;


typedef struct
{
   // stoppering chamber pressure
   FLOAT32 fPress;
   
   UInt16 usDummy;
   
   // stoppering used?
   UInt8 ucUsed;
   
   // use air or inert gas for stoppering?
   UInt8 ucAirInertGas;
      
} METHOD_STOPPERING;

typedef struct
{
   // hold chamber pressure
   FLOAT32 fPress;
   
   // hold shelf temperature
   Int16 sShelfTemp;
   
   UInt16 usDummy;
   
} METHOD_HOLD;

typedef struct
{  
   METHOD_FREEZING_PARAM par;
   METHOD_STEP step;
   
} METHOD_FREEZING;

typedef struct
{  
   METHOD_PRIMARY_DRYING_PARAM par;
   
   METHOD_STEP step;
   
} METHOD_PRIMARY_DRYING;

typedef struct
{
   METHOD_SECONDARY_DRYING_PARAM par;
   
   METHOD_STEP step;
   
} METHOD_SECONDARY_DRYING;

typedef struct
{
   METHOD_LOADING loading;
   METHOD_FREEZING freezing;
   METHOD_PRIMARY_DRYING primaryDrying;
   METHOD_SECONDARY_DRYING secondaryDrying;
   METHOD_STOPPERING stoppering;
   METHOD_HOLD hold;
} FREEZEDRYER_METHOD;

typedef struct
{
   UInt8 ucMsgAck;
   UInt8 ucMsgAckOld;
   UInt8 ucMsgType;
   UInt8 ucMsgState;
   UInt32 uiMsgNb;
} FREEZEDRYER_CYCLIC_MSG_IN;

typedef struct
{   
   // 4
   UInt32 uiDate;

   // 4
   UInt32 uiTime;
   
   // 4
   UInt8 ucStartStopCompressorHT;
   UInt8 ucStartStopCompressorLT;
   UInt8 ucStartStoppering;
   UInt8 ucSelectPressSensorType;

   //4
   UInt16 usEEV1;
   UInt16 usEEV2;   
   
   // 4
   UInt8 ucStartStopVacuumPump;
   UInt8 ucOnOffPumpDefrosting;
   UInt8 ucVacuumPumpOilUseHoursReset;
   UInt8 ucVacuumPumpOpHoursReset;
      
   
} FREEZEDRYER_CYCLIC_CMD_IN;


typedef struct
{     
   // 4
   UInt8 ucProtValve1Avail;
   UInt8 ucProtValve2Avail;
   UInt16 usSuctionLineTempLT; // AD value
   
   // 4
   UInt16 usDischargeLineTempHT; // AD value
   UInt16 usDischargeLineTempLT; // AD value
   
   // 4
   UInt16 usCondPressLT; // [mBar]
   UInt16 usEvapPressLT; // [mBar]
   
   // 4
   UInt16 usCondPressHT; // [mBar]
   UInt16 usEvapPressHT; // [mBar]
   
} FREEZEDRYER_CYC_MEAS_BOARD_IN;