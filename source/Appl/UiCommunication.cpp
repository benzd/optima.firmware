/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------

Autor                  Datum
Felix Sch�r            09.04.2014
Project:               L-200 / L-300

-- BESCHREIBUNG ---------------------------------------------------------------

Communication interface to the UI
Singleton class
Receive and send Data stored as dual port RAM

-- AENDERUNGEN ----------------------------------------------------------------


-----------------------------------------------------------------------------*/
#include "UICommunication.h"
#include "TimeBase.h"
#include "JsonBox.h"
#include "ProcessControl.h"
extern "C"
{
   #include "Hardware.h"
}

static void ManualRPCCallback(string & sTopic, vector <UInt8> & vData, void* pvArg);

// static instance pointer initialization
CUICommunication* CUICommunication::m_pInstance = 0;

// private constructor
CUICommunication::CUICommunication(void)
{
   //pProcessControl = 0;
}

// private destructor
CUICommunication::~CUICommunication(void)
{
}

// singleton
CUICommunication* CUICommunication::GetInstance()
{
   if (m_pInstance == 0)
   {
      m_pInstance = new CUICommunication();
   }

   return m_pInstance;
}

//-----------------------------------------------------------------------------
void CUICommunication::Init(void)
//-----------------------------------------------------------------------------
{
   ip4_addr_t broker;
   
   // configure mqtt broker
   //IP4_ADDR(&broker, 192, 168, 10, 100);
   IP4_ADDR(&broker, 192, 168, 10, 99);
   m_mqttClient.Configure(broker, MQTT_PORT, 1, (TXTCHR*)"RT1062_MQTT", (TXTCHR*)"evalUser", (TXTCHR*)"evalPasswd");
   
   //pProcessControl = CProcessControl::GetInstance();
   CTimeBase::GetInstance()->GetTimeBaseObserver(CTimeBase::EN_BASE100)->AddObserver(this);
}

//-----------------------------------------------------------------------------
void CUICommunication::Start(void)
//-----------------------------------------------------------------------------
{
   //SetupLastWill(); // funktioniert noch nicht (message geht nicht weg und ui kann "unknown" eh nicht anzeigen
   //m_mqttClient.Start();
}

// update function from observer
//-----------------------------------------------------------------------------
void  CUICommunication::UpdateObserver(CObservable* p_Observable,UInt32 p_nId)
//-----------------------------------------------------------------------------
{
   static UInt32 timer100ms = 0;
   static Bool mqttConnected = FALSE;
   static Bool lastB295ConnectionState = FALSE;
   
   if (p_nId == OBSERVABLE_MSGSYS_ACK) // messages need to be observed by specialized classes
   {
   }
   else if ( p_nId == OBSERVABLE_TMR)
   {
      if(m_mqttClient.IsConnected())
      {
         // test for first connection establishment
         if(!mqttConnected)
         {
            mqttConnected = TRUE;
            m_mqttClient.Subscribe(m_SUBJECT_RPC_CMD, m_uiSubscriptionID_rpcManCmd, ManualRPCCallback);
            SendEventConfigurationChanged();
            SendEventParameterChanged();
            SendEventComponentStatusChange("dryinggasstatuschanged","off");
            SendEventComponentStatusChange("inlettemperaturestatuschanged","off");
            SendEventComponentStatusChange("dispersiongasstatuschanged","off");
         }
         else
         {
            // only process cylcic updates if connected
            m_mqttClient.HandleSubscriptionCallbacks();
            timer100ms++;
            if(((timer100ms%10) == 0) && (m_mqttClient.IsConnected()))   // each 10.call (10*100ms = 1s)
            {
               SendEventCyclicProcessData();
               GetIOData()->digOut.bFwLed = !GetIOData()->digOut.bFwLed;
            }
            m_mqttClient.HandleDebugOutput();
            
            // check b295 connection -> has to be moved to correct component lateer
            if(GetIOData()->digIn.bInertLoopConnected != lastB295ConnectionState)
            {
               SendEventConfigurationChanged();
            }
            lastB295ConnectionState = GetIOData()->digIn.bInertLoopConnected;
         }
      }
      else
      {
         SetupLastWill(); // funktioniert noch nicht (message geht nicht weg und ui kann "unknown" eh nicht anzeigen
         m_mqttClient.Start();
         mqttConnected = FALSE;
         timer100ms++;
         if((timer100ms%2) == 0)   // each 2.call (2*100ms = 0.2s) // schnelles blinken wenn keine Verbindung
         {
            GetIOData()->digOut.bFwLed = !GetIOData()->digOut.bFwLed;
         }
      }
   }
}

// Set source for send data block
//-----------------------------------------------------------------------------
SPRAYDRYER_CYCLIC_OUT_STATUS* CUICommunication::GetCyclicSendData(void)
//-----------------------------------------------------------------------------
{
   return &m_SEND_CYCLIC_OUT_Data;
}

// get source for regulator trace data
//-----------------------------------------------------------------------------
SPRAYDRYER_REG_TRACE* CUICommunication::GetRegTraceSendData(void)
//-----------------------------------------------------------------------------
{
   return &m_SEND_REG_TRACE_Data;
}

//-----------------------------------------------------------------------------
void CUICommunication::SendEventComponentStatusChange(const char* pcComponent, const char* pcNewStatus)
//-----------------------------------------------------------------------------
{
   JsonBox::Value componentStatusJson;
   string jsonPayloadString;
   string sOutTopic;
   char acOutTopic[200] = {0};

   // build json with new instrument status
   componentStatusJson["status"] = JsonBox::Value(pcNewStatus);
   componentStatusJson["timestamp"] = "2020-01-15T10:17:55.565";
   
   // prepare output data
   componentStatusJson.writeToString(jsonPayloadString);
   sOutTopic = m_SUBJECT_COMPONENT_STATUS;
   sOutTopic.append(pcComponent);
   strcpy(&acOutTopic[0], sOutTopic.c_str());
   
   m_mqttClient.Publish(acOutTopic, jsonPayloadString, true);

}

//-----------------------------------------------------------------------------
void CUICommunication::SendEventInstrumentStatusChange(const char* pcNewStatus)
//-----------------------------------------------------------------------------
{
   JsonBox::Value statusJson;
   string jsonPayloadString;

   // instrument status
   statusJson["status"] = JsonBox::Value(pcNewStatus);
   statusJson["timestamp"] = "2020-01-15T10:17:55.565";
   
   statusJson.writeToString(jsonPayloadString);
   m_mqttClient.Publish(m_SUBJECT_INSTRUMENT_STATUS, jsonPayloadString, true);

}



//-----------------------------------------------------------------------------
void CUICommunication::SendEventCyclicProcessData(void)
//-----------------------------------------------------------------------------
{
   JsonBox::Value procDataJson;
   string jsonPayloadString;
   
   // instrument process data
   procDataJson["dryingGas"] = JsonBox::Value(m_SEND_CYCLIC_OUT_Data.fDryingGasFlow);
   procDataJson["inletTemperature"] = JsonBox::Value(m_SEND_CYCLIC_OUT_Data.fInletTemperature);
   procDataJson["dispersionGas"] = JsonBox::Value(m_SEND_CYCLIC_OUT_Data.fDispersionGasFlow);
   procDataJson["outletTemperature"] = JsonBox::Value(m_SEND_CYCLIC_OUT_Data.fOutletTemperature);
   procDataJson["productTemparature"] = JsonBox::Value(m_SEND_CYCLIC_OUT_Data.fProductTemperature);
   procDataJson["oxygen"] = JsonBox::Value(m_SEND_CYCLIC_OUT_Data.fOxygenConcentration);
   procDataJson["timestamp"] = "2020-01-13T10:17:55.565";
   procDataJson.writeToString(jsonPayloadString);
   m_mqttClient.Publish(m_SUBJECT_PROC_DATA, jsonPayloadString, false);

}

//-----------------------------------------------------------------------------
void CUICommunication::SendEventParameterChanged(void)
//-----------------------------------------------------------------------------
{
   JsonBox::Value parameterJson;
   string jsonPayloadString;
   
   // instrument parameters
   parameterJson["dryingGas"] = JsonBox::Value((Float32)GetParamData()->dryingGasTarget/10);
   parameterJson["inletTemperature"] = JsonBox::Value((Float32)GetParamData()->inletTemperatureTarget/10);
   parameterJson["dispersionGas"] = JsonBox::Value((int)GetParamData()->dispersionGasTarget);
   parameterJson["samplePump"] = JsonBox::Value((Float32)GetParamData()->samplePumpTarget/10);
   parameterJson["unblockNozzle"] = JsonBox::Value((int)GetParamData()->unblockNozzleTarget);
   parameterJson["timestamp"] = "2020-01-13T10:17:55.565";
   parameterJson.writeToString(jsonPayloadString);
   m_mqttClient.Publish(m_SUBJECT_PARAMETER_CHANGED, jsonPayloadString, true);

}

//-----------------------------------------------------------------------------
void CUICommunication::SendEventConfigurationChanged(void)
//-----------------------------------------------------------------------------
{
   JsonBox::Value configJson;
   string jsonPayloadString;
   
   // instrument parameters
   configJson["timestamp"] = "2020-03-13T15:17:55.565";
   configJson["components"][size_t(0)]["name"] = JsonBox::Value("ControlBoard");
   
   if(GetIOData()->digIn.bInertLoopConnected)
   {
      configJson["components"][size_t(0)]["subcomponents"][size_t(0)]["name"] = JsonBox::Value("B295");
   }
   else
   {
      configJson["components"][size_t(0)]["subcomponents"][size_t(0)][""] = JsonBox::Value("");
   }

   configJson.writeToString(jsonPayloadString);
   m_mqttClient.Publish(m_SUBJECT_CONFIG_CHANGED, jsonPayloadString, true);
}
     
//-----------------------------------------------------------------------------
void CUICommunication::SetupLastWill(void)
//-----------------------------------------------------------------------------
{
   JsonBox::Value statusJson;
   string jsonPayloadString;

   char acOutTopic[200] = {0};

   // instrument status
   statusJson["status"] = JsonBox::Value("unknown");
   statusJson["timestamp"] = "2020-01-15T10:17:55.565";

   statusJson.writeToString(jsonPayloadString);
   
   strcpy(&acOutTopic[0], jsonPayloadString.c_str());
   
   m_mqttClient.LastWill("v1/system/event/status", acOutTopic);
}

//-----------------------------------------------------------------------------
void parseSetParameterRequest(vector <UInt8> & vData)
//-----------------------------------------------------------------------------
{
   JsonBox::Array ar;
   JsonBox::Value v;
   string sKey;
   
   std::string receivedPayload(vData.begin(), vData.end());
   
   v.loadFromString(receivedPayload);
   
   ar = v["params"].getArray();
   
   for ( int iCnt = 0; iCnt < ar.size(); iCnt++ )
   {
      JsonBox::Value element;
      element = ar[iCnt];
      
      sKey = element["key"].getString();
      if(sKey == "dryingGas")
      {
         GetParamData()->dryingGasTarget = (UInt32)(element["value"].getFloat() * 10);
      }
      else if(sKey == "inletTemperature")
      {
         GetParamData()->inletTemperatureTarget = (UInt32)(element["value"].getFloat() * 10);
      }
      else if(sKey == "dispersionGas")
      {
         GetParamData()->dispersionGasTarget = (UInt32)(element["value"].getInteger());
      }
      else if(sKey == "samplePump")
      {
         GetParamData()->samplePumpTarget = (UInt32)(element["value"].getFloat() * 10);
      }
      else if(sKey == "unblockNozzle")
      {
         GetParamData()->unblockNozzleTarget = (UInt32)(element["value"].getInteger());
      }
   }
   
}


//-----------------------------------------------------------------------------
static void ManualRPCCallback(string & sTopic, vector <UInt8> & vData, void* pvArg)
//-----------------------------------------------------------------------------
{
   JsonBox::Value responseJson;
   string jsonPayloadString;
   string sOutTopic;
   char acOutTopic[120] = {0};
   CProcessControl* pProcessControl;

   pProcessControl = CProcessControl::GetInstance();
   // check which command was received
   if(sTopic.find("setparameters") != string::npos)
   {
      parseSetParameterRequest(vData);
   }
   else if(sTopic.find("startdryinggas") != string::npos)
   {
      pProcessControl->DoCommand(CProcessControl::CMD_MAN_DRYINGGAS_ON);
   }
   else if(sTopic.find("stopdryinggas") != string::npos)
   {
      pProcessControl->DoCommand(CProcessControl::CMD_MAN_DRYINGGAS_OFF);
   }
   else if(sTopic.find("startinlettemperature") != string::npos)
   {
      pProcessControl->DoCommand(CProcessControl::CMD_MAN_HEATER_ON);
   }
   else if(sTopic.find("stopinlettemperature") != string::npos)
   {
      pProcessControl->DoCommand(CProcessControl::CMD_MAN_HEATER_OFF);
   }
   else if(sTopic.find("startdispersiongas") != string::npos)
   {
      pProcessControl->DoCommand(CProcessControl::CMD_MAN_DISPERSIONGAS_ON);
   }
   else if(sTopic.find("stopdispersiongas") != string::npos)
   {
      pProcessControl->DoCommand(CProcessControl::CMD_MAN_DISPERSIONGAS_OFF);
   }
   
   // build response
   responseJson["resultCode"] = JsonBox::Value("success");
   responseJson["isSuccess"] = JsonBox::Value(true);
   responseJson.writeToString(jsonPayloadString);
   sOutTopic = sTopic;
   sOutTopic.append("/response");
   strcpy(&acOutTopic[0], sOutTopic.c_str());
   
   // publish the response
   CUICommunication::GetInstance()->m_mqttClient.Publish(acOutTopic, jsonPayloadString, false);
   
   // publish the current paramters
   if(sTopic.find("setparameters") != string::npos)
   {
      CUICommunication::GetInstance()->SendEventParameterChanged();
   }
   
}