/*-----------------------------------------------------------------------------

B�chi Labortechnik AG, CH-9230 Flawil

-------------------------------------------------------------------------------

Autor                  Datum
R. Kaufmann            26.06.2019

-- BESCHREIBUNG ---------------------------------------------------------------
Kommunikation Standard-Telegramme �ber B�chi-Bus.

-- AENDERUNGEN ----------------------------------------------------------------

Autor                  Datum

-----------------------------------------------------------------------------*/
#pragma once

#include "BuchiTypes.h"

// Standard-Telegramme �ber B�chi-Bus beantworten.
UInt32 DoStdTelegram(UInt8 command, UInt8* pRecData, UInt32 recDataLen, UInt8* pSendData);